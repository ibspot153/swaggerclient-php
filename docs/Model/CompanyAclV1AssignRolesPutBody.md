# CompanyAclV1AssignRolesPutBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**user_id** | **int** |  | 
**roles** | [**\Swagger\Client\Model\CompanyDataRoleInterface[]**](CompanyDataRoleInterface.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


