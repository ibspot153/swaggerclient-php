# CompanyDataCompanyCustomerInterface

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customer_id** | **int** | Customer ID. | [optional] 
**company_id** | **int** | Company ID. | [optional] 
**job_title** | **string** | Get job title. | [optional] 
**status** | **int** | Customer status. | [optional] 
**telephone** | **string** | Get telephone. | [optional] 
**extension_attributes** | [**\Swagger\Client\Model\CompanyDataCompanyCustomerExtensionInterface**](CompanyDataCompanyCustomerExtensionInterface.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


