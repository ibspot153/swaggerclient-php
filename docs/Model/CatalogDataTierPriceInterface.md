# CatalogDataTierPriceInterface

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**price** | **float** | Tier price. | 
**price_type** | **string** | Tier price type. | 
**website_id** | **int** | Website id. | 
**sku** | **string** | SKU. | 
**customer_group** | **string** | Customer group. | 
**quantity** | **float** | Quantity. | 
**extension_attributes** | [**\Swagger\Client\Model\CatalogDataTierPriceExtensionInterface**](CatalogDataTierPriceExtensionInterface.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


