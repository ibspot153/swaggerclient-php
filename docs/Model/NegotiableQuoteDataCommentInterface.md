# NegotiableQuoteDataCommentInterface

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**entity_id** | **int** | Comment ID. | 
**parent_id** | **int** | Negotiable quote ID, that this comment belongs to. | 
**creator_type** | **int** | The comment creator type. | 
**is_decline** | **int** | Is quote was declined by seller. | 
**is_draft** | **int** | Is quote draft flag. | 
**creator_id** | **int** | Comment creator ID. | 
**comment** | **string** | Comment. | 
**created_at** | **string** | Comment created at. | 
**extension_attributes** | [**\Swagger\Client\Model\NegotiableQuoteDataCommentExtensionInterface**](NegotiableQuoteDataCommentExtensionInterface.md) |  | [optional] 
**attachments** | [**\Swagger\Client\Model\NegotiableQuoteDataCommentAttachmentInterface[]**](NegotiableQuoteDataCommentAttachmentInterface.md) | Existing attachments. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


