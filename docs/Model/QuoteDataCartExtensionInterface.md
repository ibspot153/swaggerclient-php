# QuoteDataCartExtensionInterface

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**shipping_assignments** | [**\Swagger\Client\Model\QuoteDataShippingAssignmentInterface[]**](QuoteDataShippingAssignmentInterface.md) |  | [optional] 
**negotiable_quote** | [**\Swagger\Client\Model\NegotiableQuoteDataNegotiableQuoteInterface**](NegotiableQuoteDataNegotiableQuoteInterface.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


