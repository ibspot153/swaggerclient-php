# InventoryApiDataSourceInterface

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**source_code** | **string** | Source code | [optional] 
**name** | **string** | Source name | [optional] 
**email** | **string** | Source email | [optional] 
**contact_name** | **string** | Source contact name | [optional] 
**enabled** | **bool** | If source is enabled. For new entity can be null | [optional] 
**description** | **string** | Source description | [optional] 
**latitude** | **float** | Source latitude | [optional] 
**longitude** | **float** | Source longitude | [optional] 
**country_id** | **string** | Source country id | [optional] 
**region_id** | **int** | Region id if source has registered region. | [optional] 
**region** | **string** | Region title if source has custom region | [optional] 
**city** | **string** | Source city | [optional] 
**street** | **string** | Source street name | [optional] 
**postcode** | **string** | Source post code | [optional] 
**phone** | **string** | Source phone number | [optional] 
**fax** | **string** | Source fax | [optional] 
**use_default_carrier_config** | **bool** | Is need to use default config | [optional] 
**carrier_links** | [**\Swagger\Client\Model\InventoryApiDataSourceCarrierLinkInterface[]**](InventoryApiDataSourceCarrierLinkInterface.md) |  | [optional] 
**extension_attributes** | [**\Swagger\Client\Model\InventoryApiDataSourceExtensionInterface**](InventoryApiDataSourceExtensionInterface.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


