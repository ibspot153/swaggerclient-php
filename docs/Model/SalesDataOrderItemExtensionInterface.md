# SalesDataOrderItemExtensionInterface

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**gift_message** | [**\Swagger\Client\Model\GiftMessageDataMessageInterface**](GiftMessageDataMessageInterface.md) |  | [optional] 
**gw_id** | **string** |  | [optional] 
**gw_base_price** | **string** |  | [optional] 
**gw_price** | **string** |  | [optional] 
**gw_base_tax_amount** | **string** |  | [optional] 
**gw_tax_amount** | **string** |  | [optional] 
**gw_base_price_invoiced** | **string** |  | [optional] 
**gw_price_invoiced** | **string** |  | [optional] 
**gw_base_tax_amount_invoiced** | **string** |  | [optional] 
**gw_tax_amount_invoiced** | **string** |  | [optional] 
**gw_base_price_refunded** | **string** |  | [optional] 
**gw_price_refunded** | **string** |  | [optional] 
**gw_base_tax_amount_refunded** | **string** |  | [optional] 
**gw_tax_amount_refunded** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


