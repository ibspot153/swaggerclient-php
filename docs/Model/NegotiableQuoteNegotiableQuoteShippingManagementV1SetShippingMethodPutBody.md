# NegotiableQuoteNegotiableQuoteShippingManagementV1SetShippingMethodPutBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**shipping_method** | **string** | The shipping method code. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


