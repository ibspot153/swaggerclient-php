# CatalogDataCategoryLinkInterface

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**position** | **int** |  | [optional] 
**category_id** | **string** | Category id | 
**extension_attributes** | [**\Swagger\Client\Model\CatalogDataCategoryLinkExtensionInterface**](CatalogDataCategoryLinkExtensionInterface.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


