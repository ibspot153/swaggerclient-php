# CompanyDataCompanyOrderInterface

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**order_id** | **int** | Order ID. | [optional] 
**company_id** | **int** | Company ID. | [optional] 
**company_name** | **string** | Company name. | [optional] 
**extension_attributes** | [**\Swagger\Client\Model\CompanyDataCompanyOrderExtensionInterface**](CompanyDataCompanyOrderExtensionInterface.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


