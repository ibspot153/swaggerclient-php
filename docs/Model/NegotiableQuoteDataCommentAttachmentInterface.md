# NegotiableQuoteDataCommentAttachmentInterface

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**attachment_id** | **int** | Attachment ID. | 
**comment_id** | **int** | Comment ID. | 
**file_name** | **string** | File name. | 
**file_path** | **string** | File path. | 
**file_type** | **string** | File type. | 
**extension_attributes** | [**\Swagger\Client\Model\NegotiableQuoteDataCommentAttachmentExtensionInterface**](NegotiableQuoteDataCommentAttachmentExtensionInterface.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


