# CompanyCreditDataCreditLimitInterface

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | ID. | [optional] 
**company_id** | **int** | Company id. | [optional] 
**credit_limit** | **float** | Credit Limit. | [optional] 
**balance** | **float** | Balance. | [optional] 
**currency_code** | **string** | Currency Code. | [optional] 
**exceed_limit** | **bool** | Exceed Limit. | 
**available_limit** | **float** | Available Limit. | [optional] 
**credit_comment** | **string** | Credit comment for company credit history. | [optional] 
**extension_attributes** | [**\Swagger\Client\Model\CompanyCreditDataCreditLimitExtensionInterface**](CompanyCreditDataCreditLimitExtensionInterface.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


