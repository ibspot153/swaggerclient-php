# CatalogDataProductRenderPriceInfoExtensionInterface

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**msrp** | [**\Swagger\Client\Model\MsrpDataProductRenderMsrpPriceInfoInterface**](MsrpDataProductRenderMsrpPriceInfoInterface.md) |  | [optional] 
**tax_adjustments** | [**\Swagger\Client\Model\CatalogDataProductRenderPriceInfoInterface**](CatalogDataProductRenderPriceInfoInterface.md) |  | [optional] 
**weee_attributes** | [**\Swagger\Client\Model\WeeeDataProductRenderWeeeAdjustmentAttributeInterface[]**](WeeeDataProductRenderWeeeAdjustmentAttributeInterface.md) |  | [optional] 
**weee_adjustment** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


