# InventorySourceSelectionApiDataSourceSelectionItemInterface

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**source_code** | **string** | Source code | 
**sku** | **string** | Item SKU | 
**qty_to_deduct** | **float** | Quantity which will be deducted for this source | 
**qty_available** | **float** | Available quantity for this source | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


