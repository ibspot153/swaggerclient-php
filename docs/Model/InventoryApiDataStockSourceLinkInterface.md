# InventoryApiDataStockSourceLinkInterface

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**stock_id** | **int** | Stock id | [optional] 
**source_code** | **string** | Source code of the link | [optional] 
**priority** | **int** | Priority of the link | [optional] 
**extension_attributes** | [**\Swagger\Client\Model\InventoryApiDataStockSourceLinkExtensionInterface**](InventoryApiDataStockSourceLinkExtensionInterface.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


