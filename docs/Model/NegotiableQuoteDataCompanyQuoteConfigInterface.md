# NegotiableQuoteDataCompanyQuoteConfigInterface

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**company_id** | **string** | Company id | [optional] 
**is_quote_enabled** | **bool** | Quote enabled for company | 
**extension_attributes** | [**\Swagger\Client\Model\NegotiableQuoteDataCompanyQuoteConfigExtensionInterface**](NegotiableQuoteDataCompanyQuoteConfigExtensionInterface.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


