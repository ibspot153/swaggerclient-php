# NegotiableQuoteDataAttachmentContentInterface

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**base64_encoded_data** | **string** | Media data (base64 encoded content). | 
**type** | **string** | MIME type. | 
**name** | **string** | File name. | 
**extension_attributes** | [**\Swagger\Client\Model\NegotiableQuoteDataAttachmentContentExtensionInterface**](NegotiableQuoteDataAttachmentContentExtensionInterface.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


