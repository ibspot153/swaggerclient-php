# CatalogDataProductRenderExtensionInterface

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**wishlist_button** | [**\Swagger\Client\Model\CatalogDataProductRenderButtonInterface**](CatalogDataProductRenderButtonInterface.md) |  | [optional] 
**review_html** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


