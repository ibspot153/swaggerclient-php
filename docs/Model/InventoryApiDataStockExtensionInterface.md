# InventoryApiDataStockExtensionInterface

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sales_channels** | [**\Swagger\Client\Model\InventorySalesApiDataSalesChannelInterface[]**](InventorySalesApiDataSalesChannelInterface.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


