# SharedCatalogDataSharedCatalogInterface

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | ID. | [optional] 
**name** | **string** | Shared Catalog name. | 
**description** | **string** | Shared Catalog description. | 
**customer_group_id** | **int** | Customer Group Id. | 
**type** | **int** | Shared Catalog type. | 
**created_at** | **string** | Created time for Shared Catalog. | 
**created_by** | **int** | Admin id for Shared Catalog. | 
**store_id** | **int** | Store id for Shared Catalog. | 
**tax_class_id** | **int** | Tax class id. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


