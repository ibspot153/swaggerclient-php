# QuoteDataTotalsExtensionInterface

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**coupon_label** | **string** |  | [optional] 
**base_customer_balance_amount** | **float** |  | [optional] 
**customer_balance_amount** | **float** |  | [optional] 
**negotiable_quote_totals** | [**\Swagger\Client\Model\NegotiableQuoteDataNegotiableQuoteTotalsInterface**](NegotiableQuoteDataNegotiableQuoteTotalsInterface.md) |  | [optional] 
**reward_points_balance** | **float** |  | [optional] 
**reward_currency_amount** | **float** |  | [optional] 
**base_reward_currency_amount** | **float** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


