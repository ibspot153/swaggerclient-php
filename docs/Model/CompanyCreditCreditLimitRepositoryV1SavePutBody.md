# CompanyCreditCreditLimitRepositoryV1SavePutBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**credit_limit** | [**\Swagger\Client\Model\CompanyCreditDataCreditLimitInterface**](CompanyCreditDataCreditLimitInterface.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


