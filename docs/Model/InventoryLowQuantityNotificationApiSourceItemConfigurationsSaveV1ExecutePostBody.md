# InventoryLowQuantityNotificationApiSourceItemConfigurationsSaveV1ExecutePostBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**source_item_configurations** | [**\Swagger\Client\Model\InventoryLowQuantityNotificationApiDataSourceItemConfigurationInterface[]**](InventoryLowQuantityNotificationApiDataSourceItemConfigurationInterface.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


