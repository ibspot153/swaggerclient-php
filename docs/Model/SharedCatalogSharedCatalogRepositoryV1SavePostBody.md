# SharedCatalogSharedCatalogRepositoryV1SavePostBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**shared_catalog** | [**\Swagger\Client\Model\SharedCatalogDataSharedCatalogInterface**](SharedCatalogDataSharedCatalogInterface.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


