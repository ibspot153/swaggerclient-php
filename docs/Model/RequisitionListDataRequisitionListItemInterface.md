# RequisitionListDataRequisitionListItemInterface

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Requisition List ID. | 
**sku** | **int** | Product SKU. | 
**requisition_list_id** | **int** | Requisition List ID. | 
**qty** | **float** | Product Qty. | 
**options** | **string[]** | Requisition list item options. | 
**store_id** | **int** | Store ID. | 
**added_at** | **string** | Added_at value. | 
**extension_attributes** | [**\Swagger\Client\Model\RequisitionListDataRequisitionListItemExtensionInterface**](RequisitionListDataRequisitionListItemExtensionInterface.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


