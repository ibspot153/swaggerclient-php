# InventoryApiSourceRepositoryV1SavePutBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**source** | [**\Swagger\Client\Model\InventoryApiDataSourceInterface**](InventoryApiDataSourceInterface.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


