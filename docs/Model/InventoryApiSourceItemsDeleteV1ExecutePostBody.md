# InventoryApiSourceItemsDeleteV1ExecutePostBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**source_items** | [**\Swagger\Client\Model\InventoryApiDataSourceItemInterface[]**](InventoryApiDataSourceItemInterface.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


