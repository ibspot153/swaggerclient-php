# CustomerDataCustomerExtensionInterface

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**company_attributes** | [**\Swagger\Client\Model\CompanyDataCompanyCustomerInterface**](CompanyDataCompanyCustomerInterface.md) |  | [optional] 
**is_subscribed** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


