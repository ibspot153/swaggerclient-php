# AsynchronousOperationsDataOperationInterface

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**extension_attributes** | [**\Swagger\Client\Model\AsynchronousOperationsDataOperationExtensionInterface**](AsynchronousOperationsDataOperationExtensionInterface.md) |  | [optional] 
**id** | **int** | Id | 
**bulk_uuid** | **string** | Bulk uuid | 
**topic_name** | **string** | Queue Topic | 
**serialized_data** | **string** | Data | 
**result_serialized_data** | **string** | Serialized Data | 
**status** | **int** | Operation status | 
**result_message** | **string** | Result message | 
**error_code** | **int** | Error code | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


