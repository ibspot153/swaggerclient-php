# CompanyCreditDataHistoryDataInterface

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | ID. | [optional] 
**company_credit_id** | **int** | Company credit id. | [optional] 
**user_id** | **int** | User Id. | [optional] 
**user_type** | **int** | User type: integration, admin, customer. | [optional] 
**currency_credit** | **string** | Currency code of credit. | [optional] 
**currency_operation** | **string** | Currency code of operation. | [optional] 
**rate** | **float** | Currency rate between credit and operation currencies. | 
**rate_credit** | **float** | Rate between credit and base currencies. | [optional] 
**amount** | **float** | Amount. | 
**balance** | **float** | Outstanding balance. | 
**credit_limit** | **float** | Credit limit. | 
**available_limit** | **float** | Available limit. | [optional] 
**type** | **int** | Type of operation. | [optional] 
**datetime** | **string** | Operation datetime. | [optional] 
**purchase_order** | **string** | Purchase Order number. | [optional] 
**comment** | **string** | Comment. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


