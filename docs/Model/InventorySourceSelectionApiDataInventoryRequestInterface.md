# InventorySourceSelectionApiDataInventoryRequestInterface

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**stock_id** | **int** | Stock Id | 
**items** | [**\Swagger\Client\Model\InventorySourceSelectionApiDataItemRequestInterface[]**](InventorySourceSelectionApiDataItemRequestInterface.md) | Items | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


