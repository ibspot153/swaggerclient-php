# CompanyDataCompanyInterface

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Id. | [optional] 
**status** | **int** | Status. | [optional] 
**company_name** | **string** | Company name. | [optional] 
**legal_name** | **string** | Legal name. | [optional] 
**company_email** | **string** | Company email. | [optional] 
**vat_tax_id** | **string** | Vat tax id. | [optional] 
**reseller_id** | **string** | Reseller Id. | [optional] 
**comment** | **string** | Comment. | [optional] 
**street** | **string[]** | Street. | 
**city** | **string** | City. | [optional] 
**country_id** | **string** | Country. | [optional] 
**region** | **string** | Region. | [optional] 
**region_id** | **string** | Region Id. | [optional] 
**postcode** | **string** | Postcode. | [optional] 
**telephone** | **string** | Telephone. | [optional] 
**customer_group_id** | **int** | Customer Group Id. | 
**sales_representative_id** | **int** | Sales Representative Id. | 
**reject_reason** | **string** | Reject Reason. | 
**rejected_at** | **string** | Rejected at time. | 
**super_user_id** | **int** | Company admin customer id. | 
**extension_attributes** | [**\Swagger\Client\Model\CompanyDataCompanyExtensionInterface**](CompanyDataCompanyExtensionInterface.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


