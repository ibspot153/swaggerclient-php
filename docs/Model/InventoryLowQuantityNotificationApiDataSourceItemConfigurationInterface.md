# InventoryLowQuantityNotificationApiDataSourceItemConfigurationInterface

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**source_code** | **string** | Source code | [optional] 
**notify_stock_qty** | **float** | Notify stock qty | [optional] 
**sku** | **string** | SKU | [optional] 
**extension_attributes** | [**\Swagger\Client\Model\InventoryLowQuantityNotificationApiDataSourceItemConfigurationExtensionInterface**](InventoryLowQuantityNotificationApiDataSourceItemConfigurationExtensionInterface.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


