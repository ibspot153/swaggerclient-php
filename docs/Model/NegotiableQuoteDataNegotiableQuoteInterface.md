# NegotiableQuoteDataNegotiableQuoteInterface

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**quote_id** | **int** | Negotiable quote ID. | 
**is_regular_quote** | **bool** | Is regular quote. | 
**status** | **string** | Negotiable quote status. | 
**negotiated_price_type** | **int** | Negotiated price type. | 
**negotiated_price_value** | **float** | Negotiated price value. | 
**shipping_price** | **float** | Proposed shipping price. | 
**quote_name** | **string** | Negotiable quote name. | 
**expiration_period** | **string** | Expiration period. | 
**email_notification_status** | **int** | Email notification status. | 
**has_unconfirmed_changes** | **bool** | Has unconfirmed changes. | 
**is_shipping_tax_changed** | **bool** | Shipping tax changes. | 
**is_customer_price_changed** | **bool** | Customer price changes. | 
**notifications** | **int** | Quote notifications. | 
**applied_rule_ids** | **string** | Quote rules. | 
**is_address_draft** | **bool** | Is address draft. | 
**deleted_sku** | **string** | Deleted products sku. | 
**creator_id** | **int** | Quote creator id. | 
**creator_type** | **int** | Quote creator type. | 
**original_total_price** | **float** | Quote original total price. | [optional] 
**base_original_total_price** | **float** | Quote original total price in base currency. | [optional] 
**negotiated_total_price** | **float** | Quote negotiated total price. | [optional] 
**base_negotiated_total_price** | **float** | Quote negotiated total price in base currency. | [optional] 
**extension_attributes** | [**\Swagger\Client\Model\NegotiableQuoteDataNegotiableQuoteExtensionInterface**](NegotiableQuoteDataNegotiableQuoteExtensionInterface.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


