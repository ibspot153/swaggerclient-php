# AsynchronousOperationsDataDetailedBulkOperationsStatusInterface

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**operations_list** | [**\Swagger\Client\Model\AsynchronousOperationsDataOperationInterface[]**](AsynchronousOperationsDataOperationInterface.md) | Operations list. | 
**extension_attributes** | [**\Swagger\Client\Model\AsynchronousOperationsDataBulkSummaryExtensionInterface**](AsynchronousOperationsDataBulkSummaryExtensionInterface.md) |  | [optional] 
**bulk_id** | **string** | Bulk uuid | 
**description** | **string** | Bulk description | 
**start_time** | **string** | Bulk scheduled time | 
**user_id** | **int** | User id | 
**operation_count** | **int** | Total number of operations scheduled in scope of this bulk | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


