# SalesDataInvoiceExtensionInterface

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**base_customer_balance_amount** | **float** |  | [optional] 
**customer_balance_amount** | **float** |  | [optional] 
**base_gift_cards_amount** | **float** |  | [optional] 
**gift_cards_amount** | **float** |  | [optional] 
**gw_base_price** | **string** |  | [optional] 
**gw_price** | **string** |  | [optional] 
**gw_items_base_price** | **string** |  | [optional] 
**gw_items_price** | **string** |  | [optional] 
**gw_card_base_price** | **string** |  | [optional] 
**gw_card_price** | **string** |  | [optional] 
**gw_base_tax_amount** | **string** |  | [optional] 
**gw_tax_amount** | **string** |  | [optional] 
**gw_items_base_tax_amount** | **string** |  | [optional] 
**gw_items_tax_amount** | **string** |  | [optional] 
**gw_card_base_tax_amount** | **string** |  | [optional] 
**gw_card_tax_amount** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


