# InventorySalesApiDataSalesChannelInterface

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **string** | Sales channel type | [optional] 
**code** | **string** | Sales channel code | [optional] 
**extension_attributes** | [**\Swagger\Client\Model\InventorySalesApiDataSalesChannelExtensionInterface**](InventorySalesApiDataSalesChannelExtensionInterface.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


