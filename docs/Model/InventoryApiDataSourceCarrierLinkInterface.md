# InventoryApiDataSourceCarrierLinkInterface

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**carrier_code** | **string** | Carrier code | [optional] 
**position** | **int** | Position | [optional] 
**extension_attributes** | [**\Swagger\Client\Model\InventoryApiDataSourceCarrierLinkExtensionInterface**](InventoryApiDataSourceCarrierLinkExtensionInterface.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


