# InventoryCatalogApiBulkInventoryTransferV1ExecutePostBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**skus** | **string[]** |  | 
**origin_source** | **string** |  | 
**destination_source** | **string** |  | 
**unassign_from_origin** | **bool** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


