# CompanyCreditCreditBalanceManagementV1IncreasePostBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**value** | **float** |  | 
**currency** | **string** |  | 
**operation_type** | **int** |  | 
**comment** | **string** | [optional] | [optional] 
**options** | [**\Swagger\Client\Model\CompanyCreditDataCreditBalanceOptionsInterface**](CompanyCreditDataCreditBalanceOptionsInterface.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


