# InventoryApiDataSourceItemInterface

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sku** | **string** | Source item sku | [optional] 
**source_code** | **string** | Source code | [optional] 
**quantity** | **float** | Source item quantity | [optional] 
**status** | **int** | Source item status (One of self::STATUS_*) | [optional] 
**extension_attributes** | [**\Swagger\Client\Model\InventoryApiDataSourceItemExtensionInterface**](InventoryApiDataSourceItemExtensionInterface.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


