# InventoryApiDataStockInterface

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**stock_id** | **int** | Stock id | [optional] 
**name** | **string** | Stock name | [optional] 
**extension_attributes** | [**\Swagger\Client\Model\InventoryApiDataStockExtensionInterface**](InventoryApiDataStockExtensionInterface.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


