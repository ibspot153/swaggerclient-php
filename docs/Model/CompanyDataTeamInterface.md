# CompanyDataTeamInterface

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | ID | [optional] 
**name** | **string** | Name | [optional] 
**description** | **string** | Description | [optional] 
**extension_attributes** | [**\Swagger\Client\Model\CompanyDataTeamExtensionInterface**](CompanyDataTeamExtensionInterface.md) |  | [optional] 
**custom_attributes** | [**\Swagger\Client\Model\FrameworkAttributeInterface[]**](FrameworkAttributeInterface.md) | Custom attributes values. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


