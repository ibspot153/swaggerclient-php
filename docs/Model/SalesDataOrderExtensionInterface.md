# SalesDataOrderExtensionInterface

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**shipping_assignments** | [**\Swagger\Client\Model\SalesDataShippingAssignmentInterface[]**](SalesDataShippingAssignmentInterface.md) |  | [optional] 
**company_order_attributes** | [**\Swagger\Client\Model\CompanyDataCompanyOrderInterface**](CompanyDataCompanyOrderInterface.md) |  | [optional] 
**base_customer_balance_amount** | **float** |  | [optional] 
**customer_balance_amount** | **float** |  | [optional] 
**base_customer_balance_invoiced** | **float** |  | [optional] 
**customer_balance_invoiced** | **float** |  | [optional] 
**base_customer_balance_refunded** | **float** |  | [optional] 
**customer_balance_refunded** | **float** |  | [optional] 
**base_customer_balance_total_refunded** | **float** |  | [optional] 
**customer_balance_total_refunded** | **float** |  | [optional] 
**gift_cards** | [**\Swagger\Client\Model\GiftCardAccountDataGiftCardInterface[]**](GiftCardAccountDataGiftCardInterface.md) |  | [optional] 
**base_gift_cards_amount** | **float** |  | [optional] 
**gift_cards_amount** | **float** |  | [optional] 
**base_gift_cards_invoiced** | **float** |  | [optional] 
**gift_cards_invoiced** | **float** |  | [optional] 
**base_gift_cards_refunded** | **float** |  | [optional] 
**gift_cards_refunded** | **float** |  | [optional] 
**applied_taxes** | [**\Swagger\Client\Model\TaxDataOrderTaxDetailsAppliedTaxInterface[]**](TaxDataOrderTaxDetailsAppliedTaxInterface.md) |  | [optional] 
**item_applied_taxes** | [**\Swagger\Client\Model\TaxDataOrderTaxDetailsItemInterface[]**](TaxDataOrderTaxDetailsItemInterface.md) |  | [optional] 
**converting_from_quote** | **bool** |  | [optional] 
**gift_message** | [**\Swagger\Client\Model\GiftMessageDataMessageInterface**](GiftMessageDataMessageInterface.md) |  | [optional] 
**gw_id** | **string** |  | [optional] 
**gw_allow_gift_receipt** | **string** |  | [optional] 
**gw_add_card** | **string** |  | [optional] 
**gw_base_price** | **string** |  | [optional] 
**gw_price** | **string** |  | [optional] 
**gw_items_base_price** | **string** |  | [optional] 
**gw_items_price** | **string** |  | [optional] 
**gw_card_base_price** | **string** |  | [optional] 
**gw_card_price** | **string** |  | [optional] 
**gw_base_tax_amount** | **string** |  | [optional] 
**gw_tax_amount** | **string** |  | [optional] 
**gw_items_base_tax_amount** | **string** |  | [optional] 
**gw_items_tax_amount** | **string** |  | [optional] 
**gw_card_base_tax_amount** | **string** |  | [optional] 
**gw_card_tax_amount** | **string** |  | [optional] 
**gw_base_price_incl_tax** | **string** |  | [optional] 
**gw_price_incl_tax** | **string** |  | [optional] 
**gw_items_base_price_incl_tax** | **string** |  | [optional] 
**gw_items_price_incl_tax** | **string** |  | [optional] 
**gw_card_base_price_incl_tax** | **string** |  | [optional] 
**gw_card_price_incl_tax** | **string** |  | [optional] 
**gw_base_price_invoiced** | **string** |  | [optional] 
**gw_price_invoiced** | **string** |  | [optional] 
**gw_items_base_price_invoiced** | **string** |  | [optional] 
**gw_items_price_invoiced** | **string** |  | [optional] 
**gw_card_base_price_invoiced** | **string** |  | [optional] 
**gw_card_price_invoiced** | **string** |  | [optional] 
**gw_base_tax_amount_invoiced** | **string** |  | [optional] 
**gw_tax_amount_invoiced** | **string** |  | [optional] 
**gw_items_base_tax_invoiced** | **string** |  | [optional] 
**gw_items_tax_invoiced** | **string** |  | [optional] 
**gw_card_base_tax_invoiced** | **string** |  | [optional] 
**gw_card_tax_invoiced** | **string** |  | [optional] 
**gw_base_price_refunded** | **string** |  | [optional] 
**gw_price_refunded** | **string** |  | [optional] 
**gw_items_base_price_refunded** | **string** |  | [optional] 
**gw_items_price_refunded** | **string** |  | [optional] 
**gw_card_base_price_refunded** | **string** |  | [optional] 
**gw_card_price_refunded** | **string** |  | [optional] 
**gw_base_tax_amount_refunded** | **string** |  | [optional] 
**gw_tax_amount_refunded** | **string** |  | [optional] 
**gw_items_base_tax_refunded** | **string** |  | [optional] 
**gw_items_tax_refunded** | **string** |  | [optional] 
**gw_card_base_tax_refunded** | **string** |  | [optional] 
**gw_card_tax_refunded** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


