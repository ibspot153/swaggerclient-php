# NegotiableQuoteNegotiableQuoteManagementV1AdminSendPostBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**quote_id** | **int** |  | 
**comment** | **string** |  | [optional] 
**files** | [**\Swagger\Client\Model\NegotiableQuoteDataAttachmentContentInterface[]**](NegotiableQuoteDataAttachmentContentInterface.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


