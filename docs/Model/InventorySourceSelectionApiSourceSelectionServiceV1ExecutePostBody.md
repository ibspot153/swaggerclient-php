# InventorySourceSelectionApiSourceSelectionServiceV1ExecutePostBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**inventory_request** | [**\Swagger\Client\Model\InventorySourceSelectionApiDataInventoryRequestInterface**](InventorySourceSelectionApiDataInventoryRequestInterface.md) |  | 
**algorithm_code** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


