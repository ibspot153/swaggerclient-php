# CompanyDataHierarchyInterface

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**structure_id** | **int** | Structure ID. | [optional] 
**entity_id** | **int** | Entity ID. | [optional] 
**entity_type** | **string** | Entity type. | [optional] 
**structure_parent_id** | **int** | Structure parent ID. | [optional] 
**extension_attributes** | [**\Swagger\Client\Model\CompanyDataHierarchyExtensionInterface**](CompanyDataHierarchyExtensionInterface.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


