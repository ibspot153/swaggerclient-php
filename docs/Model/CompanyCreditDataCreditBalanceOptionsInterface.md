# CompanyCreditDataCreditBalanceOptionsInterface

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**purchase_order** | **string** | Purchase order number. | 
**order_increment** | **string** | Order increment. | 
**currency_display** | **string** | Currency display. | 
**currency_base** | **string** | Currency base. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


