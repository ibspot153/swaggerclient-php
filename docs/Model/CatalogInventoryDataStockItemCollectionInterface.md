# CatalogInventoryDataStockItemCollectionInterface

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**\Swagger\Client\Model\CatalogInventoryDataStockItemInterface[]**](CatalogInventoryDataStockItemInterface.md) | Items | 
**search_criteria** | [**\Swagger\Client\Model\CatalogInventoryStockItemCriteriaInterface**](CatalogInventoryStockItemCriteriaInterface.md) |  | 
**total_count** | **int** | Total count. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


