# CatalogDataSpecialPriceInterface

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**price** | **float** | Product special price value. | 
**store_id** | **int** | ID of store, that contains special price value. | 
**sku** | **string** | SKU of product, that contains special price value. | 
**price_from** | **string** | Start date for special price in Y-m-d H:i:s format. | 
**price_to** | **string** | End date for special price in Y-m-d H:i:s format. | 
**extension_attributes** | [**\Swagger\Client\Model\CatalogDataSpecialPriceExtensionInterface**](CatalogDataSpecialPriceExtensionInterface.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


