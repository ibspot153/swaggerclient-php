# NegotiableQuoteDataNegotiableQuoteItemTotalsInterface

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cost** | **float** | Cost for quote item. | 
**catalog_price** | **float** | Catalog price for quote item. | 
**base_catalog_price** | **float** | Catalog price for quote item in base currency. | 
**catalog_price_incl_tax** | **float** | Catalog price with included tax for quote item. | 
**base_catalog_price_incl_tax** | **float** | Catalog price with included tax for quote item in base currency. | 
**cart_price** | **float** | Cart price for quote item. | 
**base_cart_price** | **float** | Cart price for quote item in base currency. | 
**cart_tax** | **float** | Tax from catalog price for quote item. | 
**base_cart_tax** | **float** | Tax from catalog price for quote item in base currency. | 
**cart_price_incl_tax** | **float** | Cart price with included tax for quote item. | 
**base_cart_price_incl_tax** | **float** | Cart price with included tax for quote item in base currency. | 
**extension_attributes** | [**\Swagger\Client\Model\NegotiableQuoteDataNegotiableQuoteItemTotalsExtensionInterface**](NegotiableQuoteDataNegotiableQuoteItemTotalsExtensionInterface.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


