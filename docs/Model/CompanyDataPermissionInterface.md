# CompanyDataPermissionInterface

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Id. | [optional] 
**role_id** | **int** | Role id. | [optional] 
**resource_id** | **string** | Resource id. | 
**permission** | **string** | Permission. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


