# InventorySourceSelectionApiDataSourceSelectionResultInterface

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**source_selection_items** | [**\Swagger\Client\Model\InventorySourceSelectionApiDataSourceSelectionItemInterface[]**](InventorySourceSelectionApiDataSourceSelectionItemInterface.md) |  | 
**shippable** | **bool** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


