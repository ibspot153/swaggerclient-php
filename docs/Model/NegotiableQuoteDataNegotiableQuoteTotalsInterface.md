# NegotiableQuoteDataNegotiableQuoteTotalsInterface

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items_count** | **int** | The number of different items or products in the cart. | 
**quote_status** | **string** | Negotiable quote status. | 
**created_at** | **string** | The cart creation date and time. | 
**updated_at** | **string** | The cart last update date and time. | 
**customer_group** | **int** | Customer group id. | 
**base_to_quote_rate** | **float** | Base currency to quote currency rate. | 
**cost_total** | **float** | Total cost for quote. | 
**base_cost_total** | **float** | Total cost for quote in base currency. | 
**original_total** | **float** | Original quote total. | 
**base_original_total** | **float** | Original quote total in base currency. | 
**original_tax** | **float** | Original tax amount for quote. | 
**base_original_tax** | **float** | Original tax amount for quote in base currency. | 
**original_price_incl_tax** | **float** | Original price with included tax for quote. | 
**base_original_price_incl_tax** | **float** | Original price with included tax for quote in base currency. | 
**negotiated_price_type** | **int** | Negotiable quote type. | 
**negotiated_price_value** | **float** | Negotiable price value for quote. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


