# QuoteDataTotalsItemExtensionInterface

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**negotiable_quote_item_totals** | [**\Swagger\Client\Model\NegotiableQuoteDataNegotiableQuoteItemTotalsInterface**](NegotiableQuoteDataNegotiableQuoteItemTotalsInterface.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


