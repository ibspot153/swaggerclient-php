# NegotiableQuoteDataNegotiableQuoteItemInterface

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**item_id** | **int** | Quote item id | 
**original_price** | **float** | Quote item original price | 
**original_tax_amount** | **float** | Quote item original tax amount | 
**original_discount_amount** | **float** | Quote item original discount amount | 
**extension_attributes** | [**\Swagger\Client\Model\NegotiableQuoteDataNegotiableQuoteItemExtensionInterface**](NegotiableQuoteDataNegotiableQuoteItemExtensionInterface.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


