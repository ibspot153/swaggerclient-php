# CompanyDataCompanyExtensionInterface

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**applicable_payment_method** | **int** |  | [optional] 
**available_payment_methods** | **string** |  | [optional] 
**use_config_settings** | **int** |  | [optional] 
**quote_config** | [**\Swagger\Client\Model\NegotiableQuoteDataCompanyQuoteConfigInterface**](NegotiableQuoteDataCompanyQuoteConfigInterface.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


