# AsynchronousOperationsDataSummaryOperationStatusInterface

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Id | 
**status** | **int** | Operation status | 
**result_message** | **string** | Result message | 
**error_code** | **int** | Error code | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


