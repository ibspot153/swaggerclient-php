# CompanyDataRoleInterface

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Role id. | [optional] 
**role_name** | **string** | Role name. | [optional] 
**permissions** | [**\Swagger\Client\Model\CompanyDataPermissionInterface[]**](CompanyDataPermissionInterface.md) | Permissions. | 
**company_id** | **int** | Company id. | [optional] 
**extension_attributes** | [**\Swagger\Client\Model\CompanyDataRoleExtensionInterface**](CompanyDataRoleExtensionInterface.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


