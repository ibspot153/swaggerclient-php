# CatalogDataBasePriceInterface

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**price** | **float** | Price. | 
**store_id** | **int** | Store id. | 
**sku** | **string** | SKU. | 
**extension_attributes** | [**\Swagger\Client\Model\CatalogDataBasePriceExtensionInterface**](CatalogDataBasePriceExtensionInterface.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


