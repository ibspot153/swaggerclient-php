# RequisitionListDataRequisitionListInterface

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Requisition List ID | 
**customer_id** | **int** | Customer ID | 
**name** | **string** | Requisition List Name | 
**updated_at** | **string** | Requisition List Update Time | 
**description** | **string** | Requisition List Description | 
**items** | [**\Swagger\Client\Model\RequisitionListDataRequisitionListItemInterface[]**](RequisitionListDataRequisitionListItemInterface.md) | Requisition List Items | 
**extension_attributes** | [**\Swagger\Client\Model\RequisitionListDataRequisitionListExtensionInterface**](RequisitionListDataRequisitionListExtensionInterface.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


