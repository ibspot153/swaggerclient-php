# Swagger\Client\InventoryApiStockSourceLinksSaveV1Api

All URIs are relative to *http://magento2.vagrant12/rest/all*

Method | HTTP request | Description
------------- | ------------- | -------------
[**inventoryApiStockSourceLinksSaveV1ExecutePost**](InventoryApiStockSourceLinksSaveV1Api.md#inventoryApiStockSourceLinksSaveV1ExecutePost) | **POST** /V1/inventory/stock-source-links | 


# **inventoryApiStockSourceLinksSaveV1ExecutePost**
> \Swagger\Client\Model\ErrorResponse inventoryApiStockSourceLinksSaveV1ExecutePost($inventory_api_stock_source_links_save_v1_execute_post_body)



Save StockSourceLink list data

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\InventoryApiStockSourceLinksSaveV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$inventory_api_stock_source_links_save_v1_execute_post_body = new \Swagger\Client\Model\InventoryApiStockSourceLinksSaveV1ExecutePostBody(); // \Swagger\Client\Model\InventoryApiStockSourceLinksSaveV1ExecutePostBody | 

try {
    $result = $apiInstance->inventoryApiStockSourceLinksSaveV1ExecutePost($inventory_api_stock_source_links_save_v1_execute_post_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InventoryApiStockSourceLinksSaveV1Api->inventoryApiStockSourceLinksSaveV1ExecutePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inventory_api_stock_source_links_save_v1_execute_post_body** | [**\Swagger\Client\Model\InventoryApiStockSourceLinksSaveV1ExecutePostBody**](../Model/InventoryApiStockSourceLinksSaveV1ExecutePostBody.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\ErrorResponse**](../Model/ErrorResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

