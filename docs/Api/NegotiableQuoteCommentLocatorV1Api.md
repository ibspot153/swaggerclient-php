# Swagger\Client\NegotiableQuoteCommentLocatorV1Api

All URIs are relative to *http://magento2.vagrant12/rest/all*

Method | HTTP request | Description
------------- | ------------- | -------------
[**negotiableQuoteCommentLocatorV1GetListForQuoteGet**](NegotiableQuoteCommentLocatorV1Api.md#negotiableQuoteCommentLocatorV1GetListForQuoteGet) | **GET** /V1/negotiableQuote/{quoteId}/comments | 


# **negotiableQuoteCommentLocatorV1GetListForQuoteGet**
> \Swagger\Client\Model\NegotiableQuoteDataCommentInterface[] negotiableQuoteCommentLocatorV1GetListForQuoteGet($quote_id)



Returns comments for a specified negotiable quote.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\NegotiableQuoteCommentLocatorV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$quote_id = 56; // int | Negotiable Quote ID.

try {
    $result = $apiInstance->negotiableQuoteCommentLocatorV1GetListForQuoteGet($quote_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NegotiableQuoteCommentLocatorV1Api->negotiableQuoteCommentLocatorV1GetListForQuoteGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **quote_id** | **int**| Negotiable Quote ID. |

### Return type

[**\Swagger\Client\Model\NegotiableQuoteDataCommentInterface[]**](../Model/NegotiableQuoteDataCommentInterface.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

