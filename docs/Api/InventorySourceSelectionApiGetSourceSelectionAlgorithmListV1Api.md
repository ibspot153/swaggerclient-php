# Swagger\Client\InventorySourceSelectionApiGetSourceSelectionAlgorithmListV1Api

All URIs are relative to *http://magento2.vagrant12/rest/all*

Method | HTTP request | Description
------------- | ------------- | -------------
[**inventorySourceSelectionApiGetSourceSelectionAlgorithmListV1ExecuteGet**](InventorySourceSelectionApiGetSourceSelectionAlgorithmListV1Api.md#inventorySourceSelectionApiGetSourceSelectionAlgorithmListV1ExecuteGet) | **GET** /V1/inventory/source-selection-algorithm-list | 


# **inventorySourceSelectionApiGetSourceSelectionAlgorithmListV1ExecuteGet**
> \Swagger\Client\Model\InventorySourceSelectionApiDataSourceSelectionAlgorithmInterface[] inventorySourceSelectionApiGetSourceSelectionAlgorithmListV1ExecuteGet()





### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\InventorySourceSelectionApiGetSourceSelectionAlgorithmListV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->inventorySourceSelectionApiGetSourceSelectionAlgorithmListV1ExecuteGet();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InventorySourceSelectionApiGetSourceSelectionAlgorithmListV1Api->inventorySourceSelectionApiGetSourceSelectionAlgorithmListV1ExecuteGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\InventorySourceSelectionApiDataSourceSelectionAlgorithmInterface[]**](../Model/InventorySourceSelectionApiDataSourceSelectionAlgorithmInterface.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

