# Swagger\Client\SalesInvoiceOrderV1Api

All URIs are relative to *http://magento2.vagrant12/rest/all*

Method | HTTP request | Description
------------- | ------------- | -------------
[**salesInvoiceOrderV1ExecutePost**](SalesInvoiceOrderV1Api.md#salesInvoiceOrderV1ExecutePost) | **POST** /V1/order/{orderId}/invoice | 


# **salesInvoiceOrderV1ExecutePost**
> int salesInvoiceOrderV1ExecutePost($order_id, $sales_invoice_order_v1_execute_post_body)





### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\SalesInvoiceOrderV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$order_id = 56; // int | 
$sales_invoice_order_v1_execute_post_body = new \Swagger\Client\Model\SalesInvoiceOrderV1ExecutePostBody(); // \Swagger\Client\Model\SalesInvoiceOrderV1ExecutePostBody | 

try {
    $result = $apiInstance->salesInvoiceOrderV1ExecutePost($order_id, $sales_invoice_order_v1_execute_post_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SalesInvoiceOrderV1Api->salesInvoiceOrderV1ExecutePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **order_id** | **int**|  |
 **sales_invoice_order_v1_execute_post_body** | [**\Swagger\Client\Model\SalesInvoiceOrderV1ExecutePostBody**](../Model/SalesInvoiceOrderV1ExecutePostBody.md)|  | [optional]

### Return type

**int**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

