# Swagger\Client\InventoryApiStockSourceLinksDeleteV1Api

All URIs are relative to *http://magento2.vagrant12/rest/all*

Method | HTTP request | Description
------------- | ------------- | -------------
[**inventoryApiStockSourceLinksDeleteV1ExecutePost**](InventoryApiStockSourceLinksDeleteV1Api.md#inventoryApiStockSourceLinksDeleteV1ExecutePost) | **POST** /V1/inventory/stock-source-links-delete | 


# **inventoryApiStockSourceLinksDeleteV1ExecutePost**
> \Swagger\Client\Model\ErrorResponse inventoryApiStockSourceLinksDeleteV1ExecutePost($inventory_api_stock_source_links_delete_v1_execute_post_body)



Remove StockSourceLink list list

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\InventoryApiStockSourceLinksDeleteV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$inventory_api_stock_source_links_delete_v1_execute_post_body = new \Swagger\Client\Model\InventoryApiStockSourceLinksDeleteV1ExecutePostBody(); // \Swagger\Client\Model\InventoryApiStockSourceLinksDeleteV1ExecutePostBody | 

try {
    $result = $apiInstance->inventoryApiStockSourceLinksDeleteV1ExecutePost($inventory_api_stock_source_links_delete_v1_execute_post_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InventoryApiStockSourceLinksDeleteV1Api->inventoryApiStockSourceLinksDeleteV1ExecutePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inventory_api_stock_source_links_delete_v1_execute_post_body** | [**\Swagger\Client\Model\InventoryApiStockSourceLinksDeleteV1ExecutePostBody**](../Model/InventoryApiStockSourceLinksDeleteV1ExecutePostBody.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\ErrorResponse**](../Model/ErrorResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

