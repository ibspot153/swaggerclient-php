# Swagger\Client\CompanyTeamRepositoryV1Api

All URIs are relative to *http://magento2.vagrant12/rest/all*

Method | HTTP request | Description
------------- | ------------- | -------------
[**companyTeamRepositoryV1CreatePost**](CompanyTeamRepositoryV1Api.md#companyTeamRepositoryV1CreatePost) | **POST** /V1/team/{companyId} | 
[**companyTeamRepositoryV1DeleteByIdDelete**](CompanyTeamRepositoryV1Api.md#companyTeamRepositoryV1DeleteByIdDelete) | **DELETE** /V1/team/{teamId} | 
[**companyTeamRepositoryV1GetGet**](CompanyTeamRepositoryV1Api.md#companyTeamRepositoryV1GetGet) | **GET** /V1/team/{teamId} | 
[**companyTeamRepositoryV1GetListGet**](CompanyTeamRepositoryV1Api.md#companyTeamRepositoryV1GetListGet) | **GET** /V1/team/ | 
[**companyTeamRepositoryV1SavePut**](CompanyTeamRepositoryV1Api.md#companyTeamRepositoryV1SavePut) | **PUT** /V1/team/{teamId} | 


# **companyTeamRepositoryV1CreatePost**
> \Swagger\Client\Model\ErrorResponse companyTeamRepositoryV1CreatePost($company_id, $company_team_repository_v1_create_post_body)



Create a team in the company structure.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CompanyTeamRepositoryV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$company_id = 56; // int | 
$company_team_repository_v1_create_post_body = new \Swagger\Client\Model\CompanyTeamRepositoryV1CreatePostBody(); // \Swagger\Client\Model\CompanyTeamRepositoryV1CreatePostBody | 

try {
    $result = $apiInstance->companyTeamRepositoryV1CreatePost($company_id, $company_team_repository_v1_create_post_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CompanyTeamRepositoryV1Api->companyTeamRepositoryV1CreatePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **company_id** | **int**|  |
 **company_team_repository_v1_create_post_body** | [**\Swagger\Client\Model\CompanyTeamRepositoryV1CreatePostBody**](../Model/CompanyTeamRepositoryV1CreatePostBody.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\ErrorResponse**](../Model/ErrorResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **companyTeamRepositoryV1DeleteByIdDelete**
> \Swagger\Client\Model\ErrorResponse companyTeamRepositoryV1DeleteByIdDelete($team_id)



Delete a team from the company structure.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CompanyTeamRepositoryV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$team_id = 56; // int | 

try {
    $result = $apiInstance->companyTeamRepositoryV1DeleteByIdDelete($team_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CompanyTeamRepositoryV1Api->companyTeamRepositoryV1DeleteByIdDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **team_id** | **int**|  |

### Return type

[**\Swagger\Client\Model\ErrorResponse**](../Model/ErrorResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **companyTeamRepositoryV1GetGet**
> \Swagger\Client\Model\CompanyDataTeamInterface companyTeamRepositoryV1GetGet($team_id)



Returns data for a team in the company, by entity id.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CompanyTeamRepositoryV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$team_id = 56; // int | 

try {
    $result = $apiInstance->companyTeamRepositoryV1GetGet($team_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CompanyTeamRepositoryV1Api->companyTeamRepositoryV1GetGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **team_id** | **int**|  |

### Return type

[**\Swagger\Client\Model\CompanyDataTeamInterface**](../Model/CompanyDataTeamInterface.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **companyTeamRepositoryV1GetListGet**
> \Swagger\Client\Model\CompanyDataTeamSearchResultsInterface companyTeamRepositoryV1GetListGet($search_criteria_filter_groups_0_filters_0_field, $search_criteria_filter_groups_0_filters_0_value, $search_criteria_filter_groups_0_filters_0_condition_type, $search_criteria_sort_orders_0_field, $search_criteria_sort_orders_0_direction, $search_criteria_page_size, $search_criteria_current_page)



Returns the list of teams for the specified search criteria (team name or description).

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CompanyTeamRepositoryV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_criteria_filter_groups_0_filters_0_field = "search_criteria_filter_groups_0_filters_0_field_example"; // string | Field
$search_criteria_filter_groups_0_filters_0_value = "search_criteria_filter_groups_0_filters_0_value_example"; // string | Value
$search_criteria_filter_groups_0_filters_0_condition_type = "search_criteria_filter_groups_0_filters_0_condition_type_example"; // string | Condition type
$search_criteria_sort_orders_0_field = "search_criteria_sort_orders_0_field_example"; // string | Sorting field.
$search_criteria_sort_orders_0_direction = "search_criteria_sort_orders_0_direction_example"; // string | Sorting direction.
$search_criteria_page_size = 56; // int | Page size.
$search_criteria_current_page = 56; // int | Current page.

try {
    $result = $apiInstance->companyTeamRepositoryV1GetListGet($search_criteria_filter_groups_0_filters_0_field, $search_criteria_filter_groups_0_filters_0_value, $search_criteria_filter_groups_0_filters_0_condition_type, $search_criteria_sort_orders_0_field, $search_criteria_sort_orders_0_direction, $search_criteria_page_size, $search_criteria_current_page);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CompanyTeamRepositoryV1Api->companyTeamRepositoryV1GetListGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_criteria_filter_groups_0_filters_0_field** | **string**| Field | [optional]
 **search_criteria_filter_groups_0_filters_0_value** | **string**| Value | [optional]
 **search_criteria_filter_groups_0_filters_0_condition_type** | **string**| Condition type | [optional]
 **search_criteria_sort_orders_0_field** | **string**| Sorting field. | [optional]
 **search_criteria_sort_orders_0_direction** | **string**| Sorting direction. | [optional]
 **search_criteria_page_size** | **int**| Page size. | [optional]
 **search_criteria_current_page** | **int**| Current page. | [optional]

### Return type

[**\Swagger\Client\Model\CompanyDataTeamSearchResultsInterface**](../Model/CompanyDataTeamSearchResultsInterface.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **companyTeamRepositoryV1SavePut**
> bool companyTeamRepositoryV1SavePut($team_id, $company_team_repository_v1_save_put_body)



Update a team in the company structure.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CompanyTeamRepositoryV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$team_id = "team_id_example"; // string | 
$company_team_repository_v1_save_put_body = new \Swagger\Client\Model\CompanyTeamRepositoryV1SavePutBody(); // \Swagger\Client\Model\CompanyTeamRepositoryV1SavePutBody | 

try {
    $result = $apiInstance->companyTeamRepositoryV1SavePut($team_id, $company_team_repository_v1_save_put_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CompanyTeamRepositoryV1Api->companyTeamRepositoryV1SavePut: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **team_id** | **string**|  |
 **company_team_repository_v1_save_put_body** | [**\Swagger\Client\Model\CompanyTeamRepositoryV1SavePutBody**](../Model/CompanyTeamRepositoryV1SavePutBody.md)|  | [optional]

### Return type

**bool**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

