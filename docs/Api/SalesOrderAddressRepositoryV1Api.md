# Swagger\Client\SalesOrderAddressRepositoryV1Api

All URIs are relative to *http://magento2.vagrant12/rest/all*

Method | HTTP request | Description
------------- | ------------- | -------------
[**salesOrderAddressRepositoryV1SavePut**](SalesOrderAddressRepositoryV1Api.md#salesOrderAddressRepositoryV1SavePut) | **PUT** /V1/orders/{parent_id} | 


# **salesOrderAddressRepositoryV1SavePut**
> \Swagger\Client\Model\SalesDataOrderAddressInterface salesOrderAddressRepositoryV1SavePut($parent_id, $sales_order_address_repository_v1_save_put_body)



Performs persist operations for a specified order address.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\SalesOrderAddressRepositoryV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$parent_id = "parent_id_example"; // string | 
$sales_order_address_repository_v1_save_put_body = new \Swagger\Client\Model\SalesOrderAddressRepositoryV1SavePutBody(); // \Swagger\Client\Model\SalesOrderAddressRepositoryV1SavePutBody | 

try {
    $result = $apiInstance->salesOrderAddressRepositoryV1SavePut($parent_id, $sales_order_address_repository_v1_save_put_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SalesOrderAddressRepositoryV1Api->salesOrderAddressRepositoryV1SavePut: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **parent_id** | **string**|  |
 **sales_order_address_repository_v1_save_put_body** | [**\Swagger\Client\Model\SalesOrderAddressRepositoryV1SavePutBody**](../Model/SalesOrderAddressRepositoryV1SavePutBody.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\SalesDataOrderAddressInterface**](../Model/SalesDataOrderAddressInterface.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

