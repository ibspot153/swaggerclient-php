# Swagger\Client\IntegrationAdminTokenServiceV1Api

All URIs are relative to *http://magento2.vagrant12/rest/all*

Method | HTTP request | Description
------------- | ------------- | -------------
[**integrationAdminTokenServiceV1CreateAdminAccessTokenPost**](IntegrationAdminTokenServiceV1Api.md#integrationAdminTokenServiceV1CreateAdminAccessTokenPost) | **POST** /V1/integration/admin/token | 


# **integrationAdminTokenServiceV1CreateAdminAccessTokenPost**
> string integrationAdminTokenServiceV1CreateAdminAccessTokenPost($integration_admin_token_service_v1_create_admin_access_token_post_body)



Create access token for admin given the admin credentials.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\IntegrationAdminTokenServiceV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$integration_admin_token_service_v1_create_admin_access_token_post_body = new \Swagger\Client\Model\IntegrationAdminTokenServiceV1CreateAdminAccessTokenPostBody(); // \Swagger\Client\Model\IntegrationAdminTokenServiceV1CreateAdminAccessTokenPostBody | 

try {
    $result = $apiInstance->integrationAdminTokenServiceV1CreateAdminAccessTokenPost($integration_admin_token_service_v1_create_admin_access_token_post_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling IntegrationAdminTokenServiceV1Api->integrationAdminTokenServiceV1CreateAdminAccessTokenPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **integration_admin_token_service_v1_create_admin_access_token_post_body** | [**\Swagger\Client\Model\IntegrationAdminTokenServiceV1CreateAdminAccessTokenPostBody**](../Model/IntegrationAdminTokenServiceV1CreateAdminAccessTokenPostBody.md)|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

