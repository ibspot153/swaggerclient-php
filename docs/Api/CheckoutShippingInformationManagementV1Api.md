# Swagger\Client\CheckoutShippingInformationManagementV1Api

All URIs are relative to *http://magento2.vagrant12/rest/all*

Method | HTTP request | Description
------------- | ------------- | -------------
[**checkoutShippingInformationManagementV1SaveAddressInformationPost**](CheckoutShippingInformationManagementV1Api.md#checkoutShippingInformationManagementV1SaveAddressInformationPost) | **POST** /V1/carts/mine/shipping-information | 
[**checkoutShippingInformationManagementV1SaveAddressInformationPost_0**](CheckoutShippingInformationManagementV1Api.md#checkoutShippingInformationManagementV1SaveAddressInformationPost_0) | **POST** /V1/carts/{cartId}/shipping-information | 


# **checkoutShippingInformationManagementV1SaveAddressInformationPost**
> \Swagger\Client\Model\CheckoutDataPaymentDetailsInterface checkoutShippingInformationManagementV1SaveAddressInformationPost($checkout_shipping_information_management_v1_save_address_information_post_body)





### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CheckoutShippingInformationManagementV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$checkout_shipping_information_management_v1_save_address_information_post_body = new \Swagger\Client\Model\CheckoutShippingInformationManagementV1SaveAddressInformationPostBody(); // \Swagger\Client\Model\CheckoutShippingInformationManagementV1SaveAddressInformationPostBody | 

try {
    $result = $apiInstance->checkoutShippingInformationManagementV1SaveAddressInformationPost($checkout_shipping_information_management_v1_save_address_information_post_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CheckoutShippingInformationManagementV1Api->checkoutShippingInformationManagementV1SaveAddressInformationPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **checkout_shipping_information_management_v1_save_address_information_post_body** | [**\Swagger\Client\Model\CheckoutShippingInformationManagementV1SaveAddressInformationPostBody**](../Model/CheckoutShippingInformationManagementV1SaveAddressInformationPostBody.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\CheckoutDataPaymentDetailsInterface**](../Model/CheckoutDataPaymentDetailsInterface.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **checkoutShippingInformationManagementV1SaveAddressInformationPost_0**
> \Swagger\Client\Model\CheckoutDataPaymentDetailsInterface checkoutShippingInformationManagementV1SaveAddressInformationPost_0($cart_id, $checkout_shipping_information_management_v1_save_address_information_post_body)





### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CheckoutShippingInformationManagementV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$cart_id = 56; // int | 
$checkout_shipping_information_management_v1_save_address_information_post_body = new \Swagger\Client\Model\CheckoutShippingInformationManagementV1SaveAddressInformationPostBody1(); // \Swagger\Client\Model\CheckoutShippingInformationManagementV1SaveAddressInformationPostBody1 | 

try {
    $result = $apiInstance->checkoutShippingInformationManagementV1SaveAddressInformationPost_0($cart_id, $checkout_shipping_information_management_v1_save_address_information_post_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CheckoutShippingInformationManagementV1Api->checkoutShippingInformationManagementV1SaveAddressInformationPost_0: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cart_id** | **int**|  |
 **checkout_shipping_information_management_v1_save_address_information_post_body** | [**\Swagger\Client\Model\CheckoutShippingInformationManagementV1SaveAddressInformationPostBody1**](../Model/CheckoutShippingInformationManagementV1SaveAddressInformationPostBody1.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\CheckoutDataPaymentDetailsInterface**](../Model/CheckoutDataPaymentDetailsInterface.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

