# Swagger\Client\SalesRuleCouponManagementV1Api

All URIs are relative to *http://magento2.vagrant12/rest/all*

Method | HTTP request | Description
------------- | ------------- | -------------
[**salesRuleCouponManagementV1DeleteByCodesPost**](SalesRuleCouponManagementV1Api.md#salesRuleCouponManagementV1DeleteByCodesPost) | **POST** /V1/coupons/deleteByCodes | 
[**salesRuleCouponManagementV1DeleteByIdsPost**](SalesRuleCouponManagementV1Api.md#salesRuleCouponManagementV1DeleteByIdsPost) | **POST** /V1/coupons/deleteByIds | 
[**salesRuleCouponManagementV1GeneratePost**](SalesRuleCouponManagementV1Api.md#salesRuleCouponManagementV1GeneratePost) | **POST** /V1/coupons/generate | 


# **salesRuleCouponManagementV1DeleteByCodesPost**
> \Swagger\Client\Model\SalesRuleDataCouponMassDeleteResultInterface salesRuleCouponManagementV1DeleteByCodesPost($sales_rule_coupon_management_v1_delete_by_codes_post_body)



Delete coupon by coupon codes.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\SalesRuleCouponManagementV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$sales_rule_coupon_management_v1_delete_by_codes_post_body = new \Swagger\Client\Model\SalesRuleCouponManagementV1DeleteByCodesPostBody(); // \Swagger\Client\Model\SalesRuleCouponManagementV1DeleteByCodesPostBody | 

try {
    $result = $apiInstance->salesRuleCouponManagementV1DeleteByCodesPost($sales_rule_coupon_management_v1_delete_by_codes_post_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SalesRuleCouponManagementV1Api->salesRuleCouponManagementV1DeleteByCodesPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sales_rule_coupon_management_v1_delete_by_codes_post_body** | [**\Swagger\Client\Model\SalesRuleCouponManagementV1DeleteByCodesPostBody**](../Model/SalesRuleCouponManagementV1DeleteByCodesPostBody.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\SalesRuleDataCouponMassDeleteResultInterface**](../Model/SalesRuleDataCouponMassDeleteResultInterface.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **salesRuleCouponManagementV1DeleteByIdsPost**
> \Swagger\Client\Model\SalesRuleDataCouponMassDeleteResultInterface salesRuleCouponManagementV1DeleteByIdsPost($sales_rule_coupon_management_v1_delete_by_ids_post_body)



Delete coupon by coupon ids.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\SalesRuleCouponManagementV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$sales_rule_coupon_management_v1_delete_by_ids_post_body = new \Swagger\Client\Model\SalesRuleCouponManagementV1DeleteByIdsPostBody(); // \Swagger\Client\Model\SalesRuleCouponManagementV1DeleteByIdsPostBody | 

try {
    $result = $apiInstance->salesRuleCouponManagementV1DeleteByIdsPost($sales_rule_coupon_management_v1_delete_by_ids_post_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SalesRuleCouponManagementV1Api->salesRuleCouponManagementV1DeleteByIdsPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sales_rule_coupon_management_v1_delete_by_ids_post_body** | [**\Swagger\Client\Model\SalesRuleCouponManagementV1DeleteByIdsPostBody**](../Model/SalesRuleCouponManagementV1DeleteByIdsPostBody.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\SalesRuleDataCouponMassDeleteResultInterface**](../Model/SalesRuleDataCouponMassDeleteResultInterface.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **salesRuleCouponManagementV1GeneratePost**
> string[] salesRuleCouponManagementV1GeneratePost($sales_rule_coupon_management_v1_generate_post_body)



Generate coupon for a rule

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\SalesRuleCouponManagementV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$sales_rule_coupon_management_v1_generate_post_body = new \Swagger\Client\Model\SalesRuleCouponManagementV1GeneratePostBody(); // \Swagger\Client\Model\SalesRuleCouponManagementV1GeneratePostBody | 

try {
    $result = $apiInstance->salesRuleCouponManagementV1GeneratePost($sales_rule_coupon_management_v1_generate_post_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SalesRuleCouponManagementV1Api->salesRuleCouponManagementV1GeneratePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sales_rule_coupon_management_v1_generate_post_body** | [**\Swagger\Client\Model\SalesRuleCouponManagementV1GeneratePostBody**](../Model/SalesRuleCouponManagementV1GeneratePostBody.md)|  | [optional]

### Return type

**string[]**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

