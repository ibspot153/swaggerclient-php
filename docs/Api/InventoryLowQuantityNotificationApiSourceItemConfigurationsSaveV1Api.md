# Swagger\Client\InventoryLowQuantityNotificationApiSourceItemConfigurationsSaveV1Api

All URIs are relative to *http://magento2.vagrant12/rest/all*

Method | HTTP request | Description
------------- | ------------- | -------------
[**inventoryLowQuantityNotificationApiSourceItemConfigurationsSaveV1ExecutePost**](InventoryLowQuantityNotificationApiSourceItemConfigurationsSaveV1Api.md#inventoryLowQuantityNotificationApiSourceItemConfigurationsSaveV1ExecutePost) | **POST** /V1/inventory/low-quantity-notification | 


# **inventoryLowQuantityNotificationApiSourceItemConfigurationsSaveV1ExecutePost**
> \Swagger\Client\Model\ErrorResponse inventoryLowQuantityNotificationApiSourceItemConfigurationsSaveV1ExecutePost($inventory_low_quantity_notification_api_source_item_configurations_save_v1_execute_post_body)





### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\InventoryLowQuantityNotificationApiSourceItemConfigurationsSaveV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$inventory_low_quantity_notification_api_source_item_configurations_save_v1_execute_post_body = new \Swagger\Client\Model\InventoryLowQuantityNotificationApiSourceItemConfigurationsSaveV1ExecutePostBody(); // \Swagger\Client\Model\InventoryLowQuantityNotificationApiSourceItemConfigurationsSaveV1ExecutePostBody | 

try {
    $result = $apiInstance->inventoryLowQuantityNotificationApiSourceItemConfigurationsSaveV1ExecutePost($inventory_low_quantity_notification_api_source_item_configurations_save_v1_execute_post_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InventoryLowQuantityNotificationApiSourceItemConfigurationsSaveV1Api->inventoryLowQuantityNotificationApiSourceItemConfigurationsSaveV1ExecutePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inventory_low_quantity_notification_api_source_item_configurations_save_v1_execute_post_body** | [**\Swagger\Client\Model\InventoryLowQuantityNotificationApiSourceItemConfigurationsSaveV1ExecutePostBody**](../Model/InventoryLowQuantityNotificationApiSourceItemConfigurationsSaveV1ExecutePostBody.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\ErrorResponse**](../Model/ErrorResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

