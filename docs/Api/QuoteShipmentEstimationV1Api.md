# Swagger\Client\QuoteShipmentEstimationV1Api

All URIs are relative to *http://magento2.vagrant12/rest/all*

Method | HTTP request | Description
------------- | ------------- | -------------
[**quoteShipmentEstimationV1EstimateByExtendedAddressPost**](QuoteShipmentEstimationV1Api.md#quoteShipmentEstimationV1EstimateByExtendedAddressPost) | **POST** /V1/carts/{cartId}/estimate-shipping-methods | 
[**quoteShipmentEstimationV1EstimateByExtendedAddressPost_0**](QuoteShipmentEstimationV1Api.md#quoteShipmentEstimationV1EstimateByExtendedAddressPost_0) | **POST** /V1/carts/mine/estimate-shipping-methods | 


# **quoteShipmentEstimationV1EstimateByExtendedAddressPost**
> \Swagger\Client\Model\QuoteDataShippingMethodInterface[] quoteShipmentEstimationV1EstimateByExtendedAddressPost($cart_id, $quote_shipment_estimation_v1_estimate_by_extended_address_post_body)



Estimate shipping by address and return list of available shipping methods

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\QuoteShipmentEstimationV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$cart_id = "cart_id_example"; // string | 
$quote_shipment_estimation_v1_estimate_by_extended_address_post_body = new \Swagger\Client\Model\QuoteShipmentEstimationV1EstimateByExtendedAddressPostBody(); // \Swagger\Client\Model\QuoteShipmentEstimationV1EstimateByExtendedAddressPostBody | 

try {
    $result = $apiInstance->quoteShipmentEstimationV1EstimateByExtendedAddressPost($cart_id, $quote_shipment_estimation_v1_estimate_by_extended_address_post_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling QuoteShipmentEstimationV1Api->quoteShipmentEstimationV1EstimateByExtendedAddressPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cart_id** | **string**|  |
 **quote_shipment_estimation_v1_estimate_by_extended_address_post_body** | [**\Swagger\Client\Model\QuoteShipmentEstimationV1EstimateByExtendedAddressPostBody**](../Model/QuoteShipmentEstimationV1EstimateByExtendedAddressPostBody.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\QuoteDataShippingMethodInterface[]**](../Model/QuoteDataShippingMethodInterface.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **quoteShipmentEstimationV1EstimateByExtendedAddressPost_0**
> \Swagger\Client\Model\QuoteDataShippingMethodInterface[] quoteShipmentEstimationV1EstimateByExtendedAddressPost_0($quote_shipment_estimation_v1_estimate_by_extended_address_post_body)



Estimate shipping by address and return list of available shipping methods

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\QuoteShipmentEstimationV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$quote_shipment_estimation_v1_estimate_by_extended_address_post_body = new \Swagger\Client\Model\QuoteShipmentEstimationV1EstimateByExtendedAddressPostBody1(); // \Swagger\Client\Model\QuoteShipmentEstimationV1EstimateByExtendedAddressPostBody1 | 

try {
    $result = $apiInstance->quoteShipmentEstimationV1EstimateByExtendedAddressPost_0($quote_shipment_estimation_v1_estimate_by_extended_address_post_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling QuoteShipmentEstimationV1Api->quoteShipmentEstimationV1EstimateByExtendedAddressPost_0: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **quote_shipment_estimation_v1_estimate_by_extended_address_post_body** | [**\Swagger\Client\Model\QuoteShipmentEstimationV1EstimateByExtendedAddressPostBody1**](../Model/QuoteShipmentEstimationV1EstimateByExtendedAddressPostBody1.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\QuoteDataShippingMethodInterface[]**](../Model/QuoteDataShippingMethodInterface.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

