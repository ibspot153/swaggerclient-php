# Swagger\Client\CheckoutTotalsInformationManagementV1Api

All URIs are relative to *http://magento2.vagrant12/rest/all*

Method | HTTP request | Description
------------- | ------------- | -------------
[**checkoutTotalsInformationManagementV1CalculatePost**](CheckoutTotalsInformationManagementV1Api.md#checkoutTotalsInformationManagementV1CalculatePost) | **POST** /V1/carts/{cartId}/totals-information | 
[**checkoutTotalsInformationManagementV1CalculatePost_0**](CheckoutTotalsInformationManagementV1Api.md#checkoutTotalsInformationManagementV1CalculatePost_0) | **POST** /V1/carts/mine/totals-information | 


# **checkoutTotalsInformationManagementV1CalculatePost**
> \Swagger\Client\Model\QuoteDataTotalsInterface checkoutTotalsInformationManagementV1CalculatePost($cart_id, $checkout_totals_information_management_v1_calculate_post_body)



Calculate quote totals based on address and shipping method.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CheckoutTotalsInformationManagementV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$cart_id = 56; // int | 
$checkout_totals_information_management_v1_calculate_post_body = new \Swagger\Client\Model\CheckoutTotalsInformationManagementV1CalculatePostBody(); // \Swagger\Client\Model\CheckoutTotalsInformationManagementV1CalculatePostBody | 

try {
    $result = $apiInstance->checkoutTotalsInformationManagementV1CalculatePost($cart_id, $checkout_totals_information_management_v1_calculate_post_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CheckoutTotalsInformationManagementV1Api->checkoutTotalsInformationManagementV1CalculatePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cart_id** | **int**|  |
 **checkout_totals_information_management_v1_calculate_post_body** | [**\Swagger\Client\Model\CheckoutTotalsInformationManagementV1CalculatePostBody**](../Model/CheckoutTotalsInformationManagementV1CalculatePostBody.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\QuoteDataTotalsInterface**](../Model/QuoteDataTotalsInterface.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **checkoutTotalsInformationManagementV1CalculatePost_0**
> \Swagger\Client\Model\QuoteDataTotalsInterface checkoutTotalsInformationManagementV1CalculatePost_0($checkout_totals_information_management_v1_calculate_post_body)



Calculate quote totals based on address and shipping method.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CheckoutTotalsInformationManagementV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$checkout_totals_information_management_v1_calculate_post_body = new \Swagger\Client\Model\CheckoutTotalsInformationManagementV1CalculatePostBody1(); // \Swagger\Client\Model\CheckoutTotalsInformationManagementV1CalculatePostBody1 | 

try {
    $result = $apiInstance->checkoutTotalsInformationManagementV1CalculatePost_0($checkout_totals_information_management_v1_calculate_post_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CheckoutTotalsInformationManagementV1Api->checkoutTotalsInformationManagementV1CalculatePost_0: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **checkout_totals_information_management_v1_calculate_post_body** | [**\Swagger\Client\Model\CheckoutTotalsInformationManagementV1CalculatePostBody1**](../Model/CheckoutTotalsInformationManagementV1CalculatePostBody1.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\QuoteDataTotalsInterface**](../Model/QuoteDataTotalsInterface.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

