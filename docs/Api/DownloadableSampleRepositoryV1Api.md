# Swagger\Client\DownloadableSampleRepositoryV1Api

All URIs are relative to *http://magento2.vagrant12/rest/all*

Method | HTTP request | Description
------------- | ------------- | -------------
[**downloadableSampleRepositoryV1DeleteDelete**](DownloadableSampleRepositoryV1Api.md#downloadableSampleRepositoryV1DeleteDelete) | **DELETE** /V1/products/downloadable-links/samples/{id} | 
[**downloadableSampleRepositoryV1GetListGet**](DownloadableSampleRepositoryV1Api.md#downloadableSampleRepositoryV1GetListGet) | **GET** /V1/products/{sku}/downloadable-links/samples | 
[**downloadableSampleRepositoryV1SavePost**](DownloadableSampleRepositoryV1Api.md#downloadableSampleRepositoryV1SavePost) | **POST** /V1/products/{sku}/downloadable-links/samples | 
[**downloadableSampleRepositoryV1SavePut**](DownloadableSampleRepositoryV1Api.md#downloadableSampleRepositoryV1SavePut) | **PUT** /V1/products/{sku}/downloadable-links/samples/{id} | 


# **downloadableSampleRepositoryV1DeleteDelete**
> bool downloadableSampleRepositoryV1DeleteDelete($id)



Delete downloadable sample

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DownloadableSampleRepositoryV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | 

try {
    $result = $apiInstance->downloadableSampleRepositoryV1DeleteDelete($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DownloadableSampleRepositoryV1Api->downloadableSampleRepositoryV1DeleteDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

**bool**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **downloadableSampleRepositoryV1GetListGet**
> \Swagger\Client\Model\DownloadableDataSampleInterface[] downloadableSampleRepositoryV1GetListGet($sku)



List of samples for downloadable product

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DownloadableSampleRepositoryV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$sku = "sku_example"; // string | 

try {
    $result = $apiInstance->downloadableSampleRepositoryV1GetListGet($sku);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DownloadableSampleRepositoryV1Api->downloadableSampleRepositoryV1GetListGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sku** | **string**|  |

### Return type

[**\Swagger\Client\Model\DownloadableDataSampleInterface[]**](../Model/DownloadableDataSampleInterface.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **downloadableSampleRepositoryV1SavePost**
> int downloadableSampleRepositoryV1SavePost($sku, $downloadable_sample_repository_v1_save_post_body)



Update downloadable sample of the given product

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DownloadableSampleRepositoryV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$sku = "sku_example"; // string | 
$downloadable_sample_repository_v1_save_post_body = new \Swagger\Client\Model\DownloadableSampleRepositoryV1SavePostBody(); // \Swagger\Client\Model\DownloadableSampleRepositoryV1SavePostBody | 

try {
    $result = $apiInstance->downloadableSampleRepositoryV1SavePost($sku, $downloadable_sample_repository_v1_save_post_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DownloadableSampleRepositoryV1Api->downloadableSampleRepositoryV1SavePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sku** | **string**|  |
 **downloadable_sample_repository_v1_save_post_body** | [**\Swagger\Client\Model\DownloadableSampleRepositoryV1SavePostBody**](../Model/DownloadableSampleRepositoryV1SavePostBody.md)|  | [optional]

### Return type

**int**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **downloadableSampleRepositoryV1SavePut**
> int downloadableSampleRepositoryV1SavePut($sku, $id, $downloadable_sample_repository_v1_save_put_body)



Update downloadable sample of the given product

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DownloadableSampleRepositoryV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$sku = "sku_example"; // string | 
$id = "id_example"; // string | 
$downloadable_sample_repository_v1_save_put_body = new \Swagger\Client\Model\DownloadableSampleRepositoryV1SavePutBody(); // \Swagger\Client\Model\DownloadableSampleRepositoryV1SavePutBody | 

try {
    $result = $apiInstance->downloadableSampleRepositoryV1SavePut($sku, $id, $downloadable_sample_repository_v1_save_put_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DownloadableSampleRepositoryV1Api->downloadableSampleRepositoryV1SavePut: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sku** | **string**|  |
 **id** | **string**|  |
 **downloadable_sample_repository_v1_save_put_body** | [**\Swagger\Client\Model\DownloadableSampleRepositoryV1SavePutBody**](../Model/DownloadableSampleRepositoryV1SavePutBody.md)|  | [optional]

### Return type

**int**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

