# Swagger\Client\CompanyRoleRepositoryV1Api

All URIs are relative to *http://magento2.vagrant12/rest/all*

Method | HTTP request | Description
------------- | ------------- | -------------
[**companyRoleRepositoryV1DeleteDelete**](CompanyRoleRepositoryV1Api.md#companyRoleRepositoryV1DeleteDelete) | **DELETE** /V1/company/role/{roleId} | 
[**companyRoleRepositoryV1GetGet**](CompanyRoleRepositoryV1Api.md#companyRoleRepositoryV1GetGet) | **GET** /V1/company/role/{roleId} | 
[**companyRoleRepositoryV1GetListGet**](CompanyRoleRepositoryV1Api.md#companyRoleRepositoryV1GetListGet) | **GET** /V1/company/role/ | 
[**companyRoleRepositoryV1SavePost**](CompanyRoleRepositoryV1Api.md#companyRoleRepositoryV1SavePost) | **POST** /V1/company/role/ | 
[**companyRoleRepositoryV1SavePut**](CompanyRoleRepositoryV1Api.md#companyRoleRepositoryV1SavePut) | **PUT** /V1/company/role/{id} | 


# **companyRoleRepositoryV1DeleteDelete**
> bool companyRoleRepositoryV1DeleteDelete($role_id)



Delete a role.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CompanyRoleRepositoryV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$role_id = 56; // int | 

try {
    $result = $apiInstance->companyRoleRepositoryV1DeleteDelete($role_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CompanyRoleRepositoryV1Api->companyRoleRepositoryV1DeleteDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **role_id** | **int**|  |

### Return type

**bool**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **companyRoleRepositoryV1GetGet**
> \Swagger\Client\Model\CompanyDataRoleInterface companyRoleRepositoryV1GetGet($role_id)



Returns the list of permissions for a specified role.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CompanyRoleRepositoryV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$role_id = 56; // int | 

try {
    $result = $apiInstance->companyRoleRepositoryV1GetGet($role_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CompanyRoleRepositoryV1Api->companyRoleRepositoryV1GetGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **role_id** | **int**|  |

### Return type

[**\Swagger\Client\Model\CompanyDataRoleInterface**](../Model/CompanyDataRoleInterface.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **companyRoleRepositoryV1GetListGet**
> \Swagger\Client\Model\CompanyDataRoleSearchResultsInterface companyRoleRepositoryV1GetListGet($search_criteria_filter_groups_0_filters_0_field, $search_criteria_filter_groups_0_filters_0_value, $search_criteria_filter_groups_0_filters_0_condition_type, $search_criteria_sort_orders_0_field, $search_criteria_sort_orders_0_direction, $search_criteria_page_size, $search_criteria_current_page)



Returns the list of roles and permissions for a specified company.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CompanyRoleRepositoryV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_criteria_filter_groups_0_filters_0_field = "search_criteria_filter_groups_0_filters_0_field_example"; // string | Field
$search_criteria_filter_groups_0_filters_0_value = "search_criteria_filter_groups_0_filters_0_value_example"; // string | Value
$search_criteria_filter_groups_0_filters_0_condition_type = "search_criteria_filter_groups_0_filters_0_condition_type_example"; // string | Condition type
$search_criteria_sort_orders_0_field = "search_criteria_sort_orders_0_field_example"; // string | Sorting field.
$search_criteria_sort_orders_0_direction = "search_criteria_sort_orders_0_direction_example"; // string | Sorting direction.
$search_criteria_page_size = 56; // int | Page size.
$search_criteria_current_page = 56; // int | Current page.

try {
    $result = $apiInstance->companyRoleRepositoryV1GetListGet($search_criteria_filter_groups_0_filters_0_field, $search_criteria_filter_groups_0_filters_0_value, $search_criteria_filter_groups_0_filters_0_condition_type, $search_criteria_sort_orders_0_field, $search_criteria_sort_orders_0_direction, $search_criteria_page_size, $search_criteria_current_page);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CompanyRoleRepositoryV1Api->companyRoleRepositoryV1GetListGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_criteria_filter_groups_0_filters_0_field** | **string**| Field | [optional]
 **search_criteria_filter_groups_0_filters_0_value** | **string**| Value | [optional]
 **search_criteria_filter_groups_0_filters_0_condition_type** | **string**| Condition type | [optional]
 **search_criteria_sort_orders_0_field** | **string**| Sorting field. | [optional]
 **search_criteria_sort_orders_0_direction** | **string**| Sorting direction. | [optional]
 **search_criteria_page_size** | **int**| Page size. | [optional]
 **search_criteria_current_page** | **int**| Current page. | [optional]

### Return type

[**\Swagger\Client\Model\CompanyDataRoleSearchResultsInterface**](../Model/CompanyDataRoleSearchResultsInterface.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **companyRoleRepositoryV1SavePost**
> \Swagger\Client\Model\CompanyDataRoleInterface companyRoleRepositoryV1SavePost($company_role_repository_v1_save_post_body)



Create or update a role for a selected company.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CompanyRoleRepositoryV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$company_role_repository_v1_save_post_body = new \Swagger\Client\Model\CompanyRoleRepositoryV1SavePostBody(); // \Swagger\Client\Model\CompanyRoleRepositoryV1SavePostBody | 

try {
    $result = $apiInstance->companyRoleRepositoryV1SavePost($company_role_repository_v1_save_post_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CompanyRoleRepositoryV1Api->companyRoleRepositoryV1SavePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **company_role_repository_v1_save_post_body** | [**\Swagger\Client\Model\CompanyRoleRepositoryV1SavePostBody**](../Model/CompanyRoleRepositoryV1SavePostBody.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\CompanyDataRoleInterface**](../Model/CompanyDataRoleInterface.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **companyRoleRepositoryV1SavePut**
> \Swagger\Client\Model\CompanyDataRoleInterface companyRoleRepositoryV1SavePut($id, $company_role_repository_v1_save_put_body)



Create or update a role for a selected company.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CompanyRoleRepositoryV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | 
$company_role_repository_v1_save_put_body = new \Swagger\Client\Model\CompanyRoleRepositoryV1SavePutBody(); // \Swagger\Client\Model\CompanyRoleRepositoryV1SavePutBody | 

try {
    $result = $apiInstance->companyRoleRepositoryV1SavePut($id, $company_role_repository_v1_save_put_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CompanyRoleRepositoryV1Api->companyRoleRepositoryV1SavePut: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**|  |
 **company_role_repository_v1_save_put_body** | [**\Swagger\Client\Model\CompanyRoleRepositoryV1SavePutBody**](../Model/CompanyRoleRepositoryV1SavePutBody.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\CompanyDataRoleInterface**](../Model/CompanyDataRoleInterface.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

