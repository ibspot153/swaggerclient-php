# Swagger\Client\InventoryLowQuantityNotificationApiGetSourceItemConfigurationV1Api

All URIs are relative to *http://magento2.vagrant12/rest/all*

Method | HTTP request | Description
------------- | ------------- | -------------
[**inventoryLowQuantityNotificationApiGetSourceItemConfigurationV1ExecuteGet**](InventoryLowQuantityNotificationApiGetSourceItemConfigurationV1Api.md#inventoryLowQuantityNotificationApiGetSourceItemConfigurationV1ExecuteGet) | **GET** /V1/inventory/low-quantity-notification/{sourceCode}/{sku} | 


# **inventoryLowQuantityNotificationApiGetSourceItemConfigurationV1ExecuteGet**
> \Swagger\Client\Model\InventoryLowQuantityNotificationApiDataSourceItemConfigurationInterface inventoryLowQuantityNotificationApiGetSourceItemConfigurationV1ExecuteGet($source_code, $sku)



Get the source item configuration

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\InventoryLowQuantityNotificationApiGetSourceItemConfigurationV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$source_code = "source_code_example"; // string | 
$sku = "sku_example"; // string | 

try {
    $result = $apiInstance->inventoryLowQuantityNotificationApiGetSourceItemConfigurationV1ExecuteGet($source_code, $sku);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InventoryLowQuantityNotificationApiGetSourceItemConfigurationV1Api->inventoryLowQuantityNotificationApiGetSourceItemConfigurationV1ExecuteGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **source_code** | **string**|  |
 **sku** | **string**|  |

### Return type

[**\Swagger\Client\Model\InventoryLowQuantityNotificationApiDataSourceItemConfigurationInterface**](../Model/InventoryLowQuantityNotificationApiDataSourceItemConfigurationInterface.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

