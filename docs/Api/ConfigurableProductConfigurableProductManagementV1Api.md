# Swagger\Client\ConfigurableProductConfigurableProductManagementV1Api

All URIs are relative to *http://magento2.vagrant12/rest/all*

Method | HTTP request | Description
------------- | ------------- | -------------
[**configurableProductConfigurableProductManagementV1GenerateVariationPut**](ConfigurableProductConfigurableProductManagementV1Api.md#configurableProductConfigurableProductManagementV1GenerateVariationPut) | **PUT** /V1/configurable-products/variation | 


# **configurableProductConfigurableProductManagementV1GenerateVariationPut**
> \Swagger\Client\Model\CatalogDataProductInterface[] configurableProductConfigurableProductManagementV1GenerateVariationPut($configurable_product_configurable_product_management_v1_generate_variation_put_body)



Generate variation based on same product

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ConfigurableProductConfigurableProductManagementV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$configurable_product_configurable_product_management_v1_generate_variation_put_body = new \Swagger\Client\Model\ConfigurableProductConfigurableProductManagementV1GenerateVariationPutBody(); // \Swagger\Client\Model\ConfigurableProductConfigurableProductManagementV1GenerateVariationPutBody | 

try {
    $result = $apiInstance->configurableProductConfigurableProductManagementV1GenerateVariationPut($configurable_product_configurable_product_management_v1_generate_variation_put_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ConfigurableProductConfigurableProductManagementV1Api->configurableProductConfigurableProductManagementV1GenerateVariationPut: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **configurable_product_configurable_product_management_v1_generate_variation_put_body** | [**\Swagger\Client\Model\ConfigurableProductConfigurableProductManagementV1GenerateVariationPutBody**](../Model/ConfigurableProductConfigurableProductManagementV1GenerateVariationPutBody.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\CatalogDataProductInterface[]**](../Model/CatalogDataProductInterface.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

