# Swagger\Client\CatalogTierPriceStorageV1Api

All URIs are relative to *http://magento2.vagrant12/rest/all*

Method | HTTP request | Description
------------- | ------------- | -------------
[**catalogTierPriceStorageV1DeletePost**](CatalogTierPriceStorageV1Api.md#catalogTierPriceStorageV1DeletePost) | **POST** /V1/products/tier-prices-delete | 
[**catalogTierPriceStorageV1GetPost**](CatalogTierPriceStorageV1Api.md#catalogTierPriceStorageV1GetPost) | **POST** /V1/products/tier-prices-information | 
[**catalogTierPriceStorageV1ReplacePut**](CatalogTierPriceStorageV1Api.md#catalogTierPriceStorageV1ReplacePut) | **PUT** /V1/products/tier-prices | 
[**catalogTierPriceStorageV1UpdatePost**](CatalogTierPriceStorageV1Api.md#catalogTierPriceStorageV1UpdatePost) | **POST** /V1/products/tier-prices | 


# **catalogTierPriceStorageV1DeletePost**
> \Swagger\Client\Model\CatalogDataPriceUpdateResultInterface[] catalogTierPriceStorageV1DeletePost($catalog_tier_price_storage_v1_delete_post_body)



Delete product tier prices. If any items will have invalid price, price type, website id, sku, customer group or quantity, they will be marked as failed and excluded from delete list and \\Magento\\Catalog\\Api\\Data\\PriceUpdateResultInterface[] with problem description will be returned. If there were no failed items during update empty array will be returned. If error occurred during the update exception will be thrown.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CatalogTierPriceStorageV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$catalog_tier_price_storage_v1_delete_post_body = new \Swagger\Client\Model\CatalogTierPriceStorageV1DeletePostBody(); // \Swagger\Client\Model\CatalogTierPriceStorageV1DeletePostBody | 

try {
    $result = $apiInstance->catalogTierPriceStorageV1DeletePost($catalog_tier_price_storage_v1_delete_post_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogTierPriceStorageV1Api->catalogTierPriceStorageV1DeletePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **catalog_tier_price_storage_v1_delete_post_body** | [**\Swagger\Client\Model\CatalogTierPriceStorageV1DeletePostBody**](../Model/CatalogTierPriceStorageV1DeletePostBody.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\CatalogDataPriceUpdateResultInterface[]**](../Model/CatalogDataPriceUpdateResultInterface.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogTierPriceStorageV1GetPost**
> \Swagger\Client\Model\CatalogDataTierPriceInterface[] catalogTierPriceStorageV1GetPost($catalog_tier_price_storage_v1_get_post_body)



Return product prices. In case of at least one of skus is not found exception will be thrown.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CatalogTierPriceStorageV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$catalog_tier_price_storage_v1_get_post_body = new \Swagger\Client\Model\CatalogTierPriceStorageV1GetPostBody(); // \Swagger\Client\Model\CatalogTierPriceStorageV1GetPostBody | 

try {
    $result = $apiInstance->catalogTierPriceStorageV1GetPost($catalog_tier_price_storage_v1_get_post_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogTierPriceStorageV1Api->catalogTierPriceStorageV1GetPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **catalog_tier_price_storage_v1_get_post_body** | [**\Swagger\Client\Model\CatalogTierPriceStorageV1GetPostBody**](../Model/CatalogTierPriceStorageV1GetPostBody.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\CatalogDataTierPriceInterface[]**](../Model/CatalogDataTierPriceInterface.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogTierPriceStorageV1ReplacePut**
> \Swagger\Client\Model\CatalogDataPriceUpdateResultInterface[] catalogTierPriceStorageV1ReplacePut($catalog_tier_price_storage_v1_replace_put_body)



Remove existing tier prices and replace them with the new ones. If any items will have invalid price, price type, website id, sku, customer group or quantity, they will be marked as failed and excluded from replace list and \\Magento\\Catalog\\Api\\Data\\PriceUpdateResultInterface[] with problem description will be returned. If there were no failed items during update empty array will be returned. If error occurred during the update exception will be thrown.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CatalogTierPriceStorageV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$catalog_tier_price_storage_v1_replace_put_body = new \Swagger\Client\Model\CatalogTierPriceStorageV1ReplacePutBody(); // \Swagger\Client\Model\CatalogTierPriceStorageV1ReplacePutBody | 

try {
    $result = $apiInstance->catalogTierPriceStorageV1ReplacePut($catalog_tier_price_storage_v1_replace_put_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogTierPriceStorageV1Api->catalogTierPriceStorageV1ReplacePut: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **catalog_tier_price_storage_v1_replace_put_body** | [**\Swagger\Client\Model\CatalogTierPriceStorageV1ReplacePutBody**](../Model/CatalogTierPriceStorageV1ReplacePutBody.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\CatalogDataPriceUpdateResultInterface[]**](../Model/CatalogDataPriceUpdateResultInterface.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogTierPriceStorageV1UpdatePost**
> \Swagger\Client\Model\CatalogDataPriceUpdateResultInterface[] catalogTierPriceStorageV1UpdatePost($catalog_tier_price_storage_v1_update_post_body)



Add or update product prices. If any items will have invalid price, price type, website id, sku, customer group or quantity, they will be marked as failed and excluded from update list and \\Magento\\Catalog\\Api\\Data\\PriceUpdateResultInterface[] with problem description will be returned. If there were no failed items during update empty array will be returned. If error occurred during the update exception will be thrown.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CatalogTierPriceStorageV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$catalog_tier_price_storage_v1_update_post_body = new \Swagger\Client\Model\CatalogTierPriceStorageV1UpdatePostBody(); // \Swagger\Client\Model\CatalogTierPriceStorageV1UpdatePostBody | 

try {
    $result = $apiInstance->catalogTierPriceStorageV1UpdatePost($catalog_tier_price_storage_v1_update_post_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogTierPriceStorageV1Api->catalogTierPriceStorageV1UpdatePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **catalog_tier_price_storage_v1_update_post_body** | [**\Swagger\Client\Model\CatalogTierPriceStorageV1UpdatePostBody**](../Model/CatalogTierPriceStorageV1UpdatePostBody.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\CatalogDataPriceUpdateResultInterface[]**](../Model/CatalogDataPriceUpdateResultInterface.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

