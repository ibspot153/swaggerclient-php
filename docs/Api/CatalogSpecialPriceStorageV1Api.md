# Swagger\Client\CatalogSpecialPriceStorageV1Api

All URIs are relative to *http://magento2.vagrant12/rest/all*

Method | HTTP request | Description
------------- | ------------- | -------------
[**catalogSpecialPriceStorageV1DeletePost**](CatalogSpecialPriceStorageV1Api.md#catalogSpecialPriceStorageV1DeletePost) | **POST** /V1/products/special-price-delete | 
[**catalogSpecialPriceStorageV1GetPost**](CatalogSpecialPriceStorageV1Api.md#catalogSpecialPriceStorageV1GetPost) | **POST** /V1/products/special-price-information | 
[**catalogSpecialPriceStorageV1UpdatePost**](CatalogSpecialPriceStorageV1Api.md#catalogSpecialPriceStorageV1UpdatePost) | **POST** /V1/products/special-price | 


# **catalogSpecialPriceStorageV1DeletePost**
> \Swagger\Client\Model\CatalogDataPriceUpdateResultInterface[] catalogSpecialPriceStorageV1DeletePost($catalog_special_price_storage_v1_delete_post_body)



Delete product's special price. If any items will have invalid price, store id, sku or dates, they will be marked as failed and excluded from delete list and \\Magento\\Catalog\\Api\\Data\\PriceUpdateResultInterface[] with problem description will be returned. If there were no failed items during update empty array will be returned. If error occurred during the delete exception will be thrown.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CatalogSpecialPriceStorageV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$catalog_special_price_storage_v1_delete_post_body = new \Swagger\Client\Model\CatalogSpecialPriceStorageV1DeletePostBody(); // \Swagger\Client\Model\CatalogSpecialPriceStorageV1DeletePostBody | 

try {
    $result = $apiInstance->catalogSpecialPriceStorageV1DeletePost($catalog_special_price_storage_v1_delete_post_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogSpecialPriceStorageV1Api->catalogSpecialPriceStorageV1DeletePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **catalog_special_price_storage_v1_delete_post_body** | [**\Swagger\Client\Model\CatalogSpecialPriceStorageV1DeletePostBody**](../Model/CatalogSpecialPriceStorageV1DeletePostBody.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\CatalogDataPriceUpdateResultInterface[]**](../Model/CatalogDataPriceUpdateResultInterface.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogSpecialPriceStorageV1GetPost**
> \Swagger\Client\Model\CatalogDataSpecialPriceInterface[] catalogSpecialPriceStorageV1GetPost($catalog_special_price_storage_v1_get_post_body)



Return product's special price. In case of at least one of skus is not found exception will be thrown.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CatalogSpecialPriceStorageV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$catalog_special_price_storage_v1_get_post_body = new \Swagger\Client\Model\CatalogSpecialPriceStorageV1GetPostBody(); // \Swagger\Client\Model\CatalogSpecialPriceStorageV1GetPostBody | 

try {
    $result = $apiInstance->catalogSpecialPriceStorageV1GetPost($catalog_special_price_storage_v1_get_post_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogSpecialPriceStorageV1Api->catalogSpecialPriceStorageV1GetPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **catalog_special_price_storage_v1_get_post_body** | [**\Swagger\Client\Model\CatalogSpecialPriceStorageV1GetPostBody**](../Model/CatalogSpecialPriceStorageV1GetPostBody.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\CatalogDataSpecialPriceInterface[]**](../Model/CatalogDataSpecialPriceInterface.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogSpecialPriceStorageV1UpdatePost**
> \Swagger\Client\Model\CatalogDataPriceUpdateResultInterface[] catalogSpecialPriceStorageV1UpdatePost($catalog_special_price_storage_v1_update_post_body)



Add or update product's special price. If any items will have invalid price, store id, sku or dates, they will be marked as failed and excluded from update list and \\Magento\\Catalog\\Api\\Data\\PriceUpdateResultInterface[] with problem description will be returned. If there were no failed items during update empty array will be returned. If error occurred during the update exception will be thrown.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CatalogSpecialPriceStorageV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$catalog_special_price_storage_v1_update_post_body = new \Swagger\Client\Model\CatalogSpecialPriceStorageV1UpdatePostBody(); // \Swagger\Client\Model\CatalogSpecialPriceStorageV1UpdatePostBody | 

try {
    $result = $apiInstance->catalogSpecialPriceStorageV1UpdatePost($catalog_special_price_storage_v1_update_post_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogSpecialPriceStorageV1Api->catalogSpecialPriceStorageV1UpdatePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **catalog_special_price_storage_v1_update_post_body** | [**\Swagger\Client\Model\CatalogSpecialPriceStorageV1UpdatePostBody**](../Model/CatalogSpecialPriceStorageV1UpdatePostBody.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\CatalogDataPriceUpdateResultInterface[]**](../Model/CatalogDataPriceUpdateResultInterface.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

