# Swagger\Client\RequisitionListRequisitionListRepositoryV1Api

All URIs are relative to *http://magento2.vagrant12/rest/all*

Method | HTTP request | Description
------------- | ------------- | -------------
[**requisitionListRequisitionListRepositoryV1SavePost**](RequisitionListRequisitionListRepositoryV1Api.md#requisitionListRequisitionListRepositoryV1SavePost) | **POST** /V1/requisition_lists | 


# **requisitionListRequisitionListRepositoryV1SavePost**
> \Swagger\Client\Model\RequisitionListDataRequisitionListInterface requisitionListRequisitionListRepositoryV1SavePost($requisition_list_requisition_list_repository_v1_save_post_body)



Save Requisition List

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\RequisitionListRequisitionListRepositoryV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$requisition_list_requisition_list_repository_v1_save_post_body = new \Swagger\Client\Model\RequisitionListRequisitionListRepositoryV1SavePostBody(); // \Swagger\Client\Model\RequisitionListRequisitionListRepositoryV1SavePostBody | 

try {
    $result = $apiInstance->requisitionListRequisitionListRepositoryV1SavePost($requisition_list_requisition_list_repository_v1_save_post_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RequisitionListRequisitionListRepositoryV1Api->requisitionListRequisitionListRepositoryV1SavePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requisition_list_requisition_list_repository_v1_save_post_body** | [**\Swagger\Client\Model\RequisitionListRequisitionListRepositoryV1SavePostBody**](../Model/RequisitionListRequisitionListRepositoryV1SavePostBody.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\RequisitionListDataRequisitionListInterface**](../Model/RequisitionListDataRequisitionListInterface.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

