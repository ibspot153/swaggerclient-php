# Swagger\Client\NegotiableQuotePaymentInformationManagementV1Api

All URIs are relative to *http://magento2.vagrant12/rest/all*

Method | HTTP request | Description
------------- | ------------- | -------------
[**negotiableQuotePaymentInformationManagementV1GetPaymentInformationGet**](NegotiableQuotePaymentInformationManagementV1Api.md#negotiableQuotePaymentInformationManagementV1GetPaymentInformationGet) | **GET** /V1/negotiable-carts/{cartId}/payment-information | 
[**negotiableQuotePaymentInformationManagementV1SavePaymentInformationAndPlaceOrderPost**](NegotiableQuotePaymentInformationManagementV1Api.md#negotiableQuotePaymentInformationManagementV1SavePaymentInformationAndPlaceOrderPost) | **POST** /V1/negotiable-carts/{cartId}/payment-information | 
[**negotiableQuotePaymentInformationManagementV1SavePaymentInformationPost**](NegotiableQuotePaymentInformationManagementV1Api.md#negotiableQuotePaymentInformationManagementV1SavePaymentInformationPost) | **POST** /V1/negotiable-carts/{cartId}/set-payment-information | 


# **negotiableQuotePaymentInformationManagementV1GetPaymentInformationGet**
> \Swagger\Client\Model\CheckoutDataPaymentDetailsInterface negotiableQuotePaymentInformationManagementV1GetPaymentInformationGet($cart_id)



Get payment information

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\NegotiableQuotePaymentInformationManagementV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$cart_id = 56; // int | 

try {
    $result = $apiInstance->negotiableQuotePaymentInformationManagementV1GetPaymentInformationGet($cart_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NegotiableQuotePaymentInformationManagementV1Api->negotiableQuotePaymentInformationManagementV1GetPaymentInformationGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cart_id** | **int**|  |

### Return type

[**\Swagger\Client\Model\CheckoutDataPaymentDetailsInterface**](../Model/CheckoutDataPaymentDetailsInterface.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **negotiableQuotePaymentInformationManagementV1SavePaymentInformationAndPlaceOrderPost**
> int negotiableQuotePaymentInformationManagementV1SavePaymentInformationAndPlaceOrderPost($cart_id, $negotiable_quote_payment_information_management_v1_save_payment_information_and_place_order_post_body)



Set payment information and place order for a specified cart.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\NegotiableQuotePaymentInformationManagementV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$cart_id = 56; // int | 
$negotiable_quote_payment_information_management_v1_save_payment_information_and_place_order_post_body = new \Swagger\Client\Model\NegotiableQuotePaymentInformationManagementV1SavePaymentInformationAndPlaceOrderPostBody(); // \Swagger\Client\Model\NegotiableQuotePaymentInformationManagementV1SavePaymentInformationAndPlaceOrderPostBody | 

try {
    $result = $apiInstance->negotiableQuotePaymentInformationManagementV1SavePaymentInformationAndPlaceOrderPost($cart_id, $negotiable_quote_payment_information_management_v1_save_payment_information_and_place_order_post_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NegotiableQuotePaymentInformationManagementV1Api->negotiableQuotePaymentInformationManagementV1SavePaymentInformationAndPlaceOrderPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cart_id** | **int**|  |
 **negotiable_quote_payment_information_management_v1_save_payment_information_and_place_order_post_body** | [**\Swagger\Client\Model\NegotiableQuotePaymentInformationManagementV1SavePaymentInformationAndPlaceOrderPostBody**](../Model/NegotiableQuotePaymentInformationManagementV1SavePaymentInformationAndPlaceOrderPostBody.md)|  | [optional]

### Return type

**int**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **negotiableQuotePaymentInformationManagementV1SavePaymentInformationPost**
> int negotiableQuotePaymentInformationManagementV1SavePaymentInformationPost($cart_id, $negotiable_quote_payment_information_management_v1_save_payment_information_post_body)



Set payment information for a specified cart.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\NegotiableQuotePaymentInformationManagementV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$cart_id = 56; // int | 
$negotiable_quote_payment_information_management_v1_save_payment_information_post_body = new \Swagger\Client\Model\NegotiableQuotePaymentInformationManagementV1SavePaymentInformationPostBody(); // \Swagger\Client\Model\NegotiableQuotePaymentInformationManagementV1SavePaymentInformationPostBody | 

try {
    $result = $apiInstance->negotiableQuotePaymentInformationManagementV1SavePaymentInformationPost($cart_id, $negotiable_quote_payment_information_management_v1_save_payment_information_post_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NegotiableQuotePaymentInformationManagementV1Api->negotiableQuotePaymentInformationManagementV1SavePaymentInformationPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cart_id** | **int**|  |
 **negotiable_quote_payment_information_management_v1_save_payment_information_post_body** | [**\Swagger\Client\Model\NegotiableQuotePaymentInformationManagementV1SavePaymentInformationPostBody**](../Model/NegotiableQuotePaymentInformationManagementV1SavePaymentInformationPostBody.md)|  | [optional]

### Return type

**int**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

