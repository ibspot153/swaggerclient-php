# Swagger\Client\InventoryCatalogApiBulkSourceUnassignV1Api

All URIs are relative to *http://magento2.vagrant12/rest/all*

Method | HTTP request | Description
------------- | ------------- | -------------
[**inventoryCatalogApiBulkSourceUnassignV1ExecutePost**](InventoryCatalogApiBulkSourceUnassignV1Api.md#inventoryCatalogApiBulkSourceUnassignV1ExecutePost) | **POST** /V1/inventory/bulk-product-source-unassign | 


# **inventoryCatalogApiBulkSourceUnassignV1ExecutePost**
> int inventoryCatalogApiBulkSourceUnassignV1ExecutePost($inventory_catalog_api_bulk_source_unassign_v1_execute_post_body)



Run mass product to source un-assignment

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\InventoryCatalogApiBulkSourceUnassignV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$inventory_catalog_api_bulk_source_unassign_v1_execute_post_body = new \Swagger\Client\Model\InventoryCatalogApiBulkSourceUnassignV1ExecutePostBody(); // \Swagger\Client\Model\InventoryCatalogApiBulkSourceUnassignV1ExecutePostBody | 

try {
    $result = $apiInstance->inventoryCatalogApiBulkSourceUnassignV1ExecutePost($inventory_catalog_api_bulk_source_unassign_v1_execute_post_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InventoryCatalogApiBulkSourceUnassignV1Api->inventoryCatalogApiBulkSourceUnassignV1ExecutePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inventory_catalog_api_bulk_source_unassign_v1_execute_post_body** | [**\Swagger\Client\Model\InventoryCatalogApiBulkSourceUnassignV1ExecutePostBody**](../Model/InventoryCatalogApiBulkSourceUnassignV1ExecutePostBody.md)|  | [optional]

### Return type

**int**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

