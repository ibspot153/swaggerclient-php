# Swagger\Client\GiftRegistryShippingMethodManagementV1Api

All URIs are relative to *http://magento2.vagrant12/rest/all*

Method | HTTP request | Description
------------- | ------------- | -------------
[**giftRegistryShippingMethodManagementV1EstimateByRegistryIdPost**](GiftRegistryShippingMethodManagementV1Api.md#giftRegistryShippingMethodManagementV1EstimateByRegistryIdPost) | **POST** /V1/giftregistry/mine/estimate-shipping-methods | 


# **giftRegistryShippingMethodManagementV1EstimateByRegistryIdPost**
> \Swagger\Client\Model\QuoteDataShippingMethodInterface[] giftRegistryShippingMethodManagementV1EstimateByRegistryIdPost($gift_registry_shipping_method_management_v1_estimate_by_registry_id_post_body)



Estimate shipping

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\GiftRegistryShippingMethodManagementV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$gift_registry_shipping_method_management_v1_estimate_by_registry_id_post_body = new \Swagger\Client\Model\GiftRegistryShippingMethodManagementV1EstimateByRegistryIdPostBody(); // \Swagger\Client\Model\GiftRegistryShippingMethodManagementV1EstimateByRegistryIdPostBody | 

try {
    $result = $apiInstance->giftRegistryShippingMethodManagementV1EstimateByRegistryIdPost($gift_registry_shipping_method_management_v1_estimate_by_registry_id_post_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GiftRegistryShippingMethodManagementV1Api->giftRegistryShippingMethodManagementV1EstimateByRegistryIdPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **gift_registry_shipping_method_management_v1_estimate_by_registry_id_post_body** | [**\Swagger\Client\Model\GiftRegistryShippingMethodManagementV1EstimateByRegistryIdPostBody**](../Model/GiftRegistryShippingMethodManagementV1EstimateByRegistryIdPostBody.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\QuoteDataShippingMethodInterface[]**](../Model/QuoteDataShippingMethodInterface.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

