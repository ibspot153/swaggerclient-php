# Swagger\Client\CompanyCreditCreditHistoryManagementV1Api

All URIs are relative to *http://magento2.vagrant12/rest/all*

Method | HTTP request | Description
------------- | ------------- | -------------
[**companyCreditCreditHistoryManagementV1GetListGet**](CompanyCreditCreditHistoryManagementV1Api.md#companyCreditCreditHistoryManagementV1GetListGet) | **GET** /V1/companyCredits/history | 
[**companyCreditCreditHistoryManagementV1UpdatePut**](CompanyCreditCreditHistoryManagementV1Api.md#companyCreditCreditHistoryManagementV1UpdatePut) | **PUT** /V1/companyCredits/history/{historyId} | 


# **companyCreditCreditHistoryManagementV1GetListGet**
> \Swagger\Client\Model\CompanyCreditDataHistorySearchResultsInterface companyCreditCreditHistoryManagementV1GetListGet($search_criteria_filter_groups_0_filters_0_field, $search_criteria_filter_groups_0_filters_0_value, $search_criteria_filter_groups_0_filters_0_condition_type, $search_criteria_sort_orders_0_field, $search_criteria_sort_orders_0_direction, $search_criteria_page_size, $search_criteria_current_page)



Returns the credit history for one or more companies.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CompanyCreditCreditHistoryManagementV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_criteria_filter_groups_0_filters_0_field = "search_criteria_filter_groups_0_filters_0_field_example"; // string | Field
$search_criteria_filter_groups_0_filters_0_value = "search_criteria_filter_groups_0_filters_0_value_example"; // string | Value
$search_criteria_filter_groups_0_filters_0_condition_type = "search_criteria_filter_groups_0_filters_0_condition_type_example"; // string | Condition type
$search_criteria_sort_orders_0_field = "search_criteria_sort_orders_0_field_example"; // string | Sorting field.
$search_criteria_sort_orders_0_direction = "search_criteria_sort_orders_0_direction_example"; // string | Sorting direction.
$search_criteria_page_size = 56; // int | Page size.
$search_criteria_current_page = 56; // int | Current page.

try {
    $result = $apiInstance->companyCreditCreditHistoryManagementV1GetListGet($search_criteria_filter_groups_0_filters_0_field, $search_criteria_filter_groups_0_filters_0_value, $search_criteria_filter_groups_0_filters_0_condition_type, $search_criteria_sort_orders_0_field, $search_criteria_sort_orders_0_direction, $search_criteria_page_size, $search_criteria_current_page);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CompanyCreditCreditHistoryManagementV1Api->companyCreditCreditHistoryManagementV1GetListGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_criteria_filter_groups_0_filters_0_field** | **string**| Field | [optional]
 **search_criteria_filter_groups_0_filters_0_value** | **string**| Value | [optional]
 **search_criteria_filter_groups_0_filters_0_condition_type** | **string**| Condition type | [optional]
 **search_criteria_sort_orders_0_field** | **string**| Sorting field. | [optional]
 **search_criteria_sort_orders_0_direction** | **string**| Sorting direction. | [optional]
 **search_criteria_page_size** | **int**| Page size. | [optional]
 **search_criteria_current_page** | **int**| Current page. | [optional]

### Return type

[**\Swagger\Client\Model\CompanyCreditDataHistorySearchResultsInterface**](../Model/CompanyCreditDataHistorySearchResultsInterface.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **companyCreditCreditHistoryManagementV1UpdatePut**
> bool companyCreditCreditHistoryManagementV1UpdatePut($history_id, $company_credit_credit_history_management_v1_update_put_body)



Update the PO Number and/or comment for a Reimburse transaction.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CompanyCreditCreditHistoryManagementV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$history_id = 56; // int | 
$company_credit_credit_history_management_v1_update_put_body = new \Swagger\Client\Model\CompanyCreditCreditHistoryManagementV1UpdatePutBody(); // \Swagger\Client\Model\CompanyCreditCreditHistoryManagementV1UpdatePutBody | 

try {
    $result = $apiInstance->companyCreditCreditHistoryManagementV1UpdatePut($history_id, $company_credit_credit_history_management_v1_update_put_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CompanyCreditCreditHistoryManagementV1Api->companyCreditCreditHistoryManagementV1UpdatePut: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **history_id** | **int**|  |
 **company_credit_credit_history_management_v1_update_put_body** | [**\Swagger\Client\Model\CompanyCreditCreditHistoryManagementV1UpdatePutBody**](../Model/CompanyCreditCreditHistoryManagementV1UpdatePutBody.md)|  | [optional]

### Return type

**bool**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

