# Swagger\Client\QuoteCartTotalManagementV1Api

All URIs are relative to *http://magento2.vagrant12/rest/all*

Method | HTTP request | Description
------------- | ------------- | -------------
[**quoteCartTotalManagementV1CollectTotalsPut**](QuoteCartTotalManagementV1Api.md#quoteCartTotalManagementV1CollectTotalsPut) | **PUT** /V1/carts/mine/collect-totals | 


# **quoteCartTotalManagementV1CollectTotalsPut**
> \Swagger\Client\Model\QuoteDataTotalsInterface quoteCartTotalManagementV1CollectTotalsPut($quote_cart_total_management_v1_collect_totals_put_body)



Set shipping/billing methods and additional data for cart and collect totals.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\QuoteCartTotalManagementV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$quote_cart_total_management_v1_collect_totals_put_body = new \Swagger\Client\Model\QuoteCartTotalManagementV1CollectTotalsPutBody(); // \Swagger\Client\Model\QuoteCartTotalManagementV1CollectTotalsPutBody | 

try {
    $result = $apiInstance->quoteCartTotalManagementV1CollectTotalsPut($quote_cart_total_management_v1_collect_totals_put_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling QuoteCartTotalManagementV1Api->quoteCartTotalManagementV1CollectTotalsPut: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **quote_cart_total_management_v1_collect_totals_put_body** | [**\Swagger\Client\Model\QuoteCartTotalManagementV1CollectTotalsPutBody**](../Model/QuoteCartTotalManagementV1CollectTotalsPutBody.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\QuoteDataTotalsInterface**](../Model/QuoteDataTotalsInterface.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

