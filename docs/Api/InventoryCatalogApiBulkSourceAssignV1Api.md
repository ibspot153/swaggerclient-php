# Swagger\Client\InventoryCatalogApiBulkSourceAssignV1Api

All URIs are relative to *http://magento2.vagrant12/rest/all*

Method | HTTP request | Description
------------- | ------------- | -------------
[**inventoryCatalogApiBulkSourceAssignV1ExecutePost**](InventoryCatalogApiBulkSourceAssignV1Api.md#inventoryCatalogApiBulkSourceAssignV1ExecutePost) | **POST** /V1/inventory/bulk-product-source-assign | 


# **inventoryCatalogApiBulkSourceAssignV1ExecutePost**
> int inventoryCatalogApiBulkSourceAssignV1ExecutePost($inventory_catalog_api_bulk_source_assign_v1_execute_post_body)



Run mass product to source assignment

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\InventoryCatalogApiBulkSourceAssignV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$inventory_catalog_api_bulk_source_assign_v1_execute_post_body = new \Swagger\Client\Model\InventoryCatalogApiBulkSourceAssignV1ExecutePostBody(); // \Swagger\Client\Model\InventoryCatalogApiBulkSourceAssignV1ExecutePostBody | 

try {
    $result = $apiInstance->inventoryCatalogApiBulkSourceAssignV1ExecutePost($inventory_catalog_api_bulk_source_assign_v1_execute_post_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InventoryCatalogApiBulkSourceAssignV1Api->inventoryCatalogApiBulkSourceAssignV1ExecutePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inventory_catalog_api_bulk_source_assign_v1_execute_post_body** | [**\Swagger\Client\Model\InventoryCatalogApiBulkSourceAssignV1ExecutePostBody**](../Model/InventoryCatalogApiBulkSourceAssignV1ExecutePostBody.md)|  | [optional]

### Return type

**int**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

