# Swagger\Client\NegotiableQuoteGiftCardAccountManagementV1Api

All URIs are relative to *http://magento2.vagrant12/rest/all*

Method | HTTP request | Description
------------- | ------------- | -------------
[**negotiableQuoteGiftCardAccountManagementV1DeleteByQuoteIdDelete**](NegotiableQuoteGiftCardAccountManagementV1Api.md#negotiableQuoteGiftCardAccountManagementV1DeleteByQuoteIdDelete) | **DELETE** /V1/negotiable-carts/{cartId}/giftCards/{giftCardCode} | 
[**negotiableQuoteGiftCardAccountManagementV1SaveByQuoteIdPost**](NegotiableQuoteGiftCardAccountManagementV1Api.md#negotiableQuoteGiftCardAccountManagementV1SaveByQuoteIdPost) | **POST** /V1/negotiable-carts/{cartId}/giftCards | 


# **negotiableQuoteGiftCardAccountManagementV1DeleteByQuoteIdDelete**
> bool negotiableQuoteGiftCardAccountManagementV1DeleteByQuoteIdDelete($cart_id, $gift_card_code)



Remove GiftCard Account entity

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\NegotiableQuoteGiftCardAccountManagementV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$cart_id = 56; // int | 
$gift_card_code = "gift_card_code_example"; // string | 

try {
    $result = $apiInstance->negotiableQuoteGiftCardAccountManagementV1DeleteByQuoteIdDelete($cart_id, $gift_card_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NegotiableQuoteGiftCardAccountManagementV1Api->negotiableQuoteGiftCardAccountManagementV1DeleteByQuoteIdDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cart_id** | **int**|  |
 **gift_card_code** | **string**|  |

### Return type

**bool**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **negotiableQuoteGiftCardAccountManagementV1SaveByQuoteIdPost**
> bool negotiableQuoteGiftCardAccountManagementV1SaveByQuoteIdPost($cart_id, $negotiable_quote_gift_card_account_management_v1_save_by_quote_id_post_body)





### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\NegotiableQuoteGiftCardAccountManagementV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$cart_id = 56; // int | 
$negotiable_quote_gift_card_account_management_v1_save_by_quote_id_post_body = new \Swagger\Client\Model\NegotiableQuoteGiftCardAccountManagementV1SaveByQuoteIdPostBody(); // \Swagger\Client\Model\NegotiableQuoteGiftCardAccountManagementV1SaveByQuoteIdPostBody | 

try {
    $result = $apiInstance->negotiableQuoteGiftCardAccountManagementV1SaveByQuoteIdPost($cart_id, $negotiable_quote_gift_card_account_management_v1_save_by_quote_id_post_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NegotiableQuoteGiftCardAccountManagementV1Api->negotiableQuoteGiftCardAccountManagementV1SaveByQuoteIdPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cart_id** | **int**|  |
 **negotiable_quote_gift_card_account_management_v1_save_by_quote_id_post_body** | [**\Swagger\Client\Model\NegotiableQuoteGiftCardAccountManagementV1SaveByQuoteIdPostBody**](../Model/NegotiableQuoteGiftCardAccountManagementV1SaveByQuoteIdPostBody.md)|  | [optional]

### Return type

**bool**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

