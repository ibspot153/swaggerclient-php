# Swagger\Client\CompanyCreditCreditLimitManagementV1Api

All URIs are relative to *http://magento2.vagrant12/rest/all*

Method | HTTP request | Description
------------- | ------------- | -------------
[**companyCreditCreditLimitManagementV1GetCreditByCompanyIdGet**](CompanyCreditCreditLimitManagementV1Api.md#companyCreditCreditLimitManagementV1GetCreditByCompanyIdGet) | **GET** /V1/companyCredits/company/{companyId} | 


# **companyCreditCreditLimitManagementV1GetCreditByCompanyIdGet**
> \Swagger\Client\Model\CompanyCreditDataCreditLimitInterface companyCreditCreditLimitManagementV1GetCreditByCompanyIdGet($company_id)



Returns data on the credit limit for a specified company.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CompanyCreditCreditLimitManagementV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$company_id = 56; // int | 

try {
    $result = $apiInstance->companyCreditCreditLimitManagementV1GetCreditByCompanyIdGet($company_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CompanyCreditCreditLimitManagementV1Api->companyCreditCreditLimitManagementV1GetCreditByCompanyIdGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **company_id** | **int**|  |

### Return type

[**\Swagger\Client\Model\CompanyCreditDataCreditLimitInterface**](../Model/CompanyCreditDataCreditLimitInterface.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

