# Swagger\Client\NegotiableQuoteShippingMethodManagementV1Api

All URIs are relative to *http://magento2.vagrant12/rest/all*

Method | HTTP request | Description
------------- | ------------- | -------------
[**negotiableQuoteShippingMethodManagementV1EstimateByAddressIdPost**](NegotiableQuoteShippingMethodManagementV1Api.md#negotiableQuoteShippingMethodManagementV1EstimateByAddressIdPost) | **POST** /V1/negotiable-carts/{cartId}/estimate-shipping-methods-by-address-id | 


# **negotiableQuoteShippingMethodManagementV1EstimateByAddressIdPost**
> \Swagger\Client\Model\QuoteDataShippingMethodInterface[] negotiableQuoteShippingMethodManagementV1EstimateByAddressIdPost($cart_id, $negotiable_quote_shipping_method_management_v1_estimate_by_address_id_post_body)



Estimate shipping

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\NegotiableQuoteShippingMethodManagementV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$cart_id = 56; // int | The shopping cart ID.
$negotiable_quote_shipping_method_management_v1_estimate_by_address_id_post_body = new \Swagger\Client\Model\NegotiableQuoteShippingMethodManagementV1EstimateByAddressIdPostBody(); // \Swagger\Client\Model\NegotiableQuoteShippingMethodManagementV1EstimateByAddressIdPostBody | 

try {
    $result = $apiInstance->negotiableQuoteShippingMethodManagementV1EstimateByAddressIdPost($cart_id, $negotiable_quote_shipping_method_management_v1_estimate_by_address_id_post_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NegotiableQuoteShippingMethodManagementV1Api->negotiableQuoteShippingMethodManagementV1EstimateByAddressIdPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cart_id** | **int**| The shopping cart ID. |
 **negotiable_quote_shipping_method_management_v1_estimate_by_address_id_post_body** | [**\Swagger\Client\Model\NegotiableQuoteShippingMethodManagementV1EstimateByAddressIdPostBody**](../Model/NegotiableQuoteShippingMethodManagementV1EstimateByAddressIdPostBody.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\QuoteDataShippingMethodInterface[]**](../Model/QuoteDataShippingMethodInterface.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

