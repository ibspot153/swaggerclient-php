# Swagger\Client\NegotiableQuoteBillingAddressManagementV1Api

All URIs are relative to *http://magento2.vagrant12/rest/all*

Method | HTTP request | Description
------------- | ------------- | -------------
[**negotiableQuoteBillingAddressManagementV1AssignPost**](NegotiableQuoteBillingAddressManagementV1Api.md#negotiableQuoteBillingAddressManagementV1AssignPost) | **POST** /V1/negotiable-carts/{cartId}/billing-address | 
[**negotiableQuoteBillingAddressManagementV1GetGet**](NegotiableQuoteBillingAddressManagementV1Api.md#negotiableQuoteBillingAddressManagementV1GetGet) | **GET** /V1/negotiable-carts/{cartId}/billing-address | 


# **negotiableQuoteBillingAddressManagementV1AssignPost**
> int negotiableQuoteBillingAddressManagementV1AssignPost($cart_id, $negotiable_quote_billing_address_management_v1_assign_post_body)



Assigns a specified billing address to a specified cart.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\NegotiableQuoteBillingAddressManagementV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$cart_id = 56; // int | The cart ID.
$negotiable_quote_billing_address_management_v1_assign_post_body = new \Swagger\Client\Model\NegotiableQuoteBillingAddressManagementV1AssignPostBody(); // \Swagger\Client\Model\NegotiableQuoteBillingAddressManagementV1AssignPostBody | 

try {
    $result = $apiInstance->negotiableQuoteBillingAddressManagementV1AssignPost($cart_id, $negotiable_quote_billing_address_management_v1_assign_post_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NegotiableQuoteBillingAddressManagementV1Api->negotiableQuoteBillingAddressManagementV1AssignPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cart_id** | **int**| The cart ID. |
 **negotiable_quote_billing_address_management_v1_assign_post_body** | [**\Swagger\Client\Model\NegotiableQuoteBillingAddressManagementV1AssignPostBody**](../Model/NegotiableQuoteBillingAddressManagementV1AssignPostBody.md)|  | [optional]

### Return type

**int**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **negotiableQuoteBillingAddressManagementV1GetGet**
> \Swagger\Client\Model\QuoteDataAddressInterface negotiableQuoteBillingAddressManagementV1GetGet($cart_id)



Returns the billing address for a specified quote.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\NegotiableQuoteBillingAddressManagementV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$cart_id = 56; // int | The cart ID.

try {
    $result = $apiInstance->negotiableQuoteBillingAddressManagementV1GetGet($cart_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NegotiableQuoteBillingAddressManagementV1Api->negotiableQuoteBillingAddressManagementV1GetGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cart_id** | **int**| The cart ID. |

### Return type

[**\Swagger\Client\Model\QuoteDataAddressInterface**](../Model/QuoteDataAddressInterface.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

