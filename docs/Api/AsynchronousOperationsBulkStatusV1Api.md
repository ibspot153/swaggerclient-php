# Swagger\Client\AsynchronousOperationsBulkStatusV1Api

All URIs are relative to *http://magento2.vagrant12/rest/all*

Method | HTTP request | Description
------------- | ------------- | -------------
[**asynchronousOperationsBulkStatusV1GetBulkDetailedStatusGet**](AsynchronousOperationsBulkStatusV1Api.md#asynchronousOperationsBulkStatusV1GetBulkDetailedStatusGet) | **GET** /V1/bulk/{bulkUuid}/detailed-status | 
[**asynchronousOperationsBulkStatusV1GetBulkShortStatusGet**](AsynchronousOperationsBulkStatusV1Api.md#asynchronousOperationsBulkStatusV1GetBulkShortStatusGet) | **GET** /V1/bulk/{bulkUuid}/status | 
[**asynchronousOperationsBulkStatusV1GetOperationsCountByBulkIdAndStatusGet**](AsynchronousOperationsBulkStatusV1Api.md#asynchronousOperationsBulkStatusV1GetOperationsCountByBulkIdAndStatusGet) | **GET** /V1/bulk/{bulkUuid}/operation-status/{status} | 


# **asynchronousOperationsBulkStatusV1GetBulkDetailedStatusGet**
> \Swagger\Client\Model\AsynchronousOperationsDataDetailedBulkOperationsStatusInterface asynchronousOperationsBulkStatusV1GetBulkDetailedStatusGet($bulk_uuid)



Get Bulk summary data with list of operations items full data.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\AsynchronousOperationsBulkStatusV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$bulk_uuid = "bulk_uuid_example"; // string | 

try {
    $result = $apiInstance->asynchronousOperationsBulkStatusV1GetBulkDetailedStatusGet($bulk_uuid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AsynchronousOperationsBulkStatusV1Api->asynchronousOperationsBulkStatusV1GetBulkDetailedStatusGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **bulk_uuid** | **string**|  |

### Return type

[**\Swagger\Client\Model\AsynchronousOperationsDataDetailedBulkOperationsStatusInterface**](../Model/AsynchronousOperationsDataDetailedBulkOperationsStatusInterface.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **asynchronousOperationsBulkStatusV1GetBulkShortStatusGet**
> \Swagger\Client\Model\AsynchronousOperationsDataBulkOperationsStatusInterface asynchronousOperationsBulkStatusV1GetBulkShortStatusGet($bulk_uuid)



Get Bulk summary data with list of operations items short data.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\AsynchronousOperationsBulkStatusV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$bulk_uuid = "bulk_uuid_example"; // string | 

try {
    $result = $apiInstance->asynchronousOperationsBulkStatusV1GetBulkShortStatusGet($bulk_uuid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AsynchronousOperationsBulkStatusV1Api->asynchronousOperationsBulkStatusV1GetBulkShortStatusGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **bulk_uuid** | **string**|  |

### Return type

[**\Swagger\Client\Model\AsynchronousOperationsDataBulkOperationsStatusInterface**](../Model/AsynchronousOperationsDataBulkOperationsStatusInterface.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **asynchronousOperationsBulkStatusV1GetOperationsCountByBulkIdAndStatusGet**
> int asynchronousOperationsBulkStatusV1GetOperationsCountByBulkIdAndStatusGet($bulk_uuid, $status)



Get operations count by bulk uuid and status.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\AsynchronousOperationsBulkStatusV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$bulk_uuid = "bulk_uuid_example"; // string | 
$status = 56; // int | 

try {
    $result = $apiInstance->asynchronousOperationsBulkStatusV1GetOperationsCountByBulkIdAndStatusGet($bulk_uuid, $status);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AsynchronousOperationsBulkStatusV1Api->asynchronousOperationsBulkStatusV1GetOperationsCountByBulkIdAndStatusGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **bulk_uuid** | **string**|  |
 **status** | **int**|  |

### Return type

**int**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

