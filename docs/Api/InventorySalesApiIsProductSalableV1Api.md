# Swagger\Client\InventorySalesApiIsProductSalableV1Api

All URIs are relative to *http://magento2.vagrant12/rest/all*

Method | HTTP request | Description
------------- | ------------- | -------------
[**inventorySalesApiIsProductSalableV1ExecuteGet**](InventorySalesApiIsProductSalableV1Api.md#inventorySalesApiIsProductSalableV1ExecuteGet) | **GET** /V1/inventory/is-product-salable/{sku}/{stockId} | 


# **inventorySalesApiIsProductSalableV1ExecuteGet**
> bool inventorySalesApiIsProductSalableV1ExecuteGet($sku, $stock_id)



Get is product in salable for given SKU in a given Stock

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\InventorySalesApiIsProductSalableV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$sku = "sku_example"; // string | 
$stock_id = 56; // int | 

try {
    $result = $apiInstance->inventorySalesApiIsProductSalableV1ExecuteGet($sku, $stock_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InventorySalesApiIsProductSalableV1Api->inventorySalesApiIsProductSalableV1ExecuteGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sku** | **string**|  |
 **stock_id** | **int**|  |

### Return type

**bool**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

