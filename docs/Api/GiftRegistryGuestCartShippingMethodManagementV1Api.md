# Swagger\Client\GiftRegistryGuestCartShippingMethodManagementV1Api

All URIs are relative to *http://magento2.vagrant12/rest/all*

Method | HTTP request | Description
------------- | ------------- | -------------
[**giftRegistryGuestCartShippingMethodManagementV1EstimateByRegistryIdPost**](GiftRegistryGuestCartShippingMethodManagementV1Api.md#giftRegistryGuestCartShippingMethodManagementV1EstimateByRegistryIdPost) | **POST** /V1/guest-giftregistry/{cartId}/estimate-shipping-methods | 


# **giftRegistryGuestCartShippingMethodManagementV1EstimateByRegistryIdPost**
> \Swagger\Client\Model\QuoteDataShippingMethodInterface[] giftRegistryGuestCartShippingMethodManagementV1EstimateByRegistryIdPost($cart_id, $gift_registry_guest_cart_shipping_method_management_v1_estimate_by_registry_id_post_body)



Estimate shipping

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\GiftRegistryGuestCartShippingMethodManagementV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$cart_id = "cart_id_example"; // string | The shopping cart ID.
$gift_registry_guest_cart_shipping_method_management_v1_estimate_by_registry_id_post_body = new \Swagger\Client\Model\GiftRegistryGuestCartShippingMethodManagementV1EstimateByRegistryIdPostBody(); // \Swagger\Client\Model\GiftRegistryGuestCartShippingMethodManagementV1EstimateByRegistryIdPostBody | 

try {
    $result = $apiInstance->giftRegistryGuestCartShippingMethodManagementV1EstimateByRegistryIdPost($cart_id, $gift_registry_guest_cart_shipping_method_management_v1_estimate_by_registry_id_post_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GiftRegistryGuestCartShippingMethodManagementV1Api->giftRegistryGuestCartShippingMethodManagementV1EstimateByRegistryIdPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cart_id** | **string**| The shopping cart ID. |
 **gift_registry_guest_cart_shipping_method_management_v1_estimate_by_registry_id_post_body** | [**\Swagger\Client\Model\GiftRegistryGuestCartShippingMethodManagementV1EstimateByRegistryIdPostBody**](../Model/GiftRegistryGuestCartShippingMethodManagementV1EstimateByRegistryIdPostBody.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\QuoteDataShippingMethodInterface[]**](../Model/QuoteDataShippingMethodInterface.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

