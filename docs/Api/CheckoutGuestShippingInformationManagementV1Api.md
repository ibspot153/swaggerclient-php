# Swagger\Client\CheckoutGuestShippingInformationManagementV1Api

All URIs are relative to *http://magento2.vagrant12/rest/all*

Method | HTTP request | Description
------------- | ------------- | -------------
[**checkoutGuestShippingInformationManagementV1SaveAddressInformationPost**](CheckoutGuestShippingInformationManagementV1Api.md#checkoutGuestShippingInformationManagementV1SaveAddressInformationPost) | **POST** /V1/guest-carts/{cartId}/shipping-information | 


# **checkoutGuestShippingInformationManagementV1SaveAddressInformationPost**
> \Swagger\Client\Model\CheckoutDataPaymentDetailsInterface checkoutGuestShippingInformationManagementV1SaveAddressInformationPost($cart_id, $checkout_guest_shipping_information_management_v1_save_address_information_post_body)





### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CheckoutGuestShippingInformationManagementV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$cart_id = "cart_id_example"; // string | 
$checkout_guest_shipping_information_management_v1_save_address_information_post_body = new \Swagger\Client\Model\CheckoutGuestShippingInformationManagementV1SaveAddressInformationPostBody(); // \Swagger\Client\Model\CheckoutGuestShippingInformationManagementV1SaveAddressInformationPostBody | 

try {
    $result = $apiInstance->checkoutGuestShippingInformationManagementV1SaveAddressInformationPost($cart_id, $checkout_guest_shipping_information_management_v1_save_address_information_post_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CheckoutGuestShippingInformationManagementV1Api->checkoutGuestShippingInformationManagementV1SaveAddressInformationPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cart_id** | **string**|  |
 **checkout_guest_shipping_information_management_v1_save_address_information_post_body** | [**\Swagger\Client\Model\CheckoutGuestShippingInformationManagementV1SaveAddressInformationPostBody**](../Model/CheckoutGuestShippingInformationManagementV1SaveAddressInformationPostBody.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\CheckoutDataPaymentDetailsInterface**](../Model/CheckoutDataPaymentDetailsInterface.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

