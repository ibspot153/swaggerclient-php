# Swagger\Client\SharedCatalogCompanyManagementV1Api

All URIs are relative to *http://magento2.vagrant12/rest/all*

Method | HTTP request | Description
------------- | ------------- | -------------
[**sharedCatalogCompanyManagementV1AssignCompaniesPost**](SharedCatalogCompanyManagementV1Api.md#sharedCatalogCompanyManagementV1AssignCompaniesPost) | **POST** /V1/sharedCatalog/{sharedCatalogId}/assignCompanies | 
[**sharedCatalogCompanyManagementV1GetCompaniesGet**](SharedCatalogCompanyManagementV1Api.md#sharedCatalogCompanyManagementV1GetCompaniesGet) | **GET** /V1/sharedCatalog/{sharedCatalogId}/companies | 
[**sharedCatalogCompanyManagementV1UnassignCompaniesPost**](SharedCatalogCompanyManagementV1Api.md#sharedCatalogCompanyManagementV1UnassignCompaniesPost) | **POST** /V1/sharedCatalog/{sharedCatalogId}/unassignCompanies | 


# **sharedCatalogCompanyManagementV1AssignCompaniesPost**
> bool sharedCatalogCompanyManagementV1AssignCompaniesPost($shared_catalog_id, $shared_catalog_company_management_v1_assign_companies_post_body)



Assign companies to a shared catalog.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\SharedCatalogCompanyManagementV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$shared_catalog_id = 56; // int | 
$shared_catalog_company_management_v1_assign_companies_post_body = new \Swagger\Client\Model\SharedCatalogCompanyManagementV1AssignCompaniesPostBody(); // \Swagger\Client\Model\SharedCatalogCompanyManagementV1AssignCompaniesPostBody | 

try {
    $result = $apiInstance->sharedCatalogCompanyManagementV1AssignCompaniesPost($shared_catalog_id, $shared_catalog_company_management_v1_assign_companies_post_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SharedCatalogCompanyManagementV1Api->sharedCatalogCompanyManagementV1AssignCompaniesPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **shared_catalog_id** | **int**|  |
 **shared_catalog_company_management_v1_assign_companies_post_body** | [**\Swagger\Client\Model\SharedCatalogCompanyManagementV1AssignCompaniesPostBody**](../Model/SharedCatalogCompanyManagementV1AssignCompaniesPostBody.md)|  | [optional]

### Return type

**bool**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **sharedCatalogCompanyManagementV1GetCompaniesGet**
> string sharedCatalogCompanyManagementV1GetCompaniesGet($shared_catalog_id)



Return the list of company IDs for the companies assigned to the selected catalog.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\SharedCatalogCompanyManagementV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$shared_catalog_id = 56; // int | 

try {
    $result = $apiInstance->sharedCatalogCompanyManagementV1GetCompaniesGet($shared_catalog_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SharedCatalogCompanyManagementV1Api->sharedCatalogCompanyManagementV1GetCompaniesGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **shared_catalog_id** | **int**|  |

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **sharedCatalogCompanyManagementV1UnassignCompaniesPost**
> bool sharedCatalogCompanyManagementV1UnassignCompaniesPost($shared_catalog_id, $shared_catalog_company_management_v1_unassign_companies_post_body)



Unassign companies from a shared catalog.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\SharedCatalogCompanyManagementV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$shared_catalog_id = 56; // int | 
$shared_catalog_company_management_v1_unassign_companies_post_body = new \Swagger\Client\Model\SharedCatalogCompanyManagementV1UnassignCompaniesPostBody(); // \Swagger\Client\Model\SharedCatalogCompanyManagementV1UnassignCompaniesPostBody | 

try {
    $result = $apiInstance->sharedCatalogCompanyManagementV1UnassignCompaniesPost($shared_catalog_id, $shared_catalog_company_management_v1_unassign_companies_post_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SharedCatalogCompanyManagementV1Api->sharedCatalogCompanyManagementV1UnassignCompaniesPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **shared_catalog_id** | **int**|  |
 **shared_catalog_company_management_v1_unassign_companies_post_body** | [**\Swagger\Client\Model\SharedCatalogCompanyManagementV1UnassignCompaniesPostBody**](../Model/SharedCatalogCompanyManagementV1UnassignCompaniesPostBody.md)|  | [optional]

### Return type

**bool**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

