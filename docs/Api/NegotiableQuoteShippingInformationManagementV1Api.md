# Swagger\Client\NegotiableQuoteShippingInformationManagementV1Api

All URIs are relative to *http://magento2.vagrant12/rest/all*

Method | HTTP request | Description
------------- | ------------- | -------------
[**negotiableQuoteShippingInformationManagementV1SaveAddressInformationPost**](NegotiableQuoteShippingInformationManagementV1Api.md#negotiableQuoteShippingInformationManagementV1SaveAddressInformationPost) | **POST** /V1/negotiable-carts/{cartId}/shipping-information | 


# **negotiableQuoteShippingInformationManagementV1SaveAddressInformationPost**
> \Swagger\Client\Model\CheckoutDataPaymentDetailsInterface negotiableQuoteShippingInformationManagementV1SaveAddressInformationPost($cart_id, $negotiable_quote_shipping_information_management_v1_save_address_information_post_body)





### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\NegotiableQuoteShippingInformationManagementV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$cart_id = 56; // int | 
$negotiable_quote_shipping_information_management_v1_save_address_information_post_body = new \Swagger\Client\Model\NegotiableQuoteShippingInformationManagementV1SaveAddressInformationPostBody(); // \Swagger\Client\Model\NegotiableQuoteShippingInformationManagementV1SaveAddressInformationPostBody | 

try {
    $result = $apiInstance->negotiableQuoteShippingInformationManagementV1SaveAddressInformationPost($cart_id, $negotiable_quote_shipping_information_management_v1_save_address_information_post_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NegotiableQuoteShippingInformationManagementV1Api->negotiableQuoteShippingInformationManagementV1SaveAddressInformationPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cart_id** | **int**|  |
 **negotiable_quote_shipping_information_management_v1_save_address_information_post_body** | [**\Swagger\Client\Model\NegotiableQuoteShippingInformationManagementV1SaveAddressInformationPostBody**](../Model/NegotiableQuoteShippingInformationManagementV1SaveAddressInformationPostBody.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\CheckoutDataPaymentDetailsInterface**](../Model/CheckoutDataPaymentDetailsInterface.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

