# Swagger\Client\NegotiableQuoteNegotiableQuoteShippingManagementV1Api

All URIs are relative to *http://magento2.vagrant12/rest/all*

Method | HTTP request | Description
------------- | ------------- | -------------
[**negotiableQuoteNegotiableQuoteShippingManagementV1SetShippingMethodPut**](NegotiableQuoteNegotiableQuoteShippingManagementV1Api.md#negotiableQuoteNegotiableQuoteShippingManagementV1SetShippingMethodPut) | **PUT** /V1/negotiableQuote/{quoteId}/shippingMethod | 


# **negotiableQuoteNegotiableQuoteShippingManagementV1SetShippingMethodPut**
> bool negotiableQuoteNegotiableQuoteShippingManagementV1SetShippingMethodPut($quote_id, $negotiable_quote_negotiable_quote_shipping_management_v1_set_shipping_method_put_body)



Updates the shipping method on a negotiable quote.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\NegotiableQuoteNegotiableQuoteShippingManagementV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$quote_id = 56; // int | Negotiable Quote id
$negotiable_quote_negotiable_quote_shipping_management_v1_set_shipping_method_put_body = new \Swagger\Client\Model\NegotiableQuoteNegotiableQuoteShippingManagementV1SetShippingMethodPutBody(); // \Swagger\Client\Model\NegotiableQuoteNegotiableQuoteShippingManagementV1SetShippingMethodPutBody | 

try {
    $result = $apiInstance->negotiableQuoteNegotiableQuoteShippingManagementV1SetShippingMethodPut($quote_id, $negotiable_quote_negotiable_quote_shipping_management_v1_set_shipping_method_put_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NegotiableQuoteNegotiableQuoteShippingManagementV1Api->negotiableQuoteNegotiableQuoteShippingManagementV1SetShippingMethodPut: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **quote_id** | **int**| Negotiable Quote id |
 **negotiable_quote_negotiable_quote_shipping_management_v1_set_shipping_method_put_body** | [**\Swagger\Client\Model\NegotiableQuoteNegotiableQuoteShippingManagementV1SetShippingMethodPutBody**](../Model/NegotiableQuoteNegotiableQuoteShippingManagementV1SetShippingMethodPutBody.md)|  | [optional]

### Return type

**bool**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

