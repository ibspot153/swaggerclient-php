# Swagger\Client\NegotiableQuoteNegotiableQuoteManagementV1Api

All URIs are relative to *http://magento2.vagrant12/rest/all*

Method | HTTP request | Description
------------- | ------------- | -------------
[**negotiableQuoteNegotiableQuoteManagementV1AdminSendPost**](NegotiableQuoteNegotiableQuoteManagementV1Api.md#negotiableQuoteNegotiableQuoteManagementV1AdminSendPost) | **POST** /V1/negotiableQuote/submitToCustomer | 
[**negotiableQuoteNegotiableQuoteManagementV1CreatePost**](NegotiableQuoteNegotiableQuoteManagementV1Api.md#negotiableQuoteNegotiableQuoteManagementV1CreatePost) | **POST** /V1/negotiableQuote/request | 
[**negotiableQuoteNegotiableQuoteManagementV1DeclinePost**](NegotiableQuoteNegotiableQuoteManagementV1Api.md#negotiableQuoteNegotiableQuoteManagementV1DeclinePost) | **POST** /V1/negotiableQuote/decline | 


# **negotiableQuoteNegotiableQuoteManagementV1AdminSendPost**
> bool negotiableQuoteNegotiableQuoteManagementV1AdminSendPost($negotiable_quote_negotiable_quote_management_v1_admin_send_post_body)



Submit the B2B quote to the customer. The quote status for the customer will be changed to 'Updated', and the customer can work with the quote.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\NegotiableQuoteNegotiableQuoteManagementV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$negotiable_quote_negotiable_quote_management_v1_admin_send_post_body = new \Swagger\Client\Model\NegotiableQuoteNegotiableQuoteManagementV1AdminSendPostBody(); // \Swagger\Client\Model\NegotiableQuoteNegotiableQuoteManagementV1AdminSendPostBody | 

try {
    $result = $apiInstance->negotiableQuoteNegotiableQuoteManagementV1AdminSendPost($negotiable_quote_negotiable_quote_management_v1_admin_send_post_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NegotiableQuoteNegotiableQuoteManagementV1Api->negotiableQuoteNegotiableQuoteManagementV1AdminSendPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **negotiable_quote_negotiable_quote_management_v1_admin_send_post_body** | [**\Swagger\Client\Model\NegotiableQuoteNegotiableQuoteManagementV1AdminSendPostBody**](../Model/NegotiableQuoteNegotiableQuoteManagementV1AdminSendPostBody.md)|  | [optional]

### Return type

**bool**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **negotiableQuoteNegotiableQuoteManagementV1CreatePost**
> bool negotiableQuoteNegotiableQuoteManagementV1CreatePost($negotiable_quote_negotiable_quote_management_v1_create_post_body)



Create a B2B quote based on a regular Magento quote. If the B2B quote requires a shipping address (for negotiation or tax calculations), add it to the regular quote before you create a B2B quote.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\NegotiableQuoteNegotiableQuoteManagementV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$negotiable_quote_negotiable_quote_management_v1_create_post_body = new \Swagger\Client\Model\NegotiableQuoteNegotiableQuoteManagementV1CreatePostBody(); // \Swagger\Client\Model\NegotiableQuoteNegotiableQuoteManagementV1CreatePostBody | 

try {
    $result = $apiInstance->negotiableQuoteNegotiableQuoteManagementV1CreatePost($negotiable_quote_negotiable_quote_management_v1_create_post_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NegotiableQuoteNegotiableQuoteManagementV1Api->negotiableQuoteNegotiableQuoteManagementV1CreatePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **negotiable_quote_negotiable_quote_management_v1_create_post_body** | [**\Swagger\Client\Model\NegotiableQuoteNegotiableQuoteManagementV1CreatePostBody**](../Model/NegotiableQuoteNegotiableQuoteManagementV1CreatePostBody.md)|  | [optional]

### Return type

**bool**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **negotiableQuoteNegotiableQuoteManagementV1DeclinePost**
> bool negotiableQuoteNegotiableQuoteManagementV1DeclinePost($negotiable_quote_negotiable_quote_management_v1_decline_post_body)



Decline the B2B quote. All custom pricing will be removed from this quote. The buyer will be able to place an order using their standard catalog prices and discounts.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\NegotiableQuoteNegotiableQuoteManagementV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$negotiable_quote_negotiable_quote_management_v1_decline_post_body = new \Swagger\Client\Model\NegotiableQuoteNegotiableQuoteManagementV1DeclinePostBody(); // \Swagger\Client\Model\NegotiableQuoteNegotiableQuoteManagementV1DeclinePostBody | 

try {
    $result = $apiInstance->negotiableQuoteNegotiableQuoteManagementV1DeclinePost($negotiable_quote_negotiable_quote_management_v1_decline_post_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NegotiableQuoteNegotiableQuoteManagementV1Api->negotiableQuoteNegotiableQuoteManagementV1DeclinePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **negotiable_quote_negotiable_quote_management_v1_decline_post_body** | [**\Swagger\Client\Model\NegotiableQuoteNegotiableQuoteManagementV1DeclinePostBody**](../Model/NegotiableQuoteNegotiableQuoteManagementV1DeclinePostBody.md)|  | [optional]

### Return type

**bool**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

