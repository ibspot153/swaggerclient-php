# Swagger\Client\InventoryCatalogApiBulkInventoryTransferV1Api

All URIs are relative to *http://magento2.vagrant12/rest/all*

Method | HTTP request | Description
------------- | ------------- | -------------
[**inventoryCatalogApiBulkInventoryTransferV1ExecutePost**](InventoryCatalogApiBulkInventoryTransferV1Api.md#inventoryCatalogApiBulkInventoryTransferV1ExecutePost) | **POST** /V1/inventory/bulk-product-source-transfer | 


# **inventoryCatalogApiBulkInventoryTransferV1ExecutePost**
> bool inventoryCatalogApiBulkInventoryTransferV1ExecutePost($inventory_catalog_api_bulk_inventory_transfer_v1_execute_post_body)



Run bulk inventory transfer

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\InventoryCatalogApiBulkInventoryTransferV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$inventory_catalog_api_bulk_inventory_transfer_v1_execute_post_body = new \Swagger\Client\Model\InventoryCatalogApiBulkInventoryTransferV1ExecutePostBody(); // \Swagger\Client\Model\InventoryCatalogApiBulkInventoryTransferV1ExecutePostBody | 

try {
    $result = $apiInstance->inventoryCatalogApiBulkInventoryTransferV1ExecutePost($inventory_catalog_api_bulk_inventory_transfer_v1_execute_post_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InventoryCatalogApiBulkInventoryTransferV1Api->inventoryCatalogApiBulkInventoryTransferV1ExecutePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inventory_catalog_api_bulk_inventory_transfer_v1_execute_post_body** | [**\Swagger\Client\Model\InventoryCatalogApiBulkInventoryTransferV1ExecutePostBody**](../Model/InventoryCatalogApiBulkInventoryTransferV1ExecutePostBody.md)|  | [optional]

### Return type

**bool**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

