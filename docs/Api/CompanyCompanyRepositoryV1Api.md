# Swagger\Client\CompanyCompanyRepositoryV1Api

All URIs are relative to *http://magento2.vagrant12/rest/all*

Method | HTTP request | Description
------------- | ------------- | -------------
[**companyCompanyRepositoryV1DeleteByIdDelete**](CompanyCompanyRepositoryV1Api.md#companyCompanyRepositoryV1DeleteByIdDelete) | **DELETE** /V1/company/{companyId} | 
[**companyCompanyRepositoryV1GetGet**](CompanyCompanyRepositoryV1Api.md#companyCompanyRepositoryV1GetGet) | **GET** /V1/company/{companyId} | 
[**companyCompanyRepositoryV1GetListGet**](CompanyCompanyRepositoryV1Api.md#companyCompanyRepositoryV1GetListGet) | **GET** /V1/company/ | 
[**companyCompanyRepositoryV1SavePost**](CompanyCompanyRepositoryV1Api.md#companyCompanyRepositoryV1SavePost) | **POST** /V1/company/ | 
[**companyCompanyRepositoryV1SavePut**](CompanyCompanyRepositoryV1Api.md#companyCompanyRepositoryV1SavePut) | **PUT** /V1/company/{companyId} | 


# **companyCompanyRepositoryV1DeleteByIdDelete**
> bool companyCompanyRepositoryV1DeleteByIdDelete($company_id)



Delete a company. Customers belonging to a company are not deleted with this request.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CompanyCompanyRepositoryV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$company_id = 56; // int | 

try {
    $result = $apiInstance->companyCompanyRepositoryV1DeleteByIdDelete($company_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CompanyCompanyRepositoryV1Api->companyCompanyRepositoryV1DeleteByIdDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **company_id** | **int**|  |

### Return type

**bool**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **companyCompanyRepositoryV1GetGet**
> \Swagger\Client\Model\CompanyDataCompanyInterface companyCompanyRepositoryV1GetGet($company_id)



Returns company details.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CompanyCompanyRepositoryV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$company_id = 56; // int | 

try {
    $result = $apiInstance->companyCompanyRepositoryV1GetGet($company_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CompanyCompanyRepositoryV1Api->companyCompanyRepositoryV1GetGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **company_id** | **int**|  |

### Return type

[**\Swagger\Client\Model\CompanyDataCompanyInterface**](../Model/CompanyDataCompanyInterface.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **companyCompanyRepositoryV1GetListGet**
> \Swagger\Client\Model\CompanyDataCompanySearchResultsInterface companyCompanyRepositoryV1GetListGet($search_criteria_filter_groups_0_filters_0_field, $search_criteria_filter_groups_0_filters_0_value, $search_criteria_filter_groups_0_filters_0_condition_type, $search_criteria_sort_orders_0_field, $search_criteria_sort_orders_0_direction, $search_criteria_page_size, $search_criteria_current_page)



Returns the list of companies. The list is an array of objects, and detailed information about item attributes might not be included.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CompanyCompanyRepositoryV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_criteria_filter_groups_0_filters_0_field = "search_criteria_filter_groups_0_filters_0_field_example"; // string | Field
$search_criteria_filter_groups_0_filters_0_value = "search_criteria_filter_groups_0_filters_0_value_example"; // string | Value
$search_criteria_filter_groups_0_filters_0_condition_type = "search_criteria_filter_groups_0_filters_0_condition_type_example"; // string | Condition type
$search_criteria_sort_orders_0_field = "search_criteria_sort_orders_0_field_example"; // string | Sorting field.
$search_criteria_sort_orders_0_direction = "search_criteria_sort_orders_0_direction_example"; // string | Sorting direction.
$search_criteria_page_size = 56; // int | Page size.
$search_criteria_current_page = 56; // int | Current page.

try {
    $result = $apiInstance->companyCompanyRepositoryV1GetListGet($search_criteria_filter_groups_0_filters_0_field, $search_criteria_filter_groups_0_filters_0_value, $search_criteria_filter_groups_0_filters_0_condition_type, $search_criteria_sort_orders_0_field, $search_criteria_sort_orders_0_direction, $search_criteria_page_size, $search_criteria_current_page);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CompanyCompanyRepositoryV1Api->companyCompanyRepositoryV1GetListGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_criteria_filter_groups_0_filters_0_field** | **string**| Field | [optional]
 **search_criteria_filter_groups_0_filters_0_value** | **string**| Value | [optional]
 **search_criteria_filter_groups_0_filters_0_condition_type** | **string**| Condition type | [optional]
 **search_criteria_sort_orders_0_field** | **string**| Sorting field. | [optional]
 **search_criteria_sort_orders_0_direction** | **string**| Sorting direction. | [optional]
 **search_criteria_page_size** | **int**| Page size. | [optional]
 **search_criteria_current_page** | **int**| Current page. | [optional]

### Return type

[**\Swagger\Client\Model\CompanyDataCompanySearchResultsInterface**](../Model/CompanyDataCompanySearchResultsInterface.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **companyCompanyRepositoryV1SavePost**
> \Swagger\Client\Model\CompanyDataCompanyInterface companyCompanyRepositoryV1SavePost($company_company_repository_v1_save_post_body)



Create or update a company account.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CompanyCompanyRepositoryV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$company_company_repository_v1_save_post_body = new \Swagger\Client\Model\CompanyCompanyRepositoryV1SavePostBody(); // \Swagger\Client\Model\CompanyCompanyRepositoryV1SavePostBody | 

try {
    $result = $apiInstance->companyCompanyRepositoryV1SavePost($company_company_repository_v1_save_post_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CompanyCompanyRepositoryV1Api->companyCompanyRepositoryV1SavePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **company_company_repository_v1_save_post_body** | [**\Swagger\Client\Model\CompanyCompanyRepositoryV1SavePostBody**](../Model/CompanyCompanyRepositoryV1SavePostBody.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\CompanyDataCompanyInterface**](../Model/CompanyDataCompanyInterface.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **companyCompanyRepositoryV1SavePut**
> \Swagger\Client\Model\CompanyDataCompanyInterface companyCompanyRepositoryV1SavePut($company_id, $company_company_repository_v1_save_put_body)



Create or update a company account.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CompanyCompanyRepositoryV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$company_id = "company_id_example"; // string | 
$company_company_repository_v1_save_put_body = new \Swagger\Client\Model\CompanyCompanyRepositoryV1SavePutBody(); // \Swagger\Client\Model\CompanyCompanyRepositoryV1SavePutBody | 

try {
    $result = $apiInstance->companyCompanyRepositoryV1SavePut($company_id, $company_company_repository_v1_save_put_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CompanyCompanyRepositoryV1Api->companyCompanyRepositoryV1SavePut: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **company_id** | **string**|  |
 **company_company_repository_v1_save_put_body** | [**\Swagger\Client\Model\CompanyCompanyRepositoryV1SavePutBody**](../Model/CompanyCompanyRepositoryV1SavePutBody.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\CompanyDataCompanyInterface**](../Model/CompanyDataCompanyInterface.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

