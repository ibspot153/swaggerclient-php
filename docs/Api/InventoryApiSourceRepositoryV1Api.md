# Swagger\Client\InventoryApiSourceRepositoryV1Api

All URIs are relative to *http://magento2.vagrant12/rest/all*

Method | HTTP request | Description
------------- | ------------- | -------------
[**inventoryApiSourceRepositoryV1GetGet**](InventoryApiSourceRepositoryV1Api.md#inventoryApiSourceRepositoryV1GetGet) | **GET** /V1/inventory/sources/{sourceCode} | 
[**inventoryApiSourceRepositoryV1GetListGet**](InventoryApiSourceRepositoryV1Api.md#inventoryApiSourceRepositoryV1GetListGet) | **GET** /V1/inventory/sources | 
[**inventoryApiSourceRepositoryV1SavePost**](InventoryApiSourceRepositoryV1Api.md#inventoryApiSourceRepositoryV1SavePost) | **POST** /V1/inventory/sources | 
[**inventoryApiSourceRepositoryV1SavePut**](InventoryApiSourceRepositoryV1Api.md#inventoryApiSourceRepositoryV1SavePut) | **PUT** /V1/inventory/sources/{sourceCode} | 


# **inventoryApiSourceRepositoryV1GetGet**
> \Swagger\Client\Model\InventoryApiDataSourceInterface inventoryApiSourceRepositoryV1GetGet($source_code)



Get Source data by given code. If you want to create plugin on get method, also you need to create separate plugin on getList method, because entity loading way is different for these methods

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\InventoryApiSourceRepositoryV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$source_code = "source_code_example"; // string | 

try {
    $result = $apiInstance->inventoryApiSourceRepositoryV1GetGet($source_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InventoryApiSourceRepositoryV1Api->inventoryApiSourceRepositoryV1GetGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **source_code** | **string**|  |

### Return type

[**\Swagger\Client\Model\InventoryApiDataSourceInterface**](../Model/InventoryApiDataSourceInterface.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **inventoryApiSourceRepositoryV1GetListGet**
> \Swagger\Client\Model\InventoryApiDataSourceSearchResultsInterface inventoryApiSourceRepositoryV1GetListGet($search_criteria_filter_groups_0_filters_0_field, $search_criteria_filter_groups_0_filters_0_value, $search_criteria_filter_groups_0_filters_0_condition_type, $search_criteria_sort_orders_0_field, $search_criteria_sort_orders_0_direction, $search_criteria_page_size, $search_criteria_current_page)



Find Sources by SearchCriteria SearchCriteria is not required because load all stocks is useful case

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\InventoryApiSourceRepositoryV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_criteria_filter_groups_0_filters_0_field = "search_criteria_filter_groups_0_filters_0_field_example"; // string | Field
$search_criteria_filter_groups_0_filters_0_value = "search_criteria_filter_groups_0_filters_0_value_example"; // string | Value
$search_criteria_filter_groups_0_filters_0_condition_type = "search_criteria_filter_groups_0_filters_0_condition_type_example"; // string | Condition type
$search_criteria_sort_orders_0_field = "search_criteria_sort_orders_0_field_example"; // string | Sorting field.
$search_criteria_sort_orders_0_direction = "search_criteria_sort_orders_0_direction_example"; // string | Sorting direction.
$search_criteria_page_size = 56; // int | Page size.
$search_criteria_current_page = 56; // int | Current page.

try {
    $result = $apiInstance->inventoryApiSourceRepositoryV1GetListGet($search_criteria_filter_groups_0_filters_0_field, $search_criteria_filter_groups_0_filters_0_value, $search_criteria_filter_groups_0_filters_0_condition_type, $search_criteria_sort_orders_0_field, $search_criteria_sort_orders_0_direction, $search_criteria_page_size, $search_criteria_current_page);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InventoryApiSourceRepositoryV1Api->inventoryApiSourceRepositoryV1GetListGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_criteria_filter_groups_0_filters_0_field** | **string**| Field | [optional]
 **search_criteria_filter_groups_0_filters_0_value** | **string**| Value | [optional]
 **search_criteria_filter_groups_0_filters_0_condition_type** | **string**| Condition type | [optional]
 **search_criteria_sort_orders_0_field** | **string**| Sorting field. | [optional]
 **search_criteria_sort_orders_0_direction** | **string**| Sorting direction. | [optional]
 **search_criteria_page_size** | **int**| Page size. | [optional]
 **search_criteria_current_page** | **int**| Current page. | [optional]

### Return type

[**\Swagger\Client\Model\InventoryApiDataSourceSearchResultsInterface**](../Model/InventoryApiDataSourceSearchResultsInterface.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **inventoryApiSourceRepositoryV1SavePost**
> \Swagger\Client\Model\ErrorResponse inventoryApiSourceRepositoryV1SavePost($inventory_api_source_repository_v1_save_post_body)



Save Source data

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\InventoryApiSourceRepositoryV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$inventory_api_source_repository_v1_save_post_body = new \Swagger\Client\Model\InventoryApiSourceRepositoryV1SavePostBody(); // \Swagger\Client\Model\InventoryApiSourceRepositoryV1SavePostBody | 

try {
    $result = $apiInstance->inventoryApiSourceRepositoryV1SavePost($inventory_api_source_repository_v1_save_post_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InventoryApiSourceRepositoryV1Api->inventoryApiSourceRepositoryV1SavePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inventory_api_source_repository_v1_save_post_body** | [**\Swagger\Client\Model\InventoryApiSourceRepositoryV1SavePostBody**](../Model/InventoryApiSourceRepositoryV1SavePostBody.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\ErrorResponse**](../Model/ErrorResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **inventoryApiSourceRepositoryV1SavePut**
> \Swagger\Client\Model\ErrorResponse inventoryApiSourceRepositoryV1SavePut($source_code, $inventory_api_source_repository_v1_save_put_body)



Save Source data

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\InventoryApiSourceRepositoryV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$source_code = "source_code_example"; // string | 
$inventory_api_source_repository_v1_save_put_body = new \Swagger\Client\Model\InventoryApiSourceRepositoryV1SavePutBody(); // \Swagger\Client\Model\InventoryApiSourceRepositoryV1SavePutBody | 

try {
    $result = $apiInstance->inventoryApiSourceRepositoryV1SavePut($source_code, $inventory_api_source_repository_v1_save_put_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InventoryApiSourceRepositoryV1Api->inventoryApiSourceRepositoryV1SavePut: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **source_code** | **string**|  |
 **inventory_api_source_repository_v1_save_put_body** | [**\Swagger\Client\Model\InventoryApiSourceRepositoryV1SavePutBody**](../Model/InventoryApiSourceRepositoryV1SavePutBody.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\ErrorResponse**](../Model/ErrorResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

