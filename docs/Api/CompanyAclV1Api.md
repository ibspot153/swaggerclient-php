# Swagger\Client\CompanyAclV1Api

All URIs are relative to *http://magento2.vagrant12/rest/all*

Method | HTTP request | Description
------------- | ------------- | -------------
[**companyAclV1AssignRolesPut**](CompanyAclV1Api.md#companyAclV1AssignRolesPut) | **PUT** /V1/company/assignRoles | 
[**companyAclV1GetUsersByRoleIdGet**](CompanyAclV1Api.md#companyAclV1GetUsersByRoleIdGet) | **GET** /V1/company/role/{roleId}/users | 


# **companyAclV1AssignRolesPut**
> bool companyAclV1AssignRolesPut($company_acl_v1_assign_roles_put_body)



Change a role for a company user.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CompanyAclV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$company_acl_v1_assign_roles_put_body = new \Swagger\Client\Model\CompanyAclV1AssignRolesPutBody(); // \Swagger\Client\Model\CompanyAclV1AssignRolesPutBody | 

try {
    $result = $apiInstance->companyAclV1AssignRolesPut($company_acl_v1_assign_roles_put_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CompanyAclV1Api->companyAclV1AssignRolesPut: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **company_acl_v1_assign_roles_put_body** | [**\Swagger\Client\Model\CompanyAclV1AssignRolesPutBody**](../Model/CompanyAclV1AssignRolesPutBody.md)|  | [optional]

### Return type

**bool**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **companyAclV1GetUsersByRoleIdGet**
> \Swagger\Client\Model\CustomerDataCustomerInterface[] companyAclV1GetUsersByRoleIdGet($role_id)



View the list of company users assigned to a specified role.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CompanyAclV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$role_id = 56; // int | 

try {
    $result = $apiInstance->companyAclV1GetUsersByRoleIdGet($role_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CompanyAclV1Api->companyAclV1GetUsersByRoleIdGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **role_id** | **int**|  |

### Return type

[**\Swagger\Client\Model\CustomerDataCustomerInterface[]**](../Model/CustomerDataCustomerInterface.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

