# Swagger\Client\WorldpayGuestPaymentInformationManagementProxyV1Api

All URIs are relative to *http://magento2.vagrant12/rest/all*

Method | HTTP request | Description
------------- | ------------- | -------------
[**worldpayGuestPaymentInformationManagementProxyV1SavePaymentInformationAndPlaceOrderPost**](WorldpayGuestPaymentInformationManagementProxyV1Api.md#worldpayGuestPaymentInformationManagementProxyV1SavePaymentInformationAndPlaceOrderPost) | **POST** /V1/worldpay-guest-carts/{cartId}/payment-information | 


# **worldpayGuestPaymentInformationManagementProxyV1SavePaymentInformationAndPlaceOrderPost**
> int worldpayGuestPaymentInformationManagementProxyV1SavePaymentInformationAndPlaceOrderPost($cart_id, $worldpay_guest_payment_information_management_proxy_v1_save_payment_information_and_place_order_post_body)



Proxy handler for guest place order

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\WorldpayGuestPaymentInformationManagementProxyV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$cart_id = "cart_id_example"; // string | 
$worldpay_guest_payment_information_management_proxy_v1_save_payment_information_and_place_order_post_body = new \Swagger\Client\Model\WorldpayGuestPaymentInformationManagementProxyV1SavePaymentInformationAndPlaceOrderPostBody(); // \Swagger\Client\Model\WorldpayGuestPaymentInformationManagementProxyV1SavePaymentInformationAndPlaceOrderPostBody | 

try {
    $result = $apiInstance->worldpayGuestPaymentInformationManagementProxyV1SavePaymentInformationAndPlaceOrderPost($cart_id, $worldpay_guest_payment_information_management_proxy_v1_save_payment_information_and_place_order_post_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WorldpayGuestPaymentInformationManagementProxyV1Api->worldpayGuestPaymentInformationManagementProxyV1SavePaymentInformationAndPlaceOrderPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cart_id** | **string**|  |
 **worldpay_guest_payment_information_management_proxy_v1_save_payment_information_and_place_order_post_body** | [**\Swagger\Client\Model\WorldpayGuestPaymentInformationManagementProxyV1SavePaymentInformationAndPlaceOrderPostBody**](../Model/WorldpayGuestPaymentInformationManagementProxyV1SavePaymentInformationAndPlaceOrderPostBody.md)|  | [optional]

### Return type

**int**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

