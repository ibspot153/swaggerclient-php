# Swagger\Client\NegotiableQuoteNegotiableCartRepositoryV1Api

All URIs are relative to *http://magento2.vagrant12/rest/all*

Method | HTTP request | Description
------------- | ------------- | -------------
[**negotiableQuoteNegotiableCartRepositoryV1SavePut**](NegotiableQuoteNegotiableCartRepositoryV1Api.md#negotiableQuoteNegotiableCartRepositoryV1SavePut) | **PUT** /V1/negotiableQuote/{quoteId} | 


# **negotiableQuoteNegotiableCartRepositoryV1SavePut**
> \Swagger\Client\Model\ErrorResponse negotiableQuoteNegotiableCartRepositoryV1SavePut($quote_id, $negotiable_quote_negotiable_cart_repository_v1_save_put_body)



Save quote

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\NegotiableQuoteNegotiableCartRepositoryV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$quote_id = "quote_id_example"; // string | 
$negotiable_quote_negotiable_cart_repository_v1_save_put_body = new \Swagger\Client\Model\NegotiableQuoteNegotiableCartRepositoryV1SavePutBody(); // \Swagger\Client\Model\NegotiableQuoteNegotiableCartRepositoryV1SavePutBody | 

try {
    $result = $apiInstance->negotiableQuoteNegotiableCartRepositoryV1SavePut($quote_id, $negotiable_quote_negotiable_cart_repository_v1_save_put_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NegotiableQuoteNegotiableCartRepositoryV1Api->negotiableQuoteNegotiableCartRepositoryV1SavePut: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **quote_id** | **string**|  |
 **negotiable_quote_negotiable_cart_repository_v1_save_put_body** | [**\Swagger\Client\Model\NegotiableQuoteNegotiableCartRepositoryV1SavePutBody**](../Model/NegotiableQuoteNegotiableCartRepositoryV1SavePutBody.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\ErrorResponse**](../Model/ErrorResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

