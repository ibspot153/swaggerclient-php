# Swagger\Client\NegotiableQuoteCouponManagementV1Api

All URIs are relative to *http://magento2.vagrant12/rest/all*

Method | HTTP request | Description
------------- | ------------- | -------------
[**negotiableQuoteCouponManagementV1RemoveDelete**](NegotiableQuoteCouponManagementV1Api.md#negotiableQuoteCouponManagementV1RemoveDelete) | **DELETE** /V1/negotiable-carts/{cartId}/coupons | 
[**negotiableQuoteCouponManagementV1SetPut**](NegotiableQuoteCouponManagementV1Api.md#negotiableQuoteCouponManagementV1SetPut) | **PUT** /V1/negotiable-carts/{cartId}/coupons/{couponCode} | 


# **negotiableQuoteCouponManagementV1RemoveDelete**
> bool negotiableQuoteCouponManagementV1RemoveDelete($cart_id)



Deletes a coupon from a specified cart.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\NegotiableQuoteCouponManagementV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$cart_id = 56; // int | The cart ID.

try {
    $result = $apiInstance->negotiableQuoteCouponManagementV1RemoveDelete($cart_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NegotiableQuoteCouponManagementV1Api->negotiableQuoteCouponManagementV1RemoveDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cart_id** | **int**| The cart ID. |

### Return type

**bool**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **negotiableQuoteCouponManagementV1SetPut**
> bool negotiableQuoteCouponManagementV1SetPut($cart_id, $coupon_code)



Adds a coupon by code to a specified cart.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\NegotiableQuoteCouponManagementV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$cart_id = 56; // int | The cart ID.
$coupon_code = "coupon_code_example"; // string | The coupon code data.

try {
    $result = $apiInstance->negotiableQuoteCouponManagementV1SetPut($cart_id, $coupon_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NegotiableQuoteCouponManagementV1Api->negotiableQuoteCouponManagementV1SetPut: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cart_id** | **int**| The cart ID. |
 **coupon_code** | **string**| The coupon code data. |

### Return type

**bool**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

