# Swagger\Client\InventoryApiSourceItemsSaveV1Api

All URIs are relative to *http://magento2.vagrant12/rest/all*

Method | HTTP request | Description
------------- | ------------- | -------------
[**inventoryApiSourceItemsSaveV1ExecutePost**](InventoryApiSourceItemsSaveV1Api.md#inventoryApiSourceItemsSaveV1ExecutePost) | **POST** /V1/inventory/source-items | 


# **inventoryApiSourceItemsSaveV1ExecutePost**
> \Swagger\Client\Model\ErrorResponse inventoryApiSourceItemsSaveV1ExecutePost($inventory_api_source_items_save_v1_execute_post_body)



Save Multiple Source item data

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\InventoryApiSourceItemsSaveV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$inventory_api_source_items_save_v1_execute_post_body = new \Swagger\Client\Model\InventoryApiSourceItemsSaveV1ExecutePostBody(); // \Swagger\Client\Model\InventoryApiSourceItemsSaveV1ExecutePostBody | 

try {
    $result = $apiInstance->inventoryApiSourceItemsSaveV1ExecutePost($inventory_api_source_items_save_v1_execute_post_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InventoryApiSourceItemsSaveV1Api->inventoryApiSourceItemsSaveV1ExecutePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inventory_api_source_items_save_v1_execute_post_body** | [**\Swagger\Client\Model\InventoryApiSourceItemsSaveV1ExecutePostBody**](../Model/InventoryApiSourceItemsSaveV1ExecutePostBody.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\ErrorResponse**](../Model/ErrorResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

