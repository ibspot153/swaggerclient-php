# Swagger\Client\InventoryApiStockRepositoryV1Api

All URIs are relative to *http://magento2.vagrant12/rest/all*

Method | HTTP request | Description
------------- | ------------- | -------------
[**inventoryApiStockRepositoryV1DeleteByIdDelete**](InventoryApiStockRepositoryV1Api.md#inventoryApiStockRepositoryV1DeleteByIdDelete) | **DELETE** /V1/inventory/stocks/{stockId} | 
[**inventoryApiStockRepositoryV1GetGet**](InventoryApiStockRepositoryV1Api.md#inventoryApiStockRepositoryV1GetGet) | **GET** /V1/inventory/stocks/{stockId} | 
[**inventoryApiStockRepositoryV1GetListGet**](InventoryApiStockRepositoryV1Api.md#inventoryApiStockRepositoryV1GetListGet) | **GET** /V1/inventory/stocks | 
[**inventoryApiStockRepositoryV1SavePost**](InventoryApiStockRepositoryV1Api.md#inventoryApiStockRepositoryV1SavePost) | **POST** /V1/inventory/stocks | 
[**inventoryApiStockRepositoryV1SavePut**](InventoryApiStockRepositoryV1Api.md#inventoryApiStockRepositoryV1SavePut) | **PUT** /V1/inventory/stocks/{stockId} | 


# **inventoryApiStockRepositoryV1DeleteByIdDelete**
> \Swagger\Client\Model\ErrorResponse inventoryApiStockRepositoryV1DeleteByIdDelete($stock_id)



Delete the Stock data by stockId. If stock is not found do nothing

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\InventoryApiStockRepositoryV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$stock_id = 56; // int | 

try {
    $result = $apiInstance->inventoryApiStockRepositoryV1DeleteByIdDelete($stock_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InventoryApiStockRepositoryV1Api->inventoryApiStockRepositoryV1DeleteByIdDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **stock_id** | **int**|  |

### Return type

[**\Swagger\Client\Model\ErrorResponse**](../Model/ErrorResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **inventoryApiStockRepositoryV1GetGet**
> \Swagger\Client\Model\InventoryApiDataStockInterface inventoryApiStockRepositoryV1GetGet($stock_id)



Get Stock data by given stockId. If you want to create plugin on get method, also you need to create separate plugin on getList method, because entity loading way is different for these methods

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\InventoryApiStockRepositoryV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$stock_id = 56; // int | 

try {
    $result = $apiInstance->inventoryApiStockRepositoryV1GetGet($stock_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InventoryApiStockRepositoryV1Api->inventoryApiStockRepositoryV1GetGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **stock_id** | **int**|  |

### Return type

[**\Swagger\Client\Model\InventoryApiDataStockInterface**](../Model/InventoryApiDataStockInterface.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **inventoryApiStockRepositoryV1GetListGet**
> \Swagger\Client\Model\InventoryApiDataStockSearchResultsInterface inventoryApiStockRepositoryV1GetListGet($search_criteria_filter_groups_0_filters_0_field, $search_criteria_filter_groups_0_filters_0_value, $search_criteria_filter_groups_0_filters_0_condition_type, $search_criteria_sort_orders_0_field, $search_criteria_sort_orders_0_direction, $search_criteria_page_size, $search_criteria_current_page)



Find Stocks by given SearchCriteria SearchCriteria is not required because load all stocks is useful case

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\InventoryApiStockRepositoryV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_criteria_filter_groups_0_filters_0_field = "search_criteria_filter_groups_0_filters_0_field_example"; // string | Field
$search_criteria_filter_groups_0_filters_0_value = "search_criteria_filter_groups_0_filters_0_value_example"; // string | Value
$search_criteria_filter_groups_0_filters_0_condition_type = "search_criteria_filter_groups_0_filters_0_condition_type_example"; // string | Condition type
$search_criteria_sort_orders_0_field = "search_criteria_sort_orders_0_field_example"; // string | Sorting field.
$search_criteria_sort_orders_0_direction = "search_criteria_sort_orders_0_direction_example"; // string | Sorting direction.
$search_criteria_page_size = 56; // int | Page size.
$search_criteria_current_page = 56; // int | Current page.

try {
    $result = $apiInstance->inventoryApiStockRepositoryV1GetListGet($search_criteria_filter_groups_0_filters_0_field, $search_criteria_filter_groups_0_filters_0_value, $search_criteria_filter_groups_0_filters_0_condition_type, $search_criteria_sort_orders_0_field, $search_criteria_sort_orders_0_direction, $search_criteria_page_size, $search_criteria_current_page);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InventoryApiStockRepositoryV1Api->inventoryApiStockRepositoryV1GetListGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_criteria_filter_groups_0_filters_0_field** | **string**| Field | [optional]
 **search_criteria_filter_groups_0_filters_0_value** | **string**| Value | [optional]
 **search_criteria_filter_groups_0_filters_0_condition_type** | **string**| Condition type | [optional]
 **search_criteria_sort_orders_0_field** | **string**| Sorting field. | [optional]
 **search_criteria_sort_orders_0_direction** | **string**| Sorting direction. | [optional]
 **search_criteria_page_size** | **int**| Page size. | [optional]
 **search_criteria_current_page** | **int**| Current page. | [optional]

### Return type

[**\Swagger\Client\Model\InventoryApiDataStockSearchResultsInterface**](../Model/InventoryApiDataStockSearchResultsInterface.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **inventoryApiStockRepositoryV1SavePost**
> int inventoryApiStockRepositoryV1SavePost($inventory_api_stock_repository_v1_save_post_body)



Save Stock data

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\InventoryApiStockRepositoryV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$inventory_api_stock_repository_v1_save_post_body = new \Swagger\Client\Model\InventoryApiStockRepositoryV1SavePostBody(); // \Swagger\Client\Model\InventoryApiStockRepositoryV1SavePostBody | 

try {
    $result = $apiInstance->inventoryApiStockRepositoryV1SavePost($inventory_api_stock_repository_v1_save_post_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InventoryApiStockRepositoryV1Api->inventoryApiStockRepositoryV1SavePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inventory_api_stock_repository_v1_save_post_body** | [**\Swagger\Client\Model\InventoryApiStockRepositoryV1SavePostBody**](../Model/InventoryApiStockRepositoryV1SavePostBody.md)|  | [optional]

### Return type

**int**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **inventoryApiStockRepositoryV1SavePut**
> int inventoryApiStockRepositoryV1SavePut($stock_id, $inventory_api_stock_repository_v1_save_put_body)



Save Stock data

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\InventoryApiStockRepositoryV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$stock_id = "stock_id_example"; // string | 
$inventory_api_stock_repository_v1_save_put_body = new \Swagger\Client\Model\InventoryApiStockRepositoryV1SavePutBody(); // \Swagger\Client\Model\InventoryApiStockRepositoryV1SavePutBody | 

try {
    $result = $apiInstance->inventoryApiStockRepositoryV1SavePut($stock_id, $inventory_api_stock_repository_v1_save_put_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InventoryApiStockRepositoryV1Api->inventoryApiStockRepositoryV1SavePut: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **stock_id** | **string**|  |
 **inventory_api_stock_repository_v1_save_put_body** | [**\Swagger\Client\Model\InventoryApiStockRepositoryV1SavePutBody**](../Model/InventoryApiStockRepositoryV1SavePutBody.md)|  | [optional]

### Return type

**int**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

