# Swagger\Client\InventorySourceSelectionApiSourceSelectionServiceV1Api

All URIs are relative to *http://magento2.vagrant12/rest/all*

Method | HTTP request | Description
------------- | ------------- | -------------
[**inventorySourceSelectionApiSourceSelectionServiceV1ExecutePost**](InventorySourceSelectionApiSourceSelectionServiceV1Api.md#inventorySourceSelectionApiSourceSelectionServiceV1ExecutePost) | **POST** /V1/inventory/source-selection-algorithm-result | 


# **inventorySourceSelectionApiSourceSelectionServiceV1ExecutePost**
> \Swagger\Client\Model\InventorySourceSelectionApiDataSourceSelectionResultInterface inventorySourceSelectionApiSourceSelectionServiceV1ExecutePost($inventory_source_selection_api_source_selection_service_v1_execute_post_body)





### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\InventorySourceSelectionApiSourceSelectionServiceV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$inventory_source_selection_api_source_selection_service_v1_execute_post_body = new \Swagger\Client\Model\InventorySourceSelectionApiSourceSelectionServiceV1ExecutePostBody(); // \Swagger\Client\Model\InventorySourceSelectionApiSourceSelectionServiceV1ExecutePostBody | 

try {
    $result = $apiInstance->inventorySourceSelectionApiSourceSelectionServiceV1ExecutePost($inventory_source_selection_api_source_selection_service_v1_execute_post_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InventorySourceSelectionApiSourceSelectionServiceV1Api->inventorySourceSelectionApiSourceSelectionServiceV1ExecutePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inventory_source_selection_api_source_selection_service_v1_execute_post_body** | [**\Swagger\Client\Model\InventorySourceSelectionApiSourceSelectionServiceV1ExecutePostBody**](../Model/InventorySourceSelectionApiSourceSelectionServiceV1ExecutePostBody.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\InventorySourceSelectionApiDataSourceSelectionResultInterface**](../Model/InventorySourceSelectionApiDataSourceSelectionResultInterface.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

