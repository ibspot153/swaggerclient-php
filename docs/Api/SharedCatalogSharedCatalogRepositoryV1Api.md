# Swagger\Client\SharedCatalogSharedCatalogRepositoryV1Api

All URIs are relative to *http://magento2.vagrant12/rest/all*

Method | HTTP request | Description
------------- | ------------- | -------------
[**sharedCatalogSharedCatalogRepositoryV1DeleteByIdDelete**](SharedCatalogSharedCatalogRepositoryV1Api.md#sharedCatalogSharedCatalogRepositoryV1DeleteByIdDelete) | **DELETE** /V1/sharedCatalog/{sharedCatalogId} | 
[**sharedCatalogSharedCatalogRepositoryV1GetGet**](SharedCatalogSharedCatalogRepositoryV1Api.md#sharedCatalogSharedCatalogRepositoryV1GetGet) | **GET** /V1/sharedCatalog/{sharedCatalogId} | 
[**sharedCatalogSharedCatalogRepositoryV1GetListGet**](SharedCatalogSharedCatalogRepositoryV1Api.md#sharedCatalogSharedCatalogRepositoryV1GetListGet) | **GET** /V1/sharedCatalog/ | 
[**sharedCatalogSharedCatalogRepositoryV1SavePost**](SharedCatalogSharedCatalogRepositoryV1Api.md#sharedCatalogSharedCatalogRepositoryV1SavePost) | **POST** /V1/sharedCatalog | 
[**sharedCatalogSharedCatalogRepositoryV1SavePut**](SharedCatalogSharedCatalogRepositoryV1Api.md#sharedCatalogSharedCatalogRepositoryV1SavePut) | **PUT** /V1/sharedCatalog/{id} | 


# **sharedCatalogSharedCatalogRepositoryV1DeleteByIdDelete**
> bool sharedCatalogSharedCatalogRepositoryV1DeleteByIdDelete($shared_catalog_id)



Delete a shared catalog by ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\SharedCatalogSharedCatalogRepositoryV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$shared_catalog_id = 56; // int | 

try {
    $result = $apiInstance->sharedCatalogSharedCatalogRepositoryV1DeleteByIdDelete($shared_catalog_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SharedCatalogSharedCatalogRepositoryV1Api->sharedCatalogSharedCatalogRepositoryV1DeleteByIdDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **shared_catalog_id** | **int**|  |

### Return type

**bool**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **sharedCatalogSharedCatalogRepositoryV1GetGet**
> \Swagger\Client\Model\SharedCatalogDataSharedCatalogInterface sharedCatalogSharedCatalogRepositoryV1GetGet($shared_catalog_id)



Return the following properties for the selected shared catalog: ID, Store Group ID, Name, Type, Description, Customer Group, Tax Class.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\SharedCatalogSharedCatalogRepositoryV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$shared_catalog_id = 56; // int | 

try {
    $result = $apiInstance->sharedCatalogSharedCatalogRepositoryV1GetGet($shared_catalog_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SharedCatalogSharedCatalogRepositoryV1Api->sharedCatalogSharedCatalogRepositoryV1GetGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **shared_catalog_id** | **int**|  |

### Return type

[**\Swagger\Client\Model\SharedCatalogDataSharedCatalogInterface**](../Model/SharedCatalogDataSharedCatalogInterface.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **sharedCatalogSharedCatalogRepositoryV1GetListGet**
> \Swagger\Client\Model\SharedCatalogDataSearchResultsInterface sharedCatalogSharedCatalogRepositoryV1GetListGet($search_criteria_filter_groups_0_filters_0_field, $search_criteria_filter_groups_0_filters_0_value, $search_criteria_filter_groups_0_filters_0_condition_type, $search_criteria_sort_orders_0_field, $search_criteria_sort_orders_0_direction, $search_criteria_page_size, $search_criteria_current_page)



Return the list of shared catalogs and basic properties for each catalog.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\SharedCatalogSharedCatalogRepositoryV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_criteria_filter_groups_0_filters_0_field = "search_criteria_filter_groups_0_filters_0_field_example"; // string | Field
$search_criteria_filter_groups_0_filters_0_value = "search_criteria_filter_groups_0_filters_0_value_example"; // string | Value
$search_criteria_filter_groups_0_filters_0_condition_type = "search_criteria_filter_groups_0_filters_0_condition_type_example"; // string | Condition type
$search_criteria_sort_orders_0_field = "search_criteria_sort_orders_0_field_example"; // string | Sorting field.
$search_criteria_sort_orders_0_direction = "search_criteria_sort_orders_0_direction_example"; // string | Sorting direction.
$search_criteria_page_size = 56; // int | Page size.
$search_criteria_current_page = 56; // int | Current page.

try {
    $result = $apiInstance->sharedCatalogSharedCatalogRepositoryV1GetListGet($search_criteria_filter_groups_0_filters_0_field, $search_criteria_filter_groups_0_filters_0_value, $search_criteria_filter_groups_0_filters_0_condition_type, $search_criteria_sort_orders_0_field, $search_criteria_sort_orders_0_direction, $search_criteria_page_size, $search_criteria_current_page);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SharedCatalogSharedCatalogRepositoryV1Api->sharedCatalogSharedCatalogRepositoryV1GetListGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_criteria_filter_groups_0_filters_0_field** | **string**| Field | [optional]
 **search_criteria_filter_groups_0_filters_0_value** | **string**| Value | [optional]
 **search_criteria_filter_groups_0_filters_0_condition_type** | **string**| Condition type | [optional]
 **search_criteria_sort_orders_0_field** | **string**| Sorting field. | [optional]
 **search_criteria_sort_orders_0_direction** | **string**| Sorting direction. | [optional]
 **search_criteria_page_size** | **int**| Page size. | [optional]
 **search_criteria_current_page** | **int**| Current page. | [optional]

### Return type

[**\Swagger\Client\Model\SharedCatalogDataSearchResultsInterface**](../Model/SharedCatalogDataSearchResultsInterface.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **sharedCatalogSharedCatalogRepositoryV1SavePost**
> int sharedCatalogSharedCatalogRepositoryV1SavePost($shared_catalog_shared_catalog_repository_v1_save_post_body)



Create or update Shared Catalog service.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\SharedCatalogSharedCatalogRepositoryV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$shared_catalog_shared_catalog_repository_v1_save_post_body = new \Swagger\Client\Model\SharedCatalogSharedCatalogRepositoryV1SavePostBody(); // \Swagger\Client\Model\SharedCatalogSharedCatalogRepositoryV1SavePostBody | 

try {
    $result = $apiInstance->sharedCatalogSharedCatalogRepositoryV1SavePost($shared_catalog_shared_catalog_repository_v1_save_post_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SharedCatalogSharedCatalogRepositoryV1Api->sharedCatalogSharedCatalogRepositoryV1SavePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **shared_catalog_shared_catalog_repository_v1_save_post_body** | [**\Swagger\Client\Model\SharedCatalogSharedCatalogRepositoryV1SavePostBody**](../Model/SharedCatalogSharedCatalogRepositoryV1SavePostBody.md)|  | [optional]

### Return type

**int**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **sharedCatalogSharedCatalogRepositoryV1SavePut**
> int sharedCatalogSharedCatalogRepositoryV1SavePut($id, $shared_catalog_shared_catalog_repository_v1_save_put_body)



Create or update Shared Catalog service.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\SharedCatalogSharedCatalogRepositoryV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | 
$shared_catalog_shared_catalog_repository_v1_save_put_body = new \Swagger\Client\Model\SharedCatalogSharedCatalogRepositoryV1SavePutBody(); // \Swagger\Client\Model\SharedCatalogSharedCatalogRepositoryV1SavePutBody | 

try {
    $result = $apiInstance->sharedCatalogSharedCatalogRepositoryV1SavePut($id, $shared_catalog_shared_catalog_repository_v1_save_put_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SharedCatalogSharedCatalogRepositoryV1Api->sharedCatalogSharedCatalogRepositoryV1SavePut: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**|  |
 **shared_catalog_shared_catalog_repository_v1_save_put_body** | [**\Swagger\Client\Model\SharedCatalogSharedCatalogRepositoryV1SavePutBody**](../Model/SharedCatalogSharedCatalogRepositoryV1SavePutBody.md)|  | [optional]

### Return type

**int**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

