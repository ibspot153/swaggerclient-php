# Swagger\Client\CompanyCreditCreditLimitRepositoryV1Api

All URIs are relative to *http://magento2.vagrant12/rest/all*

Method | HTTP request | Description
------------- | ------------- | -------------
[**companyCreditCreditLimitRepositoryV1GetGet**](CompanyCreditCreditLimitRepositoryV1Api.md#companyCreditCreditLimitRepositoryV1GetGet) | **GET** /V1/companyCredits/{creditId} | 
[**companyCreditCreditLimitRepositoryV1GetListGet**](CompanyCreditCreditLimitRepositoryV1Api.md#companyCreditCreditLimitRepositoryV1GetListGet) | **GET** /V1/companyCredits/ | 
[**companyCreditCreditLimitRepositoryV1SavePut**](CompanyCreditCreditLimitRepositoryV1Api.md#companyCreditCreditLimitRepositoryV1SavePut) | **PUT** /V1/companyCredits/{id} | 


# **companyCreditCreditLimitRepositoryV1GetGet**
> \Swagger\Client\Model\CompanyCreditDataCreditLimitInterface companyCreditCreditLimitRepositoryV1GetGet($credit_id, $reload)



Returns data on the credit limit for a specified credit limit ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CompanyCreditCreditLimitRepositoryV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$credit_id = 56; // int | 
$reload = true; // bool | [optional]

try {
    $result = $apiInstance->companyCreditCreditLimitRepositoryV1GetGet($credit_id, $reload);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CompanyCreditCreditLimitRepositoryV1Api->companyCreditCreditLimitRepositoryV1GetGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **credit_id** | **int**|  |
 **reload** | **bool**| [optional] | [optional]

### Return type

[**\Swagger\Client\Model\CompanyCreditDataCreditLimitInterface**](../Model/CompanyCreditDataCreditLimitInterface.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **companyCreditCreditLimitRepositoryV1GetListGet**
> \Swagger\Client\Model\CompanyCreditDataCreditLimitSearchResultsInterface companyCreditCreditLimitRepositoryV1GetListGet($search_criteria_filter_groups_0_filters_0_field, $search_criteria_filter_groups_0_filters_0_value, $search_criteria_filter_groups_0_filters_0_condition_type, $search_criteria_sort_orders_0_field, $search_criteria_sort_orders_0_direction, $search_criteria_page_size, $search_criteria_current_page)



Returns the list of credits for specified companies.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CompanyCreditCreditLimitRepositoryV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_criteria_filter_groups_0_filters_0_field = "search_criteria_filter_groups_0_filters_0_field_example"; // string | Field
$search_criteria_filter_groups_0_filters_0_value = "search_criteria_filter_groups_0_filters_0_value_example"; // string | Value
$search_criteria_filter_groups_0_filters_0_condition_type = "search_criteria_filter_groups_0_filters_0_condition_type_example"; // string | Condition type
$search_criteria_sort_orders_0_field = "search_criteria_sort_orders_0_field_example"; // string | Sorting field.
$search_criteria_sort_orders_0_direction = "search_criteria_sort_orders_0_direction_example"; // string | Sorting direction.
$search_criteria_page_size = 56; // int | Page size.
$search_criteria_current_page = 56; // int | Current page.

try {
    $result = $apiInstance->companyCreditCreditLimitRepositoryV1GetListGet($search_criteria_filter_groups_0_filters_0_field, $search_criteria_filter_groups_0_filters_0_value, $search_criteria_filter_groups_0_filters_0_condition_type, $search_criteria_sort_orders_0_field, $search_criteria_sort_orders_0_direction, $search_criteria_page_size, $search_criteria_current_page);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CompanyCreditCreditLimitRepositoryV1Api->companyCreditCreditLimitRepositoryV1GetListGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_criteria_filter_groups_0_filters_0_field** | **string**| Field | [optional]
 **search_criteria_filter_groups_0_filters_0_value** | **string**| Value | [optional]
 **search_criteria_filter_groups_0_filters_0_condition_type** | **string**| Condition type | [optional]
 **search_criteria_sort_orders_0_field** | **string**| Sorting field. | [optional]
 **search_criteria_sort_orders_0_direction** | **string**| Sorting direction. | [optional]
 **search_criteria_page_size** | **int**| Page size. | [optional]
 **search_criteria_current_page** | **int**| Current page. | [optional]

### Return type

[**\Swagger\Client\Model\CompanyCreditDataCreditLimitSearchResultsInterface**](../Model/CompanyCreditDataCreditLimitSearchResultsInterface.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **companyCreditCreditLimitRepositoryV1SavePut**
> \Swagger\Client\Model\CompanyCreditDataCreditLimitInterface companyCreditCreditLimitRepositoryV1SavePut($id, $company_credit_credit_limit_repository_v1_save_put_body)



Update the following company credit attributes: credit currency, credit limit and setting to exceed credit.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CompanyCreditCreditLimitRepositoryV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | 
$company_credit_credit_limit_repository_v1_save_put_body = new \Swagger\Client\Model\CompanyCreditCreditLimitRepositoryV1SavePutBody(); // \Swagger\Client\Model\CompanyCreditCreditLimitRepositoryV1SavePutBody | 

try {
    $result = $apiInstance->companyCreditCreditLimitRepositoryV1SavePut($id, $company_credit_credit_limit_repository_v1_save_put_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CompanyCreditCreditLimitRepositoryV1Api->companyCreditCreditLimitRepositoryV1SavePut: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**|  |
 **company_credit_credit_limit_repository_v1_save_put_body** | [**\Swagger\Client\Model\CompanyCreditCreditLimitRepositoryV1SavePutBody**](../Model/CompanyCreditCreditLimitRepositoryV1SavePutBody.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\CompanyCreditDataCreditLimitInterface**](../Model/CompanyCreditDataCreditLimitInterface.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

