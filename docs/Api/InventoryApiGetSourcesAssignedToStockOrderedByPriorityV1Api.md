# Swagger\Client\InventoryApiGetSourcesAssignedToStockOrderedByPriorityV1Api

All URIs are relative to *http://magento2.vagrant12/rest/all*

Method | HTTP request | Description
------------- | ------------- | -------------
[**inventoryApiGetSourcesAssignedToStockOrderedByPriorityV1ExecuteGet**](InventoryApiGetSourcesAssignedToStockOrderedByPriorityV1Api.md#inventoryApiGetSourcesAssignedToStockOrderedByPriorityV1ExecuteGet) | **GET** /V1/inventory/get-sources-assigned-to-stock-ordered-by-priority/{stockId} | 


# **inventoryApiGetSourcesAssignedToStockOrderedByPriorityV1ExecuteGet**
> \Swagger\Client\Model\InventoryApiDataSourceInterface[] inventoryApiGetSourcesAssignedToStockOrderedByPriorityV1ExecuteGet($stock_id)



Get Sources assigned to Stock ordered by priority If Stock with given id doesn't exist then return an empty array

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\InventoryApiGetSourcesAssignedToStockOrderedByPriorityV1Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$stock_id = 56; // int | 

try {
    $result = $apiInstance->inventoryApiGetSourcesAssignedToStockOrderedByPriorityV1ExecuteGet($stock_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InventoryApiGetSourcesAssignedToStockOrderedByPriorityV1Api->inventoryApiGetSourcesAssignedToStockOrderedByPriorityV1ExecuteGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **stock_id** | **int**|  |

### Return type

[**\Swagger\Client\Model\InventoryApiDataSourceInterface[]**](../Model/InventoryApiDataSourceInterface.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

