<?php
/**
 * DownloadableSampleRepositoryV1ApiTest
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Magento Open Source, Commerce, and Commerce for B2B 2.3
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: dev-2.3-develop
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.15
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the endpoint.
 */

namespace Swagger\Client;

use \Swagger\Client\Configuration;
use \Swagger\Client\ApiException;
use \Swagger\Client\ObjectSerializer;

/**
 * DownloadableSampleRepositoryV1ApiTest Class Doc Comment
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class DownloadableSampleRepositoryV1ApiTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test case for downloadableSampleRepositoryV1DeleteDelete
     *
     * .
     *
     */
    public function testDownloadableSampleRepositoryV1DeleteDelete()
    {
    }

    /**
     * Test case for downloadableSampleRepositoryV1GetListGet
     *
     * .
     *
     */
    public function testDownloadableSampleRepositoryV1GetListGet()
    {
    }

    /**
     * Test case for downloadableSampleRepositoryV1SavePost
     *
     * .
     *
     */
    public function testDownloadableSampleRepositoryV1SavePost()
    {
    }

    /**
     * Test case for downloadableSampleRepositoryV1SavePut
     *
     * .
     *
     */
    public function testDownloadableSampleRepositoryV1SavePut()
    {
    }
}
