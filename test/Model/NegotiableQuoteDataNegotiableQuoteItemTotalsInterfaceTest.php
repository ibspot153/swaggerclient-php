<?php
/**
 * NegotiableQuoteDataNegotiableQuoteItemTotalsInterfaceTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Magento Open Source, Commerce, and Commerce for B2B 2.3
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: dev-2.3-develop
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.15
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Swagger\Client;

/**
 * NegotiableQuoteDataNegotiableQuoteItemTotalsInterfaceTest Class Doc Comment
 *
 * @category    Class
 * @description Extension attribute for quote item totals model.
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class NegotiableQuoteDataNegotiableQuoteItemTotalsInterfaceTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "NegotiableQuoteDataNegotiableQuoteItemTotalsInterface"
     */
    public function testNegotiableQuoteDataNegotiableQuoteItemTotalsInterface()
    {
    }

    /**
     * Test attribute "cost"
     */
    public function testPropertyCost()
    {
    }

    /**
     * Test attribute "catalog_price"
     */
    public function testPropertyCatalogPrice()
    {
    }

    /**
     * Test attribute "base_catalog_price"
     */
    public function testPropertyBaseCatalogPrice()
    {
    }

    /**
     * Test attribute "catalog_price_incl_tax"
     */
    public function testPropertyCatalogPriceInclTax()
    {
    }

    /**
     * Test attribute "base_catalog_price_incl_tax"
     */
    public function testPropertyBaseCatalogPriceInclTax()
    {
    }

    /**
     * Test attribute "cart_price"
     */
    public function testPropertyCartPrice()
    {
    }

    /**
     * Test attribute "base_cart_price"
     */
    public function testPropertyBaseCartPrice()
    {
    }

    /**
     * Test attribute "cart_tax"
     */
    public function testPropertyCartTax()
    {
    }

    /**
     * Test attribute "base_cart_tax"
     */
    public function testPropertyBaseCartTax()
    {
    }

    /**
     * Test attribute "cart_price_incl_tax"
     */
    public function testPropertyCartPriceInclTax()
    {
    }

    /**
     * Test attribute "base_cart_price_incl_tax"
     */
    public function testPropertyBaseCartPriceInclTax()
    {
    }

    /**
     * Test attribute "extension_attributes"
     */
    public function testPropertyExtensionAttributes()
    {
    }
}
