<?php
/**
 * DownloadableSampleRepositoryV1SavePostBodyTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Magento Open Source, Commerce, and Commerce for B2B 2.3
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: dev-2.3-develop
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.15
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Swagger\Client;

/**
 * DownloadableSampleRepositoryV1SavePostBodyTest Class Doc Comment
 *
 * @category    Class
 * @description DownloadableSampleRepositoryV1SavePostBody
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class DownloadableSampleRepositoryV1SavePostBodyTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "DownloadableSampleRepositoryV1SavePostBody"
     */
    public function testDownloadableSampleRepositoryV1SavePostBody()
    {
    }

    /**
     * Test attribute "sample"
     */
    public function testPropertySample()
    {
    }

    /**
     * Test attribute "is_global_scope_content"
     */
    public function testPropertyIsGlobalScopeContent()
    {
    }
}
