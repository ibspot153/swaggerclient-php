<?php
/**
 * SalesDataOrderExtensionInterfaceTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Magento Open Source, Commerce, and Commerce for B2B 2.3
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: dev-2.3-develop
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.15
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Swagger\Client;

/**
 * SalesDataOrderExtensionInterfaceTest Class Doc Comment
 *
 * @category    Class
 * @description ExtensionInterface class for @see \\Magento\\Sales\\Api\\Data\\OrderInterface
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class SalesDataOrderExtensionInterfaceTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "SalesDataOrderExtensionInterface"
     */
    public function testSalesDataOrderExtensionInterface()
    {
    }

    /**
     * Test attribute "shipping_assignments"
     */
    public function testPropertyShippingAssignments()
    {
    }

    /**
     * Test attribute "company_order_attributes"
     */
    public function testPropertyCompanyOrderAttributes()
    {
    }

    /**
     * Test attribute "base_customer_balance_amount"
     */
    public function testPropertyBaseCustomerBalanceAmount()
    {
    }

    /**
     * Test attribute "customer_balance_amount"
     */
    public function testPropertyCustomerBalanceAmount()
    {
    }

    /**
     * Test attribute "base_customer_balance_invoiced"
     */
    public function testPropertyBaseCustomerBalanceInvoiced()
    {
    }

    /**
     * Test attribute "customer_balance_invoiced"
     */
    public function testPropertyCustomerBalanceInvoiced()
    {
    }

    /**
     * Test attribute "base_customer_balance_refunded"
     */
    public function testPropertyBaseCustomerBalanceRefunded()
    {
    }

    /**
     * Test attribute "customer_balance_refunded"
     */
    public function testPropertyCustomerBalanceRefunded()
    {
    }

    /**
     * Test attribute "base_customer_balance_total_refunded"
     */
    public function testPropertyBaseCustomerBalanceTotalRefunded()
    {
    }

    /**
     * Test attribute "customer_balance_total_refunded"
     */
    public function testPropertyCustomerBalanceTotalRefunded()
    {
    }

    /**
     * Test attribute "gift_cards"
     */
    public function testPropertyGiftCards()
    {
    }

    /**
     * Test attribute "base_gift_cards_amount"
     */
    public function testPropertyBaseGiftCardsAmount()
    {
    }

    /**
     * Test attribute "gift_cards_amount"
     */
    public function testPropertyGiftCardsAmount()
    {
    }

    /**
     * Test attribute "base_gift_cards_invoiced"
     */
    public function testPropertyBaseGiftCardsInvoiced()
    {
    }

    /**
     * Test attribute "gift_cards_invoiced"
     */
    public function testPropertyGiftCardsInvoiced()
    {
    }

    /**
     * Test attribute "base_gift_cards_refunded"
     */
    public function testPropertyBaseGiftCardsRefunded()
    {
    }

    /**
     * Test attribute "gift_cards_refunded"
     */
    public function testPropertyGiftCardsRefunded()
    {
    }

    /**
     * Test attribute "applied_taxes"
     */
    public function testPropertyAppliedTaxes()
    {
    }

    /**
     * Test attribute "item_applied_taxes"
     */
    public function testPropertyItemAppliedTaxes()
    {
    }

    /**
     * Test attribute "converting_from_quote"
     */
    public function testPropertyConvertingFromQuote()
    {
    }

    /**
     * Test attribute "gift_message"
     */
    public function testPropertyGiftMessage()
    {
    }

    /**
     * Test attribute "gw_id"
     */
    public function testPropertyGwId()
    {
    }

    /**
     * Test attribute "gw_allow_gift_receipt"
     */
    public function testPropertyGwAllowGiftReceipt()
    {
    }

    /**
     * Test attribute "gw_add_card"
     */
    public function testPropertyGwAddCard()
    {
    }

    /**
     * Test attribute "gw_base_price"
     */
    public function testPropertyGwBasePrice()
    {
    }

    /**
     * Test attribute "gw_price"
     */
    public function testPropertyGwPrice()
    {
    }

    /**
     * Test attribute "gw_items_base_price"
     */
    public function testPropertyGwItemsBasePrice()
    {
    }

    /**
     * Test attribute "gw_items_price"
     */
    public function testPropertyGwItemsPrice()
    {
    }

    /**
     * Test attribute "gw_card_base_price"
     */
    public function testPropertyGwCardBasePrice()
    {
    }

    /**
     * Test attribute "gw_card_price"
     */
    public function testPropertyGwCardPrice()
    {
    }

    /**
     * Test attribute "gw_base_tax_amount"
     */
    public function testPropertyGwBaseTaxAmount()
    {
    }

    /**
     * Test attribute "gw_tax_amount"
     */
    public function testPropertyGwTaxAmount()
    {
    }

    /**
     * Test attribute "gw_items_base_tax_amount"
     */
    public function testPropertyGwItemsBaseTaxAmount()
    {
    }

    /**
     * Test attribute "gw_items_tax_amount"
     */
    public function testPropertyGwItemsTaxAmount()
    {
    }

    /**
     * Test attribute "gw_card_base_tax_amount"
     */
    public function testPropertyGwCardBaseTaxAmount()
    {
    }

    /**
     * Test attribute "gw_card_tax_amount"
     */
    public function testPropertyGwCardTaxAmount()
    {
    }

    /**
     * Test attribute "gw_base_price_incl_tax"
     */
    public function testPropertyGwBasePriceInclTax()
    {
    }

    /**
     * Test attribute "gw_price_incl_tax"
     */
    public function testPropertyGwPriceInclTax()
    {
    }

    /**
     * Test attribute "gw_items_base_price_incl_tax"
     */
    public function testPropertyGwItemsBasePriceInclTax()
    {
    }

    /**
     * Test attribute "gw_items_price_incl_tax"
     */
    public function testPropertyGwItemsPriceInclTax()
    {
    }

    /**
     * Test attribute "gw_card_base_price_incl_tax"
     */
    public function testPropertyGwCardBasePriceInclTax()
    {
    }

    /**
     * Test attribute "gw_card_price_incl_tax"
     */
    public function testPropertyGwCardPriceInclTax()
    {
    }

    /**
     * Test attribute "gw_base_price_invoiced"
     */
    public function testPropertyGwBasePriceInvoiced()
    {
    }

    /**
     * Test attribute "gw_price_invoiced"
     */
    public function testPropertyGwPriceInvoiced()
    {
    }

    /**
     * Test attribute "gw_items_base_price_invoiced"
     */
    public function testPropertyGwItemsBasePriceInvoiced()
    {
    }

    /**
     * Test attribute "gw_items_price_invoiced"
     */
    public function testPropertyGwItemsPriceInvoiced()
    {
    }

    /**
     * Test attribute "gw_card_base_price_invoiced"
     */
    public function testPropertyGwCardBasePriceInvoiced()
    {
    }

    /**
     * Test attribute "gw_card_price_invoiced"
     */
    public function testPropertyGwCardPriceInvoiced()
    {
    }

    /**
     * Test attribute "gw_base_tax_amount_invoiced"
     */
    public function testPropertyGwBaseTaxAmountInvoiced()
    {
    }

    /**
     * Test attribute "gw_tax_amount_invoiced"
     */
    public function testPropertyGwTaxAmountInvoiced()
    {
    }

    /**
     * Test attribute "gw_items_base_tax_invoiced"
     */
    public function testPropertyGwItemsBaseTaxInvoiced()
    {
    }

    /**
     * Test attribute "gw_items_tax_invoiced"
     */
    public function testPropertyGwItemsTaxInvoiced()
    {
    }

    /**
     * Test attribute "gw_card_base_tax_invoiced"
     */
    public function testPropertyGwCardBaseTaxInvoiced()
    {
    }

    /**
     * Test attribute "gw_card_tax_invoiced"
     */
    public function testPropertyGwCardTaxInvoiced()
    {
    }

    /**
     * Test attribute "gw_base_price_refunded"
     */
    public function testPropertyGwBasePriceRefunded()
    {
    }

    /**
     * Test attribute "gw_price_refunded"
     */
    public function testPropertyGwPriceRefunded()
    {
    }

    /**
     * Test attribute "gw_items_base_price_refunded"
     */
    public function testPropertyGwItemsBasePriceRefunded()
    {
    }

    /**
     * Test attribute "gw_items_price_refunded"
     */
    public function testPropertyGwItemsPriceRefunded()
    {
    }

    /**
     * Test attribute "gw_card_base_price_refunded"
     */
    public function testPropertyGwCardBasePriceRefunded()
    {
    }

    /**
     * Test attribute "gw_card_price_refunded"
     */
    public function testPropertyGwCardPriceRefunded()
    {
    }

    /**
     * Test attribute "gw_base_tax_amount_refunded"
     */
    public function testPropertyGwBaseTaxAmountRefunded()
    {
    }

    /**
     * Test attribute "gw_tax_amount_refunded"
     */
    public function testPropertyGwTaxAmountRefunded()
    {
    }

    /**
     * Test attribute "gw_items_base_tax_refunded"
     */
    public function testPropertyGwItemsBaseTaxRefunded()
    {
    }

    /**
     * Test attribute "gw_items_tax_refunded"
     */
    public function testPropertyGwItemsTaxRefunded()
    {
    }

    /**
     * Test attribute "gw_card_base_tax_refunded"
     */
    public function testPropertyGwCardBaseTaxRefunded()
    {
    }

    /**
     * Test attribute "gw_card_tax_refunded"
     */
    public function testPropertyGwCardTaxRefunded()
    {
    }
}
