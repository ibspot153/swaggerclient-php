<?php
/**
 * CompanyCreditDataHistoryDataInterfaceTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Magento Open Source, Commerce, and Commerce for B2B 2.3
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: dev-2.3-develop
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.15
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Swagger\Client;

/**
 * CompanyCreditDataHistoryDataInterfaceTest Class Doc Comment
 *
 * @category    Class
 * @description History data transfer object interface.
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class CompanyCreditDataHistoryDataInterfaceTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "CompanyCreditDataHistoryDataInterface"
     */
    public function testCompanyCreditDataHistoryDataInterface()
    {
    }

    /**
     * Test attribute "id"
     */
    public function testPropertyId()
    {
    }

    /**
     * Test attribute "company_credit_id"
     */
    public function testPropertyCompanyCreditId()
    {
    }

    /**
     * Test attribute "user_id"
     */
    public function testPropertyUserId()
    {
    }

    /**
     * Test attribute "user_type"
     */
    public function testPropertyUserType()
    {
    }

    /**
     * Test attribute "currency_credit"
     */
    public function testPropertyCurrencyCredit()
    {
    }

    /**
     * Test attribute "currency_operation"
     */
    public function testPropertyCurrencyOperation()
    {
    }

    /**
     * Test attribute "rate"
     */
    public function testPropertyRate()
    {
    }

    /**
     * Test attribute "rate_credit"
     */
    public function testPropertyRateCredit()
    {
    }

    /**
     * Test attribute "amount"
     */
    public function testPropertyAmount()
    {
    }

    /**
     * Test attribute "balance"
     */
    public function testPropertyBalance()
    {
    }

    /**
     * Test attribute "credit_limit"
     */
    public function testPropertyCreditLimit()
    {
    }

    /**
     * Test attribute "available_limit"
     */
    public function testPropertyAvailableLimit()
    {
    }

    /**
     * Test attribute "type"
     */
    public function testPropertyType()
    {
    }

    /**
     * Test attribute "datetime"
     */
    public function testPropertyDatetime()
    {
    }

    /**
     * Test attribute "purchase_order"
     */
    public function testPropertyPurchaseOrder()
    {
    }

    /**
     * Test attribute "comment"
     */
    public function testPropertyComment()
    {
    }
}
