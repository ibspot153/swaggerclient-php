<?php
/**
 * NegotiableQuoteDataCommentAttachmentInterfaceTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Magento Open Source, Commerce, and Commerce for B2B 2.3
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: dev-2.3-develop
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.15
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Swagger\Client;

/**
 * NegotiableQuoteDataCommentAttachmentInterfaceTest Class Doc Comment
 *
 * @category    Class
 * @description Interface for quote comment attachment.
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class NegotiableQuoteDataCommentAttachmentInterfaceTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "NegotiableQuoteDataCommentAttachmentInterface"
     */
    public function testNegotiableQuoteDataCommentAttachmentInterface()
    {
    }

    /**
     * Test attribute "attachment_id"
     */
    public function testPropertyAttachmentId()
    {
    }

    /**
     * Test attribute "comment_id"
     */
    public function testPropertyCommentId()
    {
    }

    /**
     * Test attribute "file_name"
     */
    public function testPropertyFileName()
    {
    }

    /**
     * Test attribute "file_path"
     */
    public function testPropertyFilePath()
    {
    }

    /**
     * Test attribute "file_type"
     */
    public function testPropertyFileType()
    {
    }

    /**
     * Test attribute "extension_attributes"
     */
    public function testPropertyExtensionAttributes()
    {
    }
}
