<?php
/**
 * AsynchronousOperationsDataOperationInterfaceTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Magento Open Source, Commerce, and Commerce for B2B 2.3
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: dev-2.3-develop
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.15
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Swagger\Client;

/**
 * AsynchronousOperationsDataOperationInterfaceTest Class Doc Comment
 *
 * @category    Class
 * @description Class OperationInterface
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class AsynchronousOperationsDataOperationInterfaceTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "AsynchronousOperationsDataOperationInterface"
     */
    public function testAsynchronousOperationsDataOperationInterface()
    {
    }

    /**
     * Test attribute "extension_attributes"
     */
    public function testPropertyExtensionAttributes()
    {
    }

    /**
     * Test attribute "id"
     */
    public function testPropertyId()
    {
    }

    /**
     * Test attribute "bulk_uuid"
     */
    public function testPropertyBulkUuid()
    {
    }

    /**
     * Test attribute "topic_name"
     */
    public function testPropertyTopicName()
    {
    }

    /**
     * Test attribute "serialized_data"
     */
    public function testPropertySerializedData()
    {
    }

    /**
     * Test attribute "result_serialized_data"
     */
    public function testPropertyResultSerializedData()
    {
    }

    /**
     * Test attribute "status"
     */
    public function testPropertyStatus()
    {
    }

    /**
     * Test attribute "result_message"
     */
    public function testPropertyResultMessage()
    {
    }

    /**
     * Test attribute "error_code"
     */
    public function testPropertyErrorCode()
    {
    }
}
