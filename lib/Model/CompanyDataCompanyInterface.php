<?php
/**
 * CompanyDataCompanyInterface
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Magento Open Source, Commerce, and Commerce for B2B 2.3
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: dev-2.3-develop
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.15
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Client\Model;

use \ArrayAccess;
use \Swagger\Client\ObjectSerializer;

/**
 * CompanyDataCompanyInterface Class Doc Comment
 *
 * @category Class
 * @description Interface for Company entity.
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class CompanyDataCompanyInterface implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'company-data-company-interface';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'id' => 'int',
        'status' => 'int',
        'company_name' => 'string',
        'legal_name' => 'string',
        'company_email' => 'string',
        'vat_tax_id' => 'string',
        'reseller_id' => 'string',
        'comment' => 'string',
        'street' => 'string[]',
        'city' => 'string',
        'country_id' => 'string',
        'region' => 'string',
        'region_id' => 'string',
        'postcode' => 'string',
        'telephone' => 'string',
        'customer_group_id' => 'int',
        'sales_representative_id' => 'int',
        'reject_reason' => 'string',
        'rejected_at' => 'string',
        'super_user_id' => 'int',
        'extension_attributes' => '\Swagger\Client\Model\CompanyDataCompanyExtensionInterface'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'id' => null,
        'status' => null,
        'company_name' => null,
        'legal_name' => null,
        'company_email' => null,
        'vat_tax_id' => null,
        'reseller_id' => null,
        'comment' => null,
        'street' => null,
        'city' => null,
        'country_id' => null,
        'region' => null,
        'region_id' => null,
        'postcode' => null,
        'telephone' => null,
        'customer_group_id' => null,
        'sales_representative_id' => null,
        'reject_reason' => null,
        'rejected_at' => null,
        'super_user_id' => null,
        'extension_attributes' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'id' => 'id',
        'status' => 'status',
        'company_name' => 'company_name',
        'legal_name' => 'legal_name',
        'company_email' => 'company_email',
        'vat_tax_id' => 'vat_tax_id',
        'reseller_id' => 'reseller_id',
        'comment' => 'comment',
        'street' => 'street',
        'city' => 'city',
        'country_id' => 'country_id',
        'region' => 'region',
        'region_id' => 'region_id',
        'postcode' => 'postcode',
        'telephone' => 'telephone',
        'customer_group_id' => 'customer_group_id',
        'sales_representative_id' => 'sales_representative_id',
        'reject_reason' => 'reject_reason',
        'rejected_at' => 'rejected_at',
        'super_user_id' => 'super_user_id',
        'extension_attributes' => 'extension_attributes'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'id' => 'setId',
        'status' => 'setStatus',
        'company_name' => 'setCompanyName',
        'legal_name' => 'setLegalName',
        'company_email' => 'setCompanyEmail',
        'vat_tax_id' => 'setVatTaxId',
        'reseller_id' => 'setResellerId',
        'comment' => 'setComment',
        'street' => 'setStreet',
        'city' => 'setCity',
        'country_id' => 'setCountryId',
        'region' => 'setRegion',
        'region_id' => 'setRegionId',
        'postcode' => 'setPostcode',
        'telephone' => 'setTelephone',
        'customer_group_id' => 'setCustomerGroupId',
        'sales_representative_id' => 'setSalesRepresentativeId',
        'reject_reason' => 'setRejectReason',
        'rejected_at' => 'setRejectedAt',
        'super_user_id' => 'setSuperUserId',
        'extension_attributes' => 'setExtensionAttributes'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'id' => 'getId',
        'status' => 'getStatus',
        'company_name' => 'getCompanyName',
        'legal_name' => 'getLegalName',
        'company_email' => 'getCompanyEmail',
        'vat_tax_id' => 'getVatTaxId',
        'reseller_id' => 'getResellerId',
        'comment' => 'getComment',
        'street' => 'getStreet',
        'city' => 'getCity',
        'country_id' => 'getCountryId',
        'region' => 'getRegion',
        'region_id' => 'getRegionId',
        'postcode' => 'getPostcode',
        'telephone' => 'getTelephone',
        'customer_group_id' => 'getCustomerGroupId',
        'sales_representative_id' => 'getSalesRepresentativeId',
        'reject_reason' => 'getRejectReason',
        'rejected_at' => 'getRejectedAt',
        'super_user_id' => 'getSuperUserId',
        'extension_attributes' => 'getExtensionAttributes'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    

    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['id'] = isset($data['id']) ? $data['id'] : null;
        $this->container['status'] = isset($data['status']) ? $data['status'] : null;
        $this->container['company_name'] = isset($data['company_name']) ? $data['company_name'] : null;
        $this->container['legal_name'] = isset($data['legal_name']) ? $data['legal_name'] : null;
        $this->container['company_email'] = isset($data['company_email']) ? $data['company_email'] : null;
        $this->container['vat_tax_id'] = isset($data['vat_tax_id']) ? $data['vat_tax_id'] : null;
        $this->container['reseller_id'] = isset($data['reseller_id']) ? $data['reseller_id'] : null;
        $this->container['comment'] = isset($data['comment']) ? $data['comment'] : null;
        $this->container['street'] = isset($data['street']) ? $data['street'] : null;
        $this->container['city'] = isset($data['city']) ? $data['city'] : null;
        $this->container['country_id'] = isset($data['country_id']) ? $data['country_id'] : null;
        $this->container['region'] = isset($data['region']) ? $data['region'] : null;
        $this->container['region_id'] = isset($data['region_id']) ? $data['region_id'] : null;
        $this->container['postcode'] = isset($data['postcode']) ? $data['postcode'] : null;
        $this->container['telephone'] = isset($data['telephone']) ? $data['telephone'] : null;
        $this->container['customer_group_id'] = isset($data['customer_group_id']) ? $data['customer_group_id'] : null;
        $this->container['sales_representative_id'] = isset($data['sales_representative_id']) ? $data['sales_representative_id'] : null;
        $this->container['reject_reason'] = isset($data['reject_reason']) ? $data['reject_reason'] : null;
        $this->container['rejected_at'] = isset($data['rejected_at']) ? $data['rejected_at'] : null;
        $this->container['super_user_id'] = isset($data['super_user_id']) ? $data['super_user_id'] : null;
        $this->container['extension_attributes'] = isset($data['extension_attributes']) ? $data['extension_attributes'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        if ($this->container['street'] === null) {
            $invalidProperties[] = "'street' can't be null";
        }
        if ($this->container['customer_group_id'] === null) {
            $invalidProperties[] = "'customer_group_id' can't be null";
        }
        if ($this->container['sales_representative_id'] === null) {
            $invalidProperties[] = "'sales_representative_id' can't be null";
        }
        if ($this->container['reject_reason'] === null) {
            $invalidProperties[] = "'reject_reason' can't be null";
        }
        if ($this->container['rejected_at'] === null) {
            $invalidProperties[] = "'rejected_at' can't be null";
        }
        if ($this->container['super_user_id'] === null) {
            $invalidProperties[] = "'super_user_id' can't be null";
        }
        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets id
     *
     * @return int
     */
    public function getId()
    {
        return $this->container['id'];
    }

    /**
     * Sets id
     *
     * @param int $id Id.
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->container['id'] = $id;

        return $this;
    }

    /**
     * Gets status
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->container['status'];
    }

    /**
     * Sets status
     *
     * @param int $status Status.
     *
     * @return $this
     */
    public function setStatus($status)
    {
        $this->container['status'] = $status;

        return $this;
    }

    /**
     * Gets company_name
     *
     * @return string
     */
    public function getCompanyName()
    {
        return $this->container['company_name'];
    }

    /**
     * Sets company_name
     *
     * @param string $company_name Company name.
     *
     * @return $this
     */
    public function setCompanyName($company_name)
    {
        $this->container['company_name'] = $company_name;

        return $this;
    }

    /**
     * Gets legal_name
     *
     * @return string
     */
    public function getLegalName()
    {
        return $this->container['legal_name'];
    }

    /**
     * Sets legal_name
     *
     * @param string $legal_name Legal name.
     *
     * @return $this
     */
    public function setLegalName($legal_name)
    {
        $this->container['legal_name'] = $legal_name;

        return $this;
    }

    /**
     * Gets company_email
     *
     * @return string
     */
    public function getCompanyEmail()
    {
        return $this->container['company_email'];
    }

    /**
     * Sets company_email
     *
     * @param string $company_email Company email.
     *
     * @return $this
     */
    public function setCompanyEmail($company_email)
    {
        $this->container['company_email'] = $company_email;

        return $this;
    }

    /**
     * Gets vat_tax_id
     *
     * @return string
     */
    public function getVatTaxId()
    {
        return $this->container['vat_tax_id'];
    }

    /**
     * Sets vat_tax_id
     *
     * @param string $vat_tax_id Vat tax id.
     *
     * @return $this
     */
    public function setVatTaxId($vat_tax_id)
    {
        $this->container['vat_tax_id'] = $vat_tax_id;

        return $this;
    }

    /**
     * Gets reseller_id
     *
     * @return string
     */
    public function getResellerId()
    {
        return $this->container['reseller_id'];
    }

    /**
     * Sets reseller_id
     *
     * @param string $reseller_id Reseller Id.
     *
     * @return $this
     */
    public function setResellerId($reseller_id)
    {
        $this->container['reseller_id'] = $reseller_id;

        return $this;
    }

    /**
     * Gets comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->container['comment'];
    }

    /**
     * Sets comment
     *
     * @param string $comment Comment.
     *
     * @return $this
     */
    public function setComment($comment)
    {
        $this->container['comment'] = $comment;

        return $this;
    }

    /**
     * Gets street
     *
     * @return string[]
     */
    public function getStreet()
    {
        return $this->container['street'];
    }

    /**
     * Sets street
     *
     * @param string[] $street Street.
     *
     * @return $this
     */
    public function setStreet($street)
    {
        $this->container['street'] = $street;

        return $this;
    }

    /**
     * Gets city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->container['city'];
    }

    /**
     * Sets city
     *
     * @param string $city City.
     *
     * @return $this
     */
    public function setCity($city)
    {
        $this->container['city'] = $city;

        return $this;
    }

    /**
     * Gets country_id
     *
     * @return string
     */
    public function getCountryId()
    {
        return $this->container['country_id'];
    }

    /**
     * Sets country_id
     *
     * @param string $country_id Country.
     *
     * @return $this
     */
    public function setCountryId($country_id)
    {
        $this->container['country_id'] = $country_id;

        return $this;
    }

    /**
     * Gets region
     *
     * @return string
     */
    public function getRegion()
    {
        return $this->container['region'];
    }

    /**
     * Sets region
     *
     * @param string $region Region.
     *
     * @return $this
     */
    public function setRegion($region)
    {
        $this->container['region'] = $region;

        return $this;
    }

    /**
     * Gets region_id
     *
     * @return string
     */
    public function getRegionId()
    {
        return $this->container['region_id'];
    }

    /**
     * Sets region_id
     *
     * @param string $region_id Region Id.
     *
     * @return $this
     */
    public function setRegionId($region_id)
    {
        $this->container['region_id'] = $region_id;

        return $this;
    }

    /**
     * Gets postcode
     *
     * @return string
     */
    public function getPostcode()
    {
        return $this->container['postcode'];
    }

    /**
     * Sets postcode
     *
     * @param string $postcode Postcode.
     *
     * @return $this
     */
    public function setPostcode($postcode)
    {
        $this->container['postcode'] = $postcode;

        return $this;
    }

    /**
     * Gets telephone
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->container['telephone'];
    }

    /**
     * Sets telephone
     *
     * @param string $telephone Telephone.
     *
     * @return $this
     */
    public function setTelephone($telephone)
    {
        $this->container['telephone'] = $telephone;

        return $this;
    }

    /**
     * Gets customer_group_id
     *
     * @return int
     */
    public function getCustomerGroupId()
    {
        return $this->container['customer_group_id'];
    }

    /**
     * Sets customer_group_id
     *
     * @param int $customer_group_id Customer Group Id.
     *
     * @return $this
     */
    public function setCustomerGroupId($customer_group_id)
    {
        $this->container['customer_group_id'] = $customer_group_id;

        return $this;
    }

    /**
     * Gets sales_representative_id
     *
     * @return int
     */
    public function getSalesRepresentativeId()
    {
        return $this->container['sales_representative_id'];
    }

    /**
     * Sets sales_representative_id
     *
     * @param int $sales_representative_id Sales Representative Id.
     *
     * @return $this
     */
    public function setSalesRepresentativeId($sales_representative_id)
    {
        $this->container['sales_representative_id'] = $sales_representative_id;

        return $this;
    }

    /**
     * Gets reject_reason
     *
     * @return string
     */
    public function getRejectReason()
    {
        return $this->container['reject_reason'];
    }

    /**
     * Sets reject_reason
     *
     * @param string $reject_reason Reject Reason.
     *
     * @return $this
     */
    public function setRejectReason($reject_reason)
    {
        $this->container['reject_reason'] = $reject_reason;

        return $this;
    }

    /**
     * Gets rejected_at
     *
     * @return string
     */
    public function getRejectedAt()
    {
        return $this->container['rejected_at'];
    }

    /**
     * Sets rejected_at
     *
     * @param string $rejected_at Rejected at time.
     *
     * @return $this
     */
    public function setRejectedAt($rejected_at)
    {
        $this->container['rejected_at'] = $rejected_at;

        return $this;
    }

    /**
     * Gets super_user_id
     *
     * @return int
     */
    public function getSuperUserId()
    {
        return $this->container['super_user_id'];
    }

    /**
     * Sets super_user_id
     *
     * @param int $super_user_id Company admin customer id.
     *
     * @return $this
     */
    public function setSuperUserId($super_user_id)
    {
        $this->container['super_user_id'] = $super_user_id;

        return $this;
    }

    /**
     * Gets extension_attributes
     *
     * @return \Swagger\Client\Model\CompanyDataCompanyExtensionInterface
     */
    public function getExtensionAttributes()
    {
        return $this->container['extension_attributes'];
    }

    /**
     * Sets extension_attributes
     *
     * @param \Swagger\Client\Model\CompanyDataCompanyExtensionInterface $extension_attributes extension_attributes
     *
     * @return $this
     */
    public function setExtensionAttributes($extension_attributes)
    {
        $this->container['extension_attributes'] = $extension_attributes;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


