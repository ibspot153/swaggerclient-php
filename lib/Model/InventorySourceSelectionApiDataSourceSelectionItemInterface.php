<?php
/**
 * InventorySourceSelectionApiDataSourceSelectionItemInterface
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Magento Open Source, Commerce, and Commerce for B2B 2.3
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: dev-2.3-develop
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.15
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Client\Model;

use \ArrayAccess;
use \Swagger\Client\ObjectSerializer;

/**
 * InventorySourceSelectionApiDataSourceSelectionItemInterface Class Doc Comment
 *
 * @category Class
 * @description Represents source selection result for the specific source and SKU
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class InventorySourceSelectionApiDataSourceSelectionItemInterface implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'inventory-source-selection-api-data-source-selection-item-interface';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'source_code' => 'string',
        'sku' => 'string',
        'qty_to_deduct' => 'float',
        'qty_available' => 'float'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'source_code' => null,
        'sku' => null,
        'qty_to_deduct' => null,
        'qty_available' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'source_code' => 'source_code',
        'sku' => 'sku',
        'qty_to_deduct' => 'qty_to_deduct',
        'qty_available' => 'qty_available'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'source_code' => 'setSourceCode',
        'sku' => 'setSku',
        'qty_to_deduct' => 'setQtyToDeduct',
        'qty_available' => 'setQtyAvailable'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'source_code' => 'getSourceCode',
        'sku' => 'getSku',
        'qty_to_deduct' => 'getQtyToDeduct',
        'qty_available' => 'getQtyAvailable'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    

    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['source_code'] = isset($data['source_code']) ? $data['source_code'] : null;
        $this->container['sku'] = isset($data['sku']) ? $data['sku'] : null;
        $this->container['qty_to_deduct'] = isset($data['qty_to_deduct']) ? $data['qty_to_deduct'] : null;
        $this->container['qty_available'] = isset($data['qty_available']) ? $data['qty_available'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        if ($this->container['source_code'] === null) {
            $invalidProperties[] = "'source_code' can't be null";
        }
        if ($this->container['sku'] === null) {
            $invalidProperties[] = "'sku' can't be null";
        }
        if ($this->container['qty_to_deduct'] === null) {
            $invalidProperties[] = "'qty_to_deduct' can't be null";
        }
        if ($this->container['qty_available'] === null) {
            $invalidProperties[] = "'qty_available' can't be null";
        }
        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets source_code
     *
     * @return string
     */
    public function getSourceCode()
    {
        return $this->container['source_code'];
    }

    /**
     * Sets source_code
     *
     * @param string $source_code Source code
     *
     * @return $this
     */
    public function setSourceCode($source_code)
    {
        $this->container['source_code'] = $source_code;

        return $this;
    }

    /**
     * Gets sku
     *
     * @return string
     */
    public function getSku()
    {
        return $this->container['sku'];
    }

    /**
     * Sets sku
     *
     * @param string $sku Item SKU
     *
     * @return $this
     */
    public function setSku($sku)
    {
        $this->container['sku'] = $sku;

        return $this;
    }

    /**
     * Gets qty_to_deduct
     *
     * @return float
     */
    public function getQtyToDeduct()
    {
        return $this->container['qty_to_deduct'];
    }

    /**
     * Sets qty_to_deduct
     *
     * @param float $qty_to_deduct Quantity which will be deducted for this source
     *
     * @return $this
     */
    public function setQtyToDeduct($qty_to_deduct)
    {
        $this->container['qty_to_deduct'] = $qty_to_deduct;

        return $this;
    }

    /**
     * Gets qty_available
     *
     * @return float
     */
    public function getQtyAvailable()
    {
        return $this->container['qty_available'];
    }

    /**
     * Sets qty_available
     *
     * @param float $qty_available Available quantity for this source
     *
     * @return $this
     */
    public function setQtyAvailable($qty_available)
    {
        $this->container['qty_available'] = $qty_available;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


